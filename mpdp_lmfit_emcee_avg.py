import argparse
import corner
import csv
import lmfit
import matplotlib.pyplot as plt
import mpdp
import numpy as np
import os


def model(params, t, Cpsamples, Cptimes):
	k = params['k']
	ec50 = params['ec50']
	n = params['n']
	e0 = params['e0']
	emax = params['emax']

	return e0 + emax*mpdp.effect(mpdp.Ce(t, k, Cptimes, Cpsamples), ec50, n)


def residual(params, t, data, Cpsamples, Cptimes):
	return (model(params, t, Cpsamples, Cptimes) - data)


def fit_model(times2, data, Cpsamples, Cptimes, outroot, mcmc_steps):
	params = lmfit.Parameters()
	params.add('k', min=mpdp.keqs[0], max=mpdp.keqs[-1])
	params.add('ec50', min=mpdp.ec50s[0], max=mpdp.ec50s[-1])
	params.add('n', min=mpdp.hillns[0], max=mpdp.hillns[-1])
	params.add('e0', min=0, max=100)
	params.add('emax', value=0, min=-40, max=40) # midbrain response in Chen 2015 was ~70%

	mi = lmfit.minimize(residual, params, method='ampgo', args=(times2, data, Cpsamples, Cptimes), nan_policy='omit')
	lmfit.report_fit(mi)
	
	res = lmfit.minimize(residual, mi.params, method='emcee', args=(times2, data, Cpsamples, Cptimes), is_weighted=False, nan_policy='omit', steps=int(mcmc_steps))
	lmfit.report_fit(res)

	if hasattr(res, "acor"):
		print("Autocorrelation time for the parameters:")
		print("----------------------------------------")
		acor_times = {}
		for i, p in enumerate(res.params):
			print(p, res.acor[i])
			acor_times[p] = res.acor[i]
			
		with open(outroot + '_acor_times.csv', 'w', newline='') as f:
			writer = csv.DictWriter(f, fieldnames=list(acor_times.keys()))
			writer.writeheader()
			writer.writerow(acor_times)

	# extract the highest probability fit from simulation results
	highest_prob = np.argmax(res.lnprob)
	hp_loc = np.unravel_index(highest_prob, res.lnprob.shape)
	mle_soln = res.chain[hp_loc]
	p = {}
	for i, par in enumerate(res.params.keys()):
		p[par] = mle_soln[i]

	# write model fit params to csv
	with open(outroot + '_params.csv', 'w', newline='') as f:
		writer = csv.DictWriter(f, fieldnames=list(p.keys()))
		writer.writeheader()
		writer.writerow(p)

	# save plot of model fit on data
	plt.figure()
	plt.plot(times2, data, color='black', marker='.', linestyle='')
	plt.plot(times2, model(p, times2, Cpsamples, Cptimes), color='red')
	plt.savefig(outroot + '_modelfit.png')
	plt.close()

	# save corner plot of posterior probability distribution
	plt.figure()
	corner.corner(res.flatchain, labels=res.var_names, truths=list(res.params.valuesdict().values()))
	plt.savefig(outroot + '_postprob.png')
	plt.close()


def avg_model_fit(ldconc, cbfdata, outroot):
	Cptimes, Cpsamples = np.genfromtxt(ldconc, delimiter=',', usecols=(0,1), unpack=True, skip_header=1)
	Cptimes = np.insert(Cptimes, 0, -15.)  # add a data point before the beginning
	Cpsamples = np.insert(Cpsamples, 0, Cpsamples[0])
	Cptimes = np.append(Cptimes, 121.) # add a data point to the end
	Cpsamples = np.append(Cpsamples, Cpsamples[-1])

	times2, data, count = np.genfromtxt(cbfdata, delimiter=',', usecols=(0,1,3), unpack=True, skip_header=1)
	fit_model(times2, data, Cpsamples, Cptimes, outroot)


def sub_model_fit(ldconc, cbfdata, outroot, pdvars_thresh, mcmc_steps):
	Cptimes, Cpsamples = np.genfromtxt(ldconc, delimiter=',', usecols=(2,3), unpack=True, skip_header=1)

	# run correct_bloods to add a Cp(t=0) point
	Cptimes_cb, Cpsamples_cb = mpdp.correct_bloods(Cptimes, Cpsamples, beta=mpdp.ln2over(90))

	times2, data, pdvars = np.genfromtxt(cbfdata, delimiter=',', usecols=(1,2,3), unpack=True, skip_header=1)
	print('Using pdvars threshold = {}'.format(pdvars_thresh))
	print('Using {} MCMC steps\n'.format(mcmc_steps))
	keep = np.where(pdvars < float(pdvars_thresh))
	times2 = times2[keep]
	data = data[keep]
	times2, data = [ np.array(l) for l in zip(*sorted(zip(times2,data))) ] # sort both lists by time

	fit_model(times2, data, Cpsamples_cb, Cptimes_cb, outroot, mcmc_steps)


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('ldconc', help='file containing ld concentration timing/levels')
	parser.add_argument('cbfdata', help='file containing timing and cbf data')
	parser.add_argument('--outdir', help='where to store figures')
	parser.add_argument('--data_type', choices=['sub', 'avg'], default='sub')
	parser.add_argument('--pdvars_thresh', default="5.5")
	parser.add_argument('--mcmc_steps', default="1000")
	args = parser.parse_args()

	outroot = os.path.join(args.outdir, os.path.basename(args.cbfdata).split('.')[0])

	if args.data_type == 'sub':
		sub_model_fit(args.ldconc, args.cbfdata, outroot, args.pdvars_thresh, args.mcmc_steps)
	else:
		avg_model_fit(args.ldconc, args.cbfdata, outroot)
