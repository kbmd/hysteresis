{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Effect-site modeling for MPDP study\n",
    "\n",
    "We want to estimate $C_e(t)$ at the midpoint of each CBF image frame, which in general will be at arbitrary points between the times when we drew blood. If we use piecewise linear interpolation to estimate $C_p(t)$ between blood samples (as did Unadkat *et al.*, 1986), there's a closed-form solution for $C_e$. Note, for $Cp(0)$ we can estimate $\\beta$ from the time points after the end of the loading dose, and then estimate $Cp(0)=e^{\\beta t_0}C_p(t_0)$ .\n",
    "\n",
    "$C_e$ is defined by the rate equation $ C_e^{'} = k(C_p - C_e)$ (Unadkat *et al.*, 1986). Here $C_p$ is piecewise linear, say $C_{p}(t-t_i) = C_p(t_i) + m_i(t-t_i)$ on the interval $[t_i, t_{i+1}]$, where $m_i$ is defined as $m_i=\\left(C_p(t_{i+1})-C_p(t_i)\\right)/\\left(t_{i+1}-t_i\\right)$. We can also assume that $C_e(t_0)=C_p(t_0)$, since at the time of the first blood draw, the previous dose of levodopa was given 9+ hours ago, and the longest expected $t_{\\frac{1}{2} e}$ is $<5$ hours, generally $<2.5$ hours (see Contin *et al.* 2001, Table 4).\n",
    "\n",
    "From [wxMaxima](http://maxima-online.org/?inc=r-399586830)\n",
    "\n",
    "```\n",
    "    eq1: 'diff(Ce(u), u) = k*(Cpi + m*u - Ce(u));\n",
    "    atvalue(Ce(u), u=0, Ceu0);\n",
    "    desolve(eq1, Ce(u));\n",
    "```\n",
    "\n",
    "(substituting $u = t-t_i$ , Ceu0 = $C_e(t_i)$ , and Cpi = $C_p(t_i)$),\n",
    "\n",
    "$$ C_{e}(t-t_i) = \\left(C_p(t_i)-\\frac{m_i}{k_e}\\right) + m_i(t-t_i) + \\left(C_e(t_i)-C_p(t_i)+\\frac{m_i}{k_e}\\right)e^{-k(t-t_i)} $$\n",
    "\n",
    "defined on the interval $(t_i, t_{i+1}]$ ."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import math\n",
    "ln2 = math.log(2)\n",
    "def ln2over(t):\n",
    "    return ln2/t\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.integrate import ode"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read in a sample time-concentration curve and define a piecewise linear Cp(t) function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Note, this example uses Cp(0) = Cp(t0)\n",
    "Cptimes = np.asarray([-13.95, 0, 5.33, 10.5, 12.53, 15.1, 19.25, 24.1, 30.1, 45.13, 60.13, 90.3, 128.48])\n",
    "Cpsamples = np.asarray([0,0,902.81, 2780.19, 2965.27, 2015.83, 1489.56, 1232.83, 971.8, 833.71, 707.99, 631.24, 552.62])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  t \t Cp(t)\n",
      "===== \t =====\n",
      "-13.95 \t 0.0\n",
      "-2 \t 0.0\n",
      "0 \t 0.0\n",
      "9 \t 2235.49560928\n",
      "45.13 \t 833.71\n",
      "52.63 \t 770.85\n",
      "60.13 \t 707.99\n",
      "128.48 \t 552.62\n"
     ]
    }
   ],
   "source": [
    "def m(i):\n",
    "    return (Cpsamples[i+1]-Cpsamples[i])/(Cptimes[i+1]-Cptimes[i])\n",
    "def Cp(t):\n",
    "    assert (Cptimes.size == Cpsamples.size), \\\n",
    "        \"Size of Cptimes ({0}) and Cpsamples ({1}) arrays differs\".format(Cptimes.size, Cpsamples.size)\n",
    "    #Could replace the next line with if t<t0: for short times before t0, return Cp0, or Cp0*exp{-beta*(t-t0)}\n",
    "    assert (t >= math.floor(Cptimes[0])) , \"Cp(t) evaluated at t={0}\".format(t)\n",
    "    for i in range(Cptimes.size-1):\n",
    "        assert Cptimes[i] < Cptimes[i+1], \\\n",
    "            \"Cptimes not in strictly ascending order: {0}, {1}\".format(Cptimes[i], Cptimes[i+1])\n",
    "        if t == Cptimes[i]:\n",
    "            return Cpsamples[i]\n",
    "        if t < Cptimes[i+1]:\n",
    "            return Cpsamples[i] + m(i)*(t-Cptimes[i])\n",
    "    # if we get here, t >= the last Cptimes entry\n",
    "    if t == Cptimes[-1]:\n",
    "        return (Cpsamples[-1])\n",
    "    # if we get here, t >  the last Cptimes entry\n",
    "    #TODO: replace the next line with Cpsamples[-1]*exp{beta*(t-Cptimes[-1])}\n",
    "    return (Cpsamples[-1])\n",
    "\n",
    "test = (   -13.95,     -2,    0,  9,   45.13, 52.63, 60.13, 128.48)  # , -111, 129\n",
    "print('  t', '\\t', 'Cp(t)')\n",
    "print('=====', '\\t', '=====')\n",
    "for x in test:\n",
    "    print(x, '\\t', Cp(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## First try with numerical integration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def Ceprime(t, ce, ke):\n",
    "    return ke*(Cp(t)-ce)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "t0  = Cptimes[0]\n",
    "ke_test = ln2over(20)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " i\t t\t Cp\t Ce\n",
      "==\t===\t====\t====\n",
      " 0\t-13\t   0\t   0\n",
      " 1\t  0\t   8\t   0\n",
      " 2\t  5\t 855\t  71\n",
      " 3\t 10\t2617\t 336\n",
      " 4\t 12\t2922\t 502\n",
      " 5\t 15\t2034\t 705\n",
      " 6\t 19\t1515\t 842\n",
      " 7\t 24\t1235\t 925\n",
      " 8\t 30\t 974\t 958\n",
      " 9\t 45\t 834\t 933\n",
      "10\t 60\t 709\t 866\n",
      "11\t 90\t 632\t 735\n",
      "12\t128\t 554\t 625\n"
     ]
    }
   ],
   "source": [
    "t_limit = math.ceil(Cptimes[-1])\n",
    "dt = 1.0\n",
    "printstep = 10 # how often to print integration results\n",
    "integrated = np.zeros(1+int(t_limit//printstep))\n",
    "\n",
    "r = ode(Ceprime).set_integrator('dopri5', rtol=0.01, verbosity=1)\n",
    "r.set_initial_value(Cpsamples[0],t0).set_f_params(ke_test)\n",
    "\n",
    "debug = False\n",
    "i=0\n",
    "print(' i\\t t\\t Cp\\t Ce')\n",
    "print('==\\t===\\t====\\t====')\n",
    "while r.successful() and r.t <= t_limit and i < Cptimes.size:\n",
    "    if debug:\n",
    "        if r.t>=120:\n",
    "            print('>>> t = {0}; i = {1}'.format(r.t,i))\n",
    "    r.integrate(r.t+dt)\n",
    "#    if r.t % printstep == 0:\n",
    "    if r.t >= math.floor(Cptimes[i]):\n",
    "        print('{0:>2d}\\t{1:>3.0f}\\t{2:>4.0f}\\t{3:>4.0f}'.format(\\\n",
    "            i, r.t, np.asscalar(Cp(r.t)), np.asscalar(r.y)))\n",
    "        integrated[i] = np.asscalar(r.y)\n",
    "        i=i+1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([  0.00000000e+00,   7.74071909e-03,   7.06723316e+01,\n",
       "         3.35642955e+02,   5.01816048e+02,   7.05117092e+02,\n",
       "         8.42052464e+02,   9.25134112e+02,   9.57960378e+02,\n",
       "         9.33416111e+02,   8.65586259e+02,   7.34973683e+02,\n",
       "         6.24658072e+02])"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "integrated"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Now the closed-form solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "debug2 = True\n",
    "\n",
    "def m(i):\n",
    "    return (Cpsamples[i+1]-Cpsamples[i])/(Cptimes[i+1]-Cptimes[i])\n",
    "Ce0 = Cpsamples[0]\n",
    "def Ce(t,k):\n",
    "    for i in range(Cptimes.size-1):\n",
    "        if i == 0:\n",
    "            Ceti = Ce0  # \"Ceti\" means Ce(t_i)\n",
    "        else:\n",
    "            Ceti = temp\n",
    "        xx = Cpsamples[i] - m(i)/k  # for convenience below\n",
    "        if t <= Cptimes[i+1]:\n",
    "            return xx + m(i)*(t-Cptimes[i]) + (Ceti-xx)*math.exp(-k*(t-Cptimes[i]))\n",
    "        # otherwise, t > Cptimes[i+1], so compute Ce(t_{i+1}) and increment i:\n",
    "        temp = xx + m(i)*(Cptimes[i+1]-Cptimes[i]) + (Ceti-xx)*math.exp(-k*(Cptimes[i+1]-Cptimes[i]))\n",
    "        if debug2 and (i>8):\n",
    "            print('i={}\\tt_i = {}\\tCp(t_i) = {:.0f}\\tCe(t_i) = {:.0f}\\tCe(t_[i+1]) = {:.0f}'.format(\n",
    "                i,Cptimes[i],Cpsamples[i],Ceti,temp))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "i=9\tt_i = 45.13\tCp(t_i) = 834\tCe(t_i) = 933\tCe(t_[i+1]) = 865\n",
      "i=9\tt_i = 45.13\tCp(t_i) = 834\tCe(t_i) = 933\tCe(t_[i+1]) = 865\n",
      "i=10\tt_i = 60.13\tCp(t_i) = 708\tCe(t_i) = 865\tCe(t_[i+1]) = 734\n",
      " i\t  t\t    Cp\t    Ce\n",
      "==\t=====\t ======\t ======\n",
      " 0\t-13.9\t      0\t      0\n",
      " 1\t  0.0\t      0\t      0\n",
      " 2\t  5.3\t    903\t     78\n",
      " 3\t 10.5\t   2780\t    372\n",
      " 4\t 12.5\t   2965\t    542\n",
      " 5\t 15.1\t   2016\t    708\n",
      " 6\t 19.2\t   1490\t    847\n",
      " 7\t 24.1\t   1233\t    926\n",
      " 8\t 30.1\t    972\t    958\n",
      " 9\t 45.1\t    834\t    933\n",
      "10\t 60.1\t    708\t    865\n",
      "11\t 90.3\t    631\t    734\n",
      "12\t128.5\t    553\t    624\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "array([   0.        ,    0.        ,   78.47941797,  372.28729243,\n",
       "        542.23257566,  707.66368001,  846.81485795,  925.84399086,\n",
       "        958.12737162,  933.22257533,  865.19923974,  734.09824127,\n",
       "        623.60349513])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "debug = True\n",
    "if debug:\n",
    "    check = np.asarray(list(Ce(Cptimes[i],ke_test) for i in range(Cptimes.size)))\n",
    "    print(' i\\t  t\\t    Cp\\t    Ce')\n",
    "    print('==\\t=====\\t ======\\t ======')\n",
    "    for i in range(check.size):\n",
    "        print('{0:>2d}\\t{1:>5.1f}\\t{2:>7.0f}\\t{3:>7.0f}'.format(\\\n",
    "                i,Cptimes[i], np.asscalar(Cp(Cptimes[i])), np.asscalar(check[i])))\n",
    "check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYAAAAD8CAYAAAB+UHOxAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMS4wLCBo\ndHRwOi8vbWF0cGxvdGxpYi5vcmcvpW3flQAAIABJREFUeJzt3Xl8VNX9+P/XO3tCErIPe0ICiCCr\nYXGhLCqyWFE/rk0rbh/0o/bnWkXxJ1ab1qpVa7VarBTRtGhd0Lq0IIJoqyKYgLInMQlhmZANAtmT\n8/1jZtKE7Mkkc5O8n4/HPDI5d3vPhdz3nHPuOVeMMSillOp7vDwdgFJKKc/QBKCUUn2UJgCllOqj\nNAEopVQfpQlAKaX6KE0ASinVR2kCUEqpPkoTgFJK9VGaAJRSqo/y8XQALYmKijJxcXGeDkMppXqU\nbdu25Rtjoltbz9IJIC4ujq1bt3o6DKWU6lFEJLst62kTkFJK9VGaAJRSqo9qNQGISICIbBGR7SKy\nU0R+6SwfLiJfi8h+EXlDRPyc5f7O39Ody+Pq7esBZ/leEbmwqz6UUkqp1rWlD6ACmGOMOSEivsAX\nIvIxcDfwjDFmjYi8BNwIvOj8WWSMGSEiVwO/Ba4SkTHA1cBYYBDwiYiMMsbUdMHnUqpPqqqqIjc3\nl/Lyck+HorpBQEAAQ4YMwdfXt0Pbt5oAjOOBASecv/o6XwaYA/zEWf4q8AiOBLDI+R7gLeB5ERFn\n+RpjTAXwg4ikA1OBLzsUuVKqkdzcXEJCQoiLi8PxZ6d6K2MMBQUF5ObmMnz48A7to019ACLiLSJp\nQB6wHsgAio0x1c5VcoHBzveDgQPOAKuBY0Bk/fImtlHtkJKSQlxcHF5eXsTFxZGSkuLpkJRFlJeX\nExkZqRf/PkBEiIyM7FRtr023gTqbaSaKSBjwLnB6U6u54mpmWXPlDYjIEmAJwLBhw9oSXp+SkpLC\nkiVLKC0tBSA7O5slS5YAkJSU5MnQlEXoxb/v6Oy/dbvuAjLGFAObgOlAmIi4EsgQ4JDzfS4w1Bmc\nD9AfKKxf3sQ29Y+xwhiTaIxJjI5udRxDn7Ns2bK6i79LaWkpy5Yt81BESqmeqi13AUU7v/kjIoHA\n+cBuYCNwuXO1xcB7zvfvO3/HufxTZz/C+8DVzruEhgMjgS3u+iB9RU5OTrvKlepuR44c4eqrryYh\nIYExY8awYMEC9u3b1+z6xhjmzJnD8ePHKS4u5o9//GPdsqNHjzJv3rzuCLtPaksNYCCwUUR2AN8A\n640xHwD3A3c7O3MjgVec678CRDrL7waWAhhjdgJvAruAfwK36R1A7ddcs5g2l6mOcHd/kjGGSy+9\nlFmzZpGRkcGuXbv49a9/jd1ub3abjz76iAkTJhAaGtooAURHRzNw4ED+/e9/dyou1QxjjGVfZ555\nplENvf7668bPz8/g6D8xgAkKCjKvv/66p0NTFrBr1642r/v666+boKAgt/5f2rBhg5kxY0aj8o0b\nN5oZM2aYSy65xJx++unm5ptvNjU1NcYYY6655hqzceNGY4wxV111lQkICDATJkww9957rzHGmLVr\n15r/+7//63BMvV1T/+bAVtOGa6yl5wJSjSUlJfH000/z7bffAhATE8PTTz+tHcCqkTvvvJO0tLRm\nl3/11VdUVFQ0KCstLeXGG2/k5ZdfbnKbiRMn8uyzzza7z++//54zzzyzyWVbtmxh165dxMbGMm/e\nPN555x0uv/xy/v3vf/OnP/0JgMcff5zvv/++QdyJiYk89NBDzR5TdZxOBdHDFBUV8d1333HppZcC\n8Nvf/lYv/qpDTr34t1beWVOnTiU+Ph5vb2+uueYavvjiCwAKCwsJCQlpdruYmBgOHWp0v4hyA60B\n9DBvv/02VVVV3Hnnnbz77rvk5eV5OiRlUS19UwfHbLvZ2Y0njYyNjWXTpk0dOubYsWN56623mlx2\n6i2Lrt99fHyora3Fy6vp76Pl5eUEBgZ2KB7VMq0B9DB//etfGTVqFDNmzCAoKEgTgOqw5ORkgoKC\nGpQFBQWRnJzc4X3OmTOHioqKBk1I33zzDZ999hlbtmzhhx9+oLa2ljfeeINzzz0XgNNOO43MzEwA\nQkJCKCkpabDPffv2ccYZZ3Q4JtU8TQA9yMGDB9m0aRM/+clPEBFiYmI0AagOS0pKYsWKFcTGxiIi\nxMbGsmLFik41KYoI7777LuvXrychIYGxY8fyyCOPMGjQIM466yyWLl3KGWecwfDhw+uaMRcuXFhX\n44iMjOScc87hjDPO4Be/+AUAGzduZOHChZ3+vKoxbQLqQd544w2MMVxzzTUAmgBUpyUlJbm9D2nQ\noEG8+eabDco2bdpEUFAQb7zxRqP1b7rpJq699lpuuukmwFHLre/999/nvffea7Sd6jytAfQgf/3r\nX0lMTGTUqFEA2Gy2Fu+vVqonGDhwIP/7v//L8ePHGy07evQod999N+Hh4R6IrPfTBNADpKSkMHjw\nYLZt20ZGRkbdYB2tAaieYtasWXzwwQfNLr/yyisJDQ1tVB4dHc0ll1zSlaH1adoEZHGnTv5WVFRU\nN/mbKwEYY3QCMKVUu2kNwOJamvwtJiaG6upqiouLPRSdUqon0wRgcS1N/hYTEwOg/QBKqQ7RBGBx\nLU3+ZrPZALQfQCnVIZoALC45ObnRKEjXYB1XDUATgLKS9k4HDVBWVsbMmTOpqakhKyurwa2g3333\nHdddd10XR+0eL730EqtXr+7QtsHBwW6OpnWaACwuKSmpbiKsUwfraAJQVmM6MB00wMqVK7nsssvw\n9vZulADGjRtHbm6u5Z95UV1dzS233MK1117r6VDaTBNAD5CYmAjA5s2bycrKqhu443r2qyYAZRUb\nN27E19eXW265pa5s4sSJzJgxA4Ann3ySKVOmMH78eJYvX163TkpKCosWLQJg6dKlfP7550ycOJFn\nnnkGgB//+MesWbOmxWNv2rSJWbNmcfnllzN69GiSkpJwzIzsmPcoPz8fgK1btzJr1iwAHnnkERYv\nXszcuXOJi4vjnXfe4b777mPcuHHMmzePqqoqALZt28bMmTM588wzufDCCzl8+DDguL31wQcfZObM\nmfz+97/nkUce4amnngIgPT2d888/nwkTJjB58mQyMjI4ceIE5513HpMnT2bcuHEeH+CmCaAHcH17\ncn3jd/Hx8SEqKko7gVWzZs1q/HI9b6W0tOnlq1Y5lufnN17Wmpamg163bh379+9ny5YtpKWlsW3b\nNjZv3kxlZSWZmZnExcUBjimhZ8yYQVpaGnfddRfg+BL0+eeft3r81NRUnn32WXbt2kVmZmabHiST\nkZHBhx9+yHvvvcdPf/pTZs+ezXfffUdgYCAffvghVVVV/PznP+ett95i27Zt3HDDDQ0ewVpcXMxn\nn33GPffc02C/SUlJ3HbbbWzfvp3//Oc/DBw4kICAAN59912+/fZbNm7cyD333FOXpDxBxwH0AK4L\nvKvTtz4dDKZ6inXr1rFu3TomTZoEwIkTJ9i/fz8jRowgLCysxW3bOiX01KlTGTJkCOCoeWRlZdVN\nOtec+fPn4+vry7hx46ipqal7BOW4cePIyspi7969fP/991xwwQUA1NTUMHDgwLrtr7rqqkb7LCkp\n4eDBg3XzHQUEBABQVVXFgw8+yObNm/Hy8uLgwYPY7XYGDBjQ6mfrCpoAeoC8vDz8/f2bHCmpCUC1\npKVZnYOCWl4eFdXy8qa0NB20MYYHHniAm2++uUF5UVER5eXlLe63rVNC+/v717339vamuroa+O+U\n0659NbWNl5cXvr6+dYMqvby8qK6uxhjD2LFj+fLLL5s8Zr9+/RqVNfetPiUlhaNHj7Jt2zZ8fX2J\ni4tr9bN3JW0C6gHsdjsxMTFNjvbVBKCspKXpoC+88EJWrlzJiRMnAMfstnl5eYSHh1NTU1N3IWxt\nSugtW7a0u6M1Li6Obdu2AY5narTHaaedxtGjR+sSQFVVFTt37mxxm9DQUIYMGcLatWsBx0N2SktL\nOXbsGDExMfj6+rJx48Ymn8fQnTQB9AB5eXlNNv+AIwFoH4Cyipamg547dy4/+clPOOussxg3bhyX\nX3553YV+7ty5dU8IGz9+PD4+PkyYMKGuE7j+lNA5OTntfkDM8uXLueOOO5gxYwbe3t7t2tbPz4+3\n3nqL+++/nwkTJjBx4kT+85//tLrda6+9xnPPPcf48eM5++yzOXLkCElJSWzdupXExERSUlIYPXp0\nu2Jxu7Y8ONhTL30ovMOkSZPMggULmlz2q1/9ygCmvLy8m6NSVtSeh8Jbybfffmt++tOfNrmsvLzc\nTJs2zVRVVRljjLn33nvN9u3buzM8S+vMQ+G1BtAD2O32FmsA4Jg2V6meatKkScyePZuamppGy3Jy\ncnj88cfx8XF0WT755JOMHz++u0PslbQT2OKMMa02AYGjmch194NSPdENN9zQZPnIkSMZOXJkN0fT\nN2gNwOKKioqorq5uNAbARUcDK6U6ShOAxbku7K3VALQjWCnVXq0mABEZKiIbRWS3iOwUkTuc5Y+I\nyEERSXO+FtTb5gERSReRvSJyYb3yec6ydBFZ2jUfqXdpbhSwi84IqpTqqLb0AVQD9xhjvhWREGCb\niKx3LnvGGPNU/ZVFZAxwNTAWGAR8IiKjnItfAC4AcoFvROR9Y8wud3yQ3qqlUcDgGIQSGBioCUAp\n1W6t1gCMMYeNMd8635cAu4HBLWyyCFhjjKkwxvwApANTna90Y0ymMaYSWONcV7XAdWFvrgYgIjoY\nTFmK1aaDvu6665odnewO9Seaq+/vf/87p59+OrNnz+6yY3dWu/oARCQOmAR87Sy6XUR2iMhKEQl3\nlg0GDtTbLNdZ1lz5qcdYIiJbRWSr3troqAF4eXkRFRXV7Do6GExZhenD00Gf6pVXXuGPf/wjGzdu\nbNP6rmkrulObE4CIBANvA3caY44DLwIJwETgMPA716pNbG5aKG9YYMwKY0yiMSYxOjq6reH1Wnl5\neURFRbU4etFms2kNQFmCJ6eDBnjiiScYN24cEyZMYOnSxt2MGzZsYNKkSYwbN44bbriBioqKumOO\nGTOG8ePHc++99wKOsTX/8z//w5QpU5gyZUrdzKIFBQXMnTuXSZMmcfPNNzc578+jjz7KF198wS23\n3MIvfvELysvLuf766xk3bhyTJk2qSwqrVq3iiiuu4Mc//jFz585l06ZNzJw5kyuvvJJRo0axdOlS\nUlJSmDp1KuPGjSMjI6P1f4R2aNM4ABHxxXHxTzHGvANgjLHXW/4y8IHz11xgaL3NhwCuafyaK1fN\ncM0D1JKYmBhSU1O7KSLVY9x5J6SluXefEyfCs882u7it00EbY7j44ovZvHkz06dPbzQd9FNPPcUH\nH3xQt21iYiKPP/449913X7PH/vjjj1m7di1ff/01QUFBFBYWNlheXl7Oddddx4YNGxg1ahTXXnst\nL774Itdeey3vvvsue/bsQUQoLi4G4I477uCuu+7i3HPPJScnhwsvvJDdu3fzy1/+knPPPZeHH36Y\nDz/8kBUrVjSK5eGHH+bTTz/lqaeeIjExkd/9zvH9+LvvvmPPnj3MnTu3rlnsyy+/ZMeOHURERLBp\n0ya2b9/O7t27iYiIID4+nptuuoktW7bw+9//nj/84Q8828L5b6+23AUkwCvAbmPM0/XKB9Zb7VLg\ne+f794GrRcRfRIYDI4EtwDfASBEZLiJ+ODqK33fPx+i9WhoF7OLqA2jqm4hSVlF/OujJkyezZ88e\n9u/fT35+vlumg/7kk0+4/vrrCQoKAiAiIqLB8r179zJ8+HBGjXLck7J48WI2b95MaGgoAQEB3HTT\nTbzzzjt123/yySfcfvvtTJw4kYsvvpjjx49TUlLC5s2b+elPfwrAwoULCQ8PpzVffPEFP/vZzwAY\nPXo0sbGxdQngggsuaBDrlClTGDhwIP7+/iQkJDB37lzgv9NTu1NbagDnAD8DvhMR19eJB4FrRGQi\njmacLOBmAGPMThF5E9iF4w6i24wxNQAicjvwL8AbWGmMaXlKPUVeXh7Tpk1rcZ2YmBiqqqo4duxY\nq39Iqg9x4zfFtvLkdNDGmCZnzK2/vCk+Pj5s2bKFDRs2sGbNGp5//nk+/fRTamtr+fLLL5s8bkvH\nac+xofF00vWntPby8mowXbW7+wnachfQF8YYMcaMN8ZMdL4+Msb8zBgzzll+sTHmcL1tko0xCcaY\n04wxH9cr/8gYM8q5LNmtn6SXamsNwLWuUp7kyemg586dy8qVKyktLQVo1AQ0evRosrKySE9PBxyz\ndc6cOZMTJ05w7NgxFixYwLPPPkuas9ls7ty5PP/883Xbu8p/9KMfkZKSAjianYqKilo9L/W32bdv\nHzk5OZx22mmtbtfVdCSwhZ08eZKTJ0+2mgB0MJiyCk9OBz1v3jwuvvhiEhMTmThxYt2zeV0CAgL4\ny1/+whVXXMG4cePw8vLilltuoaSkhIsuuojx48czc+bMumM+99xzbN26lfHjxzNmzBheeuklwDG1\n9ObNm5k8eTLr1q1j2LBhrZ6XW2+9lZqaGsaNG8dVV13FqlWrGnzT95i2TBnqqVdfnw46MzPTAOaV\nV15pcb3t27cbwLz11lvdFJmyKp0Ouu/pzHTQOhuohbU2CthFJ4RTPV396aBPveW5qemglXtoArCw\n1kYBu7gGiWkfgOrJdDro7qd9ABbW1hqAj48PkZGRWgNQQMt3nKjepbP/1poALKytNQDQ0cDKISAg\ngIKCAk0CfYAxhoKCAgICAjq8D20CsjC73V43SKU1OiGcAhgyZAi5ubn6iNA+IiAgoFNPAtQEYGFt\nGQPgEhMTw/bt27s4ImV1vr6+DB8+3NNhqB5Cm4AsLC8vr03NP6Azgiql2k8TgIW1pwZgs9koLi6m\nsrKyi6NSSvUWmgAsLC8vr11NQIC2/Sql2kwTgEVVVVVRUFDQriYg0MFgSqm20wRgUa5HzLW3BqD9\nAEqpttIEYFGuC7nWAJRSXUUTgEW1dRSwi84IqpRqL00AFuW6kLc1AQQHBxMQEKAJQCnVZpoALKq9\nTUAioqOBlVLtognAovLy8vD39yc0NLTN2+hgMKVUe2gCsCi73U5MTEy7nj2qNQClVHtoArCo9owC\ndtEZQZVS7aEJwKLaMwrYxVUD0KmAlVJtoQnAolxNQO0RExNDZWUlx44d66KolFK9iSYACzLGdLgG\nADoWQCnVNpoALKioqIjq6uoO1QBAE4BSqm1aTQAiMlRENorIbhHZKSJ3OMsjRGS9iOx3/gx3louI\nPCci6SKyQ0Qm19vXYuf6+0Vkcdd9rJ6tvaOAXbZt2wbAj370I+Li4khJSXF7bEqp3qMtNYBq4B5j\nzOnAdOA2ERkDLAU2GGNGAhucvwPMB0Y6X0uAF8GRMIDlwDRgKrDclTRUQ+0dBQyQkpLCY489Bjia\nkLKzs1myZIkmAaVUs1pNAMaYw8aYb53vS4DdwGBgEfCqc7VXgUuc7xcBq43DV0CYiAwELgTWG2MK\njTFFwHpgnls/TS/R3lHAAMuWLaOsrKxBWWlpKcuWLXNrbEqp3qNdfQAiEgdMAr4GbMaYw+BIEoDr\najUYOFBvs1xnWXPl6hQdqQHk5OS0q1wppdqcAEQkGHgbuNMYc7ylVZsoMy2Un3qcJSKyVUS29tWn\nW9ntdry8vIiMjGzzNsOGDWtXuVJKtSkBiIgvjot/ijHmHWex3dm0g/On69aTXGBovc2HAIdaKG/A\nGLPCGJNojEmMjo5uz2fpNex2O1FRUXh7e7d5m+TkZIKCghqUBQUFkZyc7O7wlFK9RFvuAhLgFWC3\nMebpeoveB1x38iwG3qtXfq3zbqDpwDFnE9G/gLkiEu7s/J3rLFOn6MgYgKSkJFasWMHQoY4cGxoa\nyooVK0hKSuqKEJVSvYBPG9Y5B/gZ8J2IpDnLHgQeB94UkRuBHOAK57KPgAVAOlAKXA9gjCkUkceA\nb5zrPWqMKXTLp+hlOjIKGBxJICkpibPPPrvud6WUak6rCcAY8wVNt98DnNfE+ga4rZl9rQRWtifA\nvigvL4/p06d3ePtZs2bx5JNPcuLECYKDg90YmVKqN9GRwBbU0RqAy+zZs6muruaLL75wY1RKqd5G\nE4DFnDx5kpMnT7a7D6C+s88+G19fXzZt2uS+wJRSvY4mAIvpyBiAU/Xr14+pU6eyceNGd4WllOqF\nNAFYTEdGATdl9uzZbNu2jePHWxqyoZTqyzQBWIw7agDgSAA1NTXaD6CUapYmAItxVw3grLPOws/P\nT5uBlFLN0gRgMe5KAIGBgUyfPl0TgFKqWZoALCYvL4/+/fsTEBDQ6X3Nnj2b1NRUiouL3RCZUqq3\n0QRgMZ0dA1Df7Nmzqa2t5fPPP3fL/pRSvYsmAIvpyDxAzZk2bRr+/v7aDKSUapImAItxZw0gICCA\ns88+WxOAUqpJmgAsxm63u60GAI5moO3bt1NYqPPuKaUa0gRgIVVVVRQWFro1AcyaNQtjDJs3b3bb\nPpVSvYMmAAtxPQHNXU1AAFOnTiUwMFCbgZRSjWgCsBB3jQKuz9/fn3POOUcnhlNKNaIJwELcNQjs\nVLNnz2bHjh3k5+e7db9KqZ5NE4CFuBKAO2sA4OgHAPjss8/cul+lVM+mCcBCuqIJCGDKlCn069dP\n+wGUUg1oArAQu92Ov78/ISEhbt2vr68v5557rvYDKKUa0ARgIa5RwCLNPYK542bNmsXOnTvrahlK\nKaUJwELcOQr4VLNnzwbQWoBSqo4mAAtx5zxApzrzzDMJDg7WBKCUqqMJwELcPQ1EfT4+PsyYMUM7\ngpVSdTQBWERtbS15eXld1gQEjmagPXv2cPjw4S47hlKq52g1AYjIShHJE5Hv65U9IiIHRSTN+VpQ\nb9kDIpIuIntF5MJ65fOcZekistT9H6VnKy4uprq6ustqAPDffgAdD6CUgrbVAFYB85oof8YYM9H5\n+ghARMYAVwNjndv8UUS8RcQbeAGYD4wBrnGuq5y6ahRwfZMmTSI0NFSbgZRSQBsSgDFmM9DWuYQX\nAWuMMRXGmB+AdGCq85VujMk0xlQCa5zrKqeuGgRWn7e3N/Hx8axcuRIvLy/i4uJISUnpsuMppayt\nM30At4vIDmcTUbizbDBwoN46uc6y5sqVU1dNA1FfSkoKO3fupLq6GmMM2dnZLFmyRJOAUn1URxPA\ni0ACMBE4DPzOWd7UCCbTQnkjIrJERLaKyFbX9Mh9QXc0AS1btoyqqqoGZaWlpSxbtqzLjqmUsq4O\nJQBjjN0YU2OMqQVextHEA45v9kPrrToEONRCeVP7XmGMSTTGJEZHR3ckvB4pLy8PLy8vIiMju+wY\nOTk57SpXSvVuHUoAIjKw3q+XAq47hN4HrhYRfxEZDowEtgDfACNFZLiI+OHoKH6/42H3Pna7naio\nKLy9vbvsGMOGDWtXuVKqd2vLbaB/A74EThORXBG5EXhCRL4TkR3AbOAuAGPMTuBNYBfwT+A2Z02h\nGrgd+BewG3jTua5y6spRwC7JyckEBQU1KPP39yc5OblLj6uUsiYxpsmmeEtITEw0W7du9XQY3eKs\ns84iODiY9evXd+lxUlJSWLZsGTk5OXh5eTFw4EAyMzPx9fXt0uMqpbqPiGwzxiS2tp6OBLaIrpwI\nrr6kpCSysrKora3lrbfeIjc3lz/84Q9dflyllPVoArCI7mgCOtWiRYtYsGABy5cv5+DBg916bKWU\n52kCsICTJ09y8uTJbqkB1CciPPfcc1RVVXHPPfd067GVUp6nCcACumMUcHMSEhJ44IEHeOONN9iw\nYUO3H18p5TmaACygO0YBt+T+++8nISGB2267jYqKCo/EoJTqfpoALKA7RgG3JCAggOeff569e/fy\n9NNPeyQGpVT30wRgAZ5sAnKZN28el112GY899hjZ2dkei0Mp1X00AViAqwbg6akvnn32WUSEO++8\n06NxKKW6hyYAC8jLy6N///4EBAR4NI6hQ4fy8MMPs3btWj766COPxqKU6nqaACygK58F3F533XUX\np59+Oj//+c8pKyvzdDhKqS6kCcACumsUcFv4+fnxwgsvkJmZycCBA/XBMUr1YpoALMATo4BbcujQ\nIby9vTl27Jg+OEapXkwTgAVYqQYAjgfH1NTUNCjTB8co1ftoAvCwqqoqCgsLLVUD0AfHKNU3aALw\nMNdjL62UAPTBMUr1DZoAPMzTo4Cb0tSDY0SEBx980EMRKaW6giYAD7PCKOBTJSUlsWLFCmJjYxER\nBgwYgIjw8ccfY+UHCCml2kcTgIdZsQYADR8cc/jwYZ566inWrl3LCy+84OnQlFJuognAw6xYA2jK\nnXfeycKFC7nnnntIS0vzdDhKKTfQBOBhdrudgIAAQkJCPB1Ki0SEVatWER0dzVVXXcWJEyc8HZJS\nqpM0AXiYawyAiHg6lFZFRUWRkpJCeno6t956q6fDUUp1kiYAD7PaKODWzJw5k4cffpjXXnuN1atX\nezocpVQnaALwMKuNAm6Lhx56iJkzZ3Lrrbeyd+9eT4ejlOogTQAe1tNqAADe3t6kpKQQGBjIlVde\nSXl5uadDUkp1gCYAD6qtre2RCQBg8ODBrFq1ih07dnDvvfd6OhylVAe0mgBEZKWI5InI9/XKIkRk\nvYjsd/4Md5aLiDwnIukiskNEJtfbZrFz/f0isrhrPk7PUlRURHV1dY9rAnJZuHAhd999Ny+88ALv\nvPOOp8NRSrVTW2oAq4B5p5QtBTYYY0YCG5y/A8wHRjpfS4AXwZEwgOXANGAqsNyVNPqynjIGoCW/\n+c1vmDJlCjfeeCNZWVmeDkcp1Q6tJgBjzGag8JTiRcCrzvevApfUK19tHL4CwkRkIHAhsN4YU2iM\nKQLW0zip9DlWHQXcHn5+fqxZs4ba2louuOACYmNj9SEySvUQHe0DsBljDgM4f7quYIOBA/XWy3WW\nNVfeiIgsEZGtIrLVNVNmb9UbagAA8fHxXHvttaSnp5OTk6MPkVGqh3B3J3BTo5lMC+WNC41ZYYxJ\nNMYkRkdHuzU4q3HVAHp6AgD4xz/+0ahMHyKjlLX5dHA7u4gMNMYcdjbx5DnLc4Gh9dYbAhxyls86\npXxTB4/da9jtdry8vIiIiPAzskF5AAAdDUlEQVR0KB1WU15F7qf7mJpdyXXA6YA3iZTgRwleHM/2\n4u+JdzNm+jDGTovEBIeQlhmKf1QIAdEhBMaE0G9gKMG2fnj56E1pSnWnjiaA94HFwOPOn+/VK79d\nRNbg6PA95kwS/wJ+Xa/jdy7wQMfD7h3y8vKIjo7G29vb06G0rqoK0tNh506+XrkTdu0kyr6TYeX7\niKWaN4FaIBOooIgQygjhJCGcwGfbZtgGvOCoCk5q7hjBwdT2CyGrMJQy3xAq/EKo8g+hKjCUoWND\niB0bQplvCF/vDEHCQvEJC8E3IgS/yBCGjg0lMi6EmqAQCAvD21eTiVKtaTUBiMjfcHx7jxKRXBx3\n8zwOvCkiNwI5wBXO1T8CFgDpQClwPYAxplBEHgO+ca73qDHm1I7lPiUlJYXXX3+dsrIy4uLiSE5O\nJikpydNh1aktr2TbHasx6z8h4shOYsv34muqAJiCkOM9nCMRY8kafzE+E8aSGVDIvX9eSmFZGZAB\nQFBQEC88/2diQgbw739+zLZNH2LPyCKUMxgWFsPpQ4YzPGoAMQGBjBhQRmx4CZVHS7B/dhyf8hL8\nKkrodyybwILjROeXwCclBFZUNKhKnsobKMefTL8ECsJHUDZoBIwayZgfj8B2zggYOhR6QsJVqhuI\nlR/wkZiYaLZu3erpMNwuJSWFJUuWUFpaWlcWFBTEihUrPJ8EqqrY8+BqQn//GIOqsjngNYzcsHFU\njhzLzFvHwtixVMSfjn94UKNNU1JSWLZsGTk5OQwbNqzJpJaVlcU///lPPvroIzZs2EBpaSn+/v7M\nnDmTBQsWMH/+fEaOHNns5Hi15ZUcP1jCySMllOWVUH60hMr848RGlBDpV0JxznG2f3yQgNx0Igv3\nM7g8g0D+O1K5xsePLIknP2wEpYNGwIgRBI0fwdhLRhI8Zhj4dLRSrJR1iMg2Y0xiq+tpAuh+cXFx\nZGdnNyqPjY313L301dWY11OQxx6FzEzS/KdSdOejzEyei5d318xUWlFRwebNm/n444/5+OOP2bNn\nD+C4q8iVDGbNmtXo8ZTtYWpqObr9EP2PpuOfs5/cTekc/Cyd8MJ0Bpel04//JmF8fDgeNZw9VSM4\nOWgEJmEkQeNHEDV9BMNnx+Ed4NvJT6xU99AEYGFeXl5NPlpRRKitre3eYGpqOPi7Ncijv2TQyf0w\neTI1yx+l5sIF+Pl37xTVP/zwQ10y2LBhA2VlZQQEBDBr1izmz5/PggULGDFihNuOZ2oNhTsPc/jz\ndEb7pOOTlU72J/sp35nOoNJ0QvjvMw+MtzcSF0duwAj2mxGYhBEEjBtJ5LQRDP3RcILC/NwWl1Kd\npQnAwixRA6itJf/Fv1O57BEGHdvDd17jyb7+l1z08iKwwLMJysvL62oHH330Efv27QNgxIgRdclg\n5syZBAYG1m3TliaoNjOGor15HN68nxPb05kang7p6eR+lk6ofT+h5njdqjV44R03DEaMYFfVSA4H\nJeA/NoHwyfEMOjee8KHBnToXSrWXJgALS0lJ4frrr6eqqqqurFv7ANaupeC2/5/IQ9+zS8awZcEv\nuWjlZUTFWPfOmYyMjLrawcaNGykrKyMwMJDZs2czf/58qqqqeOihh7qnX8UYjmfmc2hzOsVb0/HL\n2c/kEEeCKEndT0h1cYPVC3xtRE5JgPh4thYlUBKTQOjEeGLOSmDQJBvePp5PuKp30QRgcbNmzeLz\nzz/HGNP5b6vt8cQTcP/9nBx6GquHP8KCv1xBbHzPuiumrKyMzz77rK52kJ6e3uy6nuhXOZFTyJH/\nZFK0NYOKXRlEHctgtF8mZGRQeyAXr3pjIE/Qj+LweIb8KB4SEvjicAJeI+KJmJLAkHNiCY7QpiXV\nfpoALO7MM88kMjKSdevWddsxc5a+wLDf3o655hpk9epec8dLeno6I0eObHb5Z599xvTp0/Hz8/zF\ntOZkOUe+zib/6wxO7sigNiOTYVUZDKvMwGRmIvWerVCDFwe9hlI7PIG4OQnUDk/g84Px9J+cwIBz\nErCN6m+F1jplQZoALKyqqorg4GDuuOMOnnjiiW455oFH/8LQ5TewLnARZ2b+ncgBveuOlub6VVyC\ngoKYMWMG5513Hueddx4TJ07Ey8tiTV61tRTtPoL9Pxkc+zaDqr2Z+ORkMFIyiCzOgPz8BqsXEMGh\nwAT6T0pg2Kx4ygYnsL0kgejpCQyZOgj/QIt9PtVt2poAesdXwB5m9+7dVFZWMnHixG45Xu7v3mDQ\n8pvY5D+XEdve6HUXf4Dk5OQmx1Y888wzxMTEsGHDBjZs2MB9990HQEREBLNnz65LCC2NPeg2Xl6E\njx1E+NhBwIxGi6sLj3Pw80wKv8mgbGcmZGYQdDiDqMwt8PXfCaypYbpz3XL82e8znPzQBOLmxDPw\n3ASORSeQ45PAoLPjiBgcqLUHpTUAT1i9ejWLFy9m165dnH766V16rIMvvk/Mrf/DVt+zifrmY0ZO\n6Pg99VbXlruADh06xKefflqXEA4ccExSO2TIEM477zzmzJnDeeedx+DBTU5Wa11VVZTuyeGHTzIo\n2ZFJzd4M/HMz6F+QSTwZeJeeaLD6QRnMkX4JHI+MZ8JlCURMSaAoIoGS6HgGjYvEx1ezQ0+mTUAW\ndtddd/GnP/2JkpKSrp0HaN06ahb+mO0ykaB/f8LoKSFdd6weyBhDeno6GzZs4NNPP+XTTz+loKAA\ngNNOO62udjB79mzCw3vw84uMgaNHKdyayQ+fODqmvbMy6JeXSczxDGJqDjdY/Rih5PonUNg/nrJB\nCZx7XQJBY+MpikjAN34owWHacGB1mgAsbPbs2ZSVlfHVV1912TGqN36Oz8ILYeRISj/cSNCQnjvj\naHepra1lx44ddbWDzZs3c/LkSUSEyZMn1yWEc889t1Ojky2ntBR++IHczzLI/SwDk55BwOFMIgoz\nGFjxA37893blKnw44B1HXnA8J2ISqIlLYO4t8ciIBE7ExBMUE4zVulb6Ik0AFmWMISIigquuuoqX\nXnqpS46R98EWghadjxk4iJBvN0MPfuKYJ1VWVrJly5a6hPDVV19RVVWFn58fZ511Vl1CmDJlCr6+\nvu4diGYVNTVw8CBkZLDvnxnkf52Bb04mofkZDDiZQf/ahmMejmDjSOBwSvoNpDzMht8QGzOvtIHN\nxp4iG9WRNsJH24geHtztI837Ek0AFpWVlcXw4cN56aWXuPnmm92+/4KNO/A+fxZFJozCtZ9z5sU9\nrC3bwk6ePMnnn39elxDS0tIwxhAcHExCQgK7du3y3OA+D6nJL8I7KwMyMtixNpOy7zMItP9A8Ak7\nYRV2Imrzm9yulEDyvAZQFW5j5LmOBPHvdBslQTZ8h9gIjLMRnGDDNt6GbWSoJUan9ySaACxq7dq1\nXHrppXz11VdMmzbNrfuuLiqhKGYUFTU+HF6zmSlXDnfr/lVDBQUFbNy4kQ0bNvDnP/+Z6urqRuv4\n+/uzcOFCoqKiiI6OJioqqtH7qKgo+vXr54FP0A2qquDoUbDbSf+3neP77VQesGOO2PHOtxNj7MQF\nHAG7ndqj+Q0GydUJCACbje/zbRzzt1HW31GTwGZjyJk2zphjgwEDyKmwET2iP4FBmiw0AVjU8uXL\n+dWvfkVJSYnb25F3Xfsbxrz2IOt++SVzH57e+gbKbZqb4A9gzJgx5Ofnk5+f3+xkf4GBgU0mh+be\nR0ZG4tNLBvLVqa6m7EA+RXsciaIsy054pZ24QDvmyBG+W28n6ISd/uV2ImqO4k3jc1mBH3lio8jP\nUZuIGWdj5Dk2KiNsfLbbRkDcAIITbISdZiPmtHD6BffOZKEJwKIWLVrE/v372bVrl3t3XFJC+YA4\nvmI65xR/iG/vu9Xf0toywV9tbS3FxcV1yeDo0aOtvj9+/HijfbqEh4e3OWFER0cTEhLi+bEO7lJT\nQ8WhAmoO2QkqcdQqUv9pxxw+gle+Hf8iR7IY7GMnqCTP0Zdxikp8qQyLITjeRmmojS8zbVRF2CDG\nhtfgAQQMszF6piOJmPAI8PLqMS1ROhDMolJTU5kxo/Egn0574QUCSguZ8Z/leOvFv9s1NxAtOTm5\n7nfX858jIiIYNWpUm/ZbUVFBQUFBqwkjKyuLrVu3cvTo0Qb9EPX5+vq2K2FERkbi7+/f5nPQrZ3g\n3t74D42BoTHAOPyAadc3s25tLTVHC8nfaefYPjulmUeoPGCn9rCdkaF2gqvtkGVn7KEdRObY8aVx\nU57x9sFeE02hn43jgTbKQmxURgxg0nwbtnE2Cnxs/FDqqFlEnRZJ/wjvHpEsNAF0o4KCAg4cOOD+\nEcAnTlD7xFPI/Pl4nzXVvftWbeK60Ln7Aujv78+gQYMYNGhQm9Y3xlBSUtKmWkZaWhr5+fkUFjb/\ndNaQkJBG/RVNJY0vv/yShx9+mLKyMgCys7NZsmRJg3PjMV5eeNuisNmisM0Z2+QqQc4XxlB9tIii\n3Uc4ts/OALETfNJO4U47OV/a8SuyE1FiJ9S+i8jcI/jtcCTbSOcLHHM42Ymm0NfGsKkDCI63caDK\nxvd5NrwH2fAbaqNfvI3+o2zET4vGx99zkzFqE1A32rBhA+effz7r1q3jggsucNt+q3/9W3yWLeWZ\nq77irjXu7VhWvV91dTWFhYWNkkRLCaR+TaclAQEBLFq0iPDwcMLCwggLC6t7f+rP/v3749uT2i6N\ngeJisNsp2mPnwDY7FTl2ag7Z8cqz41to54yoI/gW2qk+ZMenuqLxLkSQqCiO+gwg44SN0hAbleE2\naqNtnHfzSPyvvrRDoWkTkAWlpaUBuLcGcOIENY8/yXrmcdq1evFX7efj40NMTAwx7RgvUlpa2iAx\nzJs3r8n1ysvLSU1NpaioiKKioibvlKovODi4yeTQUuJw/QwODu7ePg4RCA+H8HDCR48m/JLmV/Ux\nhtri4xTvs3Nsr50TGY5kMXmwHTlqp/ybI4T9YGdofjoRh+0EmjJqK86GDiaAttIE0I1SU1MZPHgw\n0dHR7tvpH/+If0kBf7It5+0L3bdbpVoSFBTEsGHDGDZsGODo7G6uE3zv3r2Ao3mqtLSU4uJiioqK\nKC4ubvC+qZ/Z2dls376doqKiFjvEAby9vZtNFi0lDtf7Lp0uXASv8P5ETOtPxLTG/T9D6/9iDKbk\nBF6lJ7suHidNAN0oLS2NSZMmuW+HJ05Q/fiTfMKFTPn5dLpyWiGlWtKWTnARoV+/fvTr169Dk+3V\n1NRw7NixJpNFcwkkNze3rvZRWVnZ4v4DAwM7VPMICwsjNDTUbdOLp/z1r93Wma4JoJuUlZWxZ88e\nLr3UjVW6F1/EpyifZO/lvHmD+3arVHt1VSd4fd7e3nV3UXVEeXl5i7WNUxPJoUOH2LVrF0VFRRw7\ndqzZcR7gSG79+/fvUM0jLCys7tnWKSkpDRJpV3emaydwN/nmm2+YOnUqb7/9Npdddlnnd3jyJAwf\nTs34SXz24L+YM6fzu1RKNa22tpbjx4+3qdmqqXVcd0c1x9/fn7CwMAoKCprsJ2nvo027pRNYRLKA\nEqAGqDbGJIpIBPAGEAdkAVcaY4rE0Tvze2ABUApcZ4z5tjPH70nc3gH88stw9Cjejy5nztnu2aVS\nqmleXl5139o7oqKiolFiaCpxvPzyy01un5OT05nwm+WOJqDZxpj6Mz4tBTYYYx4XkaXO3+8H5gMj\nna9pwIvOn31CamoqoaGhDB/uhvl5jIGXXyY9ejpfZpzNzzQBKGVp/v7+2Gw2bDZbi+utW7euyc50\nV2e7u3XFzN2LgFed718FLqlXvto4fAWEicjALji+JaWlpTFx4kT33Ka2ZQvs2sXjR2/Ebu/87pRS\n1pCcnNxojrBTO9PdqbMJwADrRGSbiCxxltmMMYcBnD9dNxcPBg7U2zbXWdbr1dTUsH37dvc1/6xc\nSYVPEGt9r+S669yzS6WU5yUlJbFixQpiY2MREWJjY7t0SvHONgGdY4w5JCIxwHoR2dPCuk199W3U\nA+1MJEug66o93S09PZ3S0lL33AJaWor529942+sKLrgslKiozu9SKWUdSUlJ3TZ9RqdqAMaYQ86f\necC7wFTA7mracf7Mc66eS8PxDkOAQ03sc4UxJtEYk+jWAVMelJqaCripA/jtt5GSEl6qvIH//d/O\n704p1Xd1OAGISD8RCXG9B+YC3wPvA4udqy0G3nO+fx+4VhymA8dcTUW9XVpaGr6+vowZM6bzO1u5\nktLBI4j76Qxmzer87pRSfVdnmoBswLvOTk0f4K/GmH+KyDfAmyJyI5ADXOFc/yMct4Cm47gNtLnJ\nW3ud1NRUxo4d2/mh5hkZsGkTQcnJrH6wB8w1q5SytA4nAGNMJjChifIC4Lwmyg1wW0eP11MZY0hN\nTeWiiy7q/M5WrcJ4eWGfey0DOr83pVQf1xW3gap6Dh8+zNGjRzvf/l9Tg1m1ik99LuTmx4a4Jzil\nVJ+mCaCLuUYAd/oOoE8+QXJzebHyBm66yQ2BKaX6PE0AXcyVACZMaNRa1j5//jPHfSPZOuDHzJ/v\nhsCUUn2eJoAulpqaSkJCAqGhoR3fSVoa5u23eanqRpJu8MdH53BVSrmBXkq6mGsKiA4zBu65h8p+\nETxZtpSvb3RfbEqpvk1rAF3o+PHjpKendy4B/OMf8Omn+P/mEXYfCSc+3n3xKaX6Nq0BdKEdO3YA\nnegArqyEe++lMv40/G6+mage9LxspZT1aQ2gC3V6CoiXXoL9+7n8h6d4bY1e/ZVS7qUJoAulpaUR\nHR3NoEGD2r9xYSHmkUf4Kvh8tg1YyMUXuz8+pVTfpk1AXSg1NbXjzwB47DFM8TGWmN/x3Cqhf3/3\nx6eU6tu0BtBFKisr2blzZ8fa//ftwzz/PKu8biT2ovG44xHCSil1Kq0BdJHdu3dTWVnZsfb/++6j\nxjeA34c8yvvPgzseIqaUUqfSGkAX6fAUEBs3wnvv4fPQA3xzYACxsV0QnFJKoQmgy6SlpREUFMTI\nkSPbvlFNDTV33M2JyGGYO++is7NHK6VUSzQBdJHU1FTGjx+Pt7d32zdavRrv79JYUvA4OzMDuy44\npZRCE0CXMMa0fwqIEyeovG8ZXzGNqNuv5owzui4+pZQC7QTuEllZWRw7dqxdCaD28Sfwyz/Mr6Pe\n5vVk7fVVSnU9TQBdoN0dwAcOUPPEU7zJVVz3p7PozMShSinVVtoE1AVSU1Px8vLijLa24yxbhhe1\nfJ/0OJde2rWxKaWUiyaALpCWlsbo0aMJCgpqfeVvvoHXXsP7nrv41etxes+/UqrbaALoAq4pIFpl\nDPmL7+ZEvxgq73mg6wNTSql6NAG4WX5+Prm5uW1q/z/52jtE7f6CZyMewytMG/6VUt1LE4CbuTqA\nW6wB2O2U3vMQ3HQj33EG8968QR/zqJTqdpoA3KzFBLBvH9x8MzVDYgl4+tf8q2oO629+m8TpevVX\nSnW/bk8AIjJPRPaKSLqILO3u43e1tLQ0hgwZQlRUVF1Z2cav+OHM/8GMHg2vvkrxosX8KmkPo757\nh7tfGuXBaJVSfVm3JgAR8QZeAOYDY4BrRGSMu4+TkpJCXFwcXl5exMXFkZKS4u5DNHvMlJQUCgoK\nSHntNXJf/AcZg39E4Jyz6P/tRj6d/iBkZxP51p94+PVROtpXKeVR3d32MBVIN8ZkAojIGmARsMtd\nB0hJSWHJkiWUlpYCkJ2dzZIlSwBISkpy12GaPaYfcHVZGZOufYAhHCSbYaye/CwjfnMjcy4IBr3N\nUyllEWKM6b6DiVwOzDPG3OT8/WfANGPM7U2tn5iYaLZu3dquY8TFxXE8O5vP6QcMOPX4+PjY8fKq\npKamHzU1EY229/U9gkgVNTXB1NSEN7H8ECI11NSEUlPjeExX/XMYQybRGNKI5oWg8/jV3tXYhujz\nfJVS3UdEthljEltbr7trAE19/22QgURkCbAEYNiwYe0+QE5ODiHALvyB6AbLAv0D6d8ffHzKqKjo\nz4kTtkbbh4XV4u1dQXl5OCdLoxstDw+rxsurirKyCErLHO38ZWWldcu3kc3rVLOeo0jZG7w85G/t\n/gxKKdUdujsB5AJD6/0+BDhUfwVjzApgBThqAO09wLBhw8jOzuZKCoGv6spjY2PJysrqQMiti4uL\nIzs7u8lYlFLKqrr7LqBvgJEiMlxE/ICrgffdeYDk5ORGUzAEBQWRnJzszsN4/JhKKdVZ3ZoAjDHV\nwO3Av4DdwJvGmJ3uPEZSUhIrVqwgNjYWESE2NpYVK1Z0WQewp46plFKd1a2dwO3VkU5gpZTq69ra\nCawjgZVSqo/SBKCUUn2UJgCllOqjNAEopVQfpQlAKaX6KEvfBSQiRwHXCKsoIN+D4bRVT4kTNNau\n0lNi7SlxgsbaXrHGmMZTGZzC0gmgPhHZ2pbbmjytp8QJGmtX6Smx9pQ4QWPtKtoEpJRSfZQmAKWU\n6qN6UgJY4ekA2qinxAkaa1fpKbH2lDhBY+0SPaYPQCmllHv1pBqAUkopN7J0AhCRJ0Vkj4jsEJF3\nRSSs3rIHnA+W3ysiF3oyThcrP/BeRIaKyEYR2S0iO0XkDmd5hIisF5H9zp+NH4PmASLiLSKpIvKB\n8/fhIvK1M843nNOJe5yIhInIW87/p7tF5CwLn9O7nP/234vI30QkwCrnVURWikieiHxfr6zJ8ygO\nzzn/znaIyGQLxNqjrlUulk4AwHrgDGPMeGAf8ACA80HyVwNjgXnAH50PnPeY7nrgfSdUA/cYY04H\npgO3OeNbCmwwxowENjh/t4I7cEwZ7vJb4BlnnEXAjR6JqrHfA/80xowGJuCI2XLnVEQGA/8fkGiM\nOQPwxvE3ZJXzugrH33J9zZ3H+cBI52sJ8GI3xeiyisax9phrVX2WTgDGmHXOZwiA4/FeQ5zvFwFr\njDEVxpgfgHQcD5z3pLoH3htjKgHXA+8twRhz2BjzrfN9CY4L1WAcMb7qXO1V4BLPRPhfIjIEWAj8\n2fm7AHOAt5yrWCXOUOBHwCsAxphKY0wxFjynTj5AoIj4AEHAYSxyXo0xm4HCU4qbO4+LgNXG4Ssg\nTEQGdk+kTcfaw65VdSydAE5xA/Cx8/1g4EC9ZbnOMk+yYkxNEpE4YBLwNWAzxhwGR5IAYjwXWZ1n\ngfuAWufvkUBxvT8wq5zbeOAo8Bdnc9WfRaQfFjynxpiDwFNADo4L/zFgG9Y8ry7NnUer/61Z/VpV\nx+MJQEQ+cbZJnvpaVG+dZTiaMFJcRU3sytO3M1kxpkZEJBh4G7jTGHPc0/GcSkQuAvKMMdvqFzex\nqhXOrQ8wGXjRGDMJOIkFmnua4mw/XwQMBwYB/XA0pZzKCue1NVb9/9BTrlV1uvuh8I0YY85vabmI\nLAYuAs4z/71ntdWHy3uAFWNqQER8cVz8U4wx7ziL7SIy0Bhz2FmNzvNchACcA1wsIguAACAUR40g\nTER8nN9WrXJuc4FcY8zXzt/fwpEArHZOAc4HfjDGHAUQkXeAs7HmeXVp7jxa8m+tB12r6ni8BtAS\nEZkH3A9cbIwprbfofeBqEfEXkeE4OoO2eCLGerr8gfed4WxHfwXYbYx5ut6i94HFzveLgfe6O7b6\njDEPGGOGGGPicJzDT40xScBG4HLnah6PE8AYcwQ4ICKnOYvOA3ZhsXPqlANMF5Eg5/8FV6yWO6/1\nNHce3weudd4NNB045moq8pQedq36L2OMZV84OkwOAGnO10v1li0DMoC9wHxPx+qMaQGOOwAygGWe\njueU2M7FUfXcUe98LsDRvr4B2O/8GeHpWOvFPAv4wPk+HscfTjrwd8Df0/E545oIbHWe17VAuFXP\nKfBLYA/wPfAa4G+V8wr8DUffRBWOb803NncecTSrvOD8O/sOx51Nno61R12rXC8dCayUUn2UpZuA\nlFJKdR1NAEop1UdpAlBKqT5KE4BSSvVRmgCUUqqP0gSglFJ9lCYApZTqozQBKKVUH/X/AO7xw9Gk\nqb2wAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x2179de085c0>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.plot(Cptimes, Cpsamples,'ko-',label='Cp(t)')\n",
    "plt.plot(Cptimes,integrated,'b--',label='Ce(t), numerical')\n",
    "plt.plot(Cptimes,     check,'r-', label='Ce(t), closed form')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Yay! It's working.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "The version of the notebook server is 5.0.0 and is running on:\n",
    "Python 3.6.3 |Anaconda, Inc.| (default, Oct 15 2017, 03:27:45) [MSC v.1900 64 bit (AMD64)]\n",
    "\n",
    "Current Kernel Information:\n",
    "Python 3.6.3 |Anaconda, Inc.| (default, Oct 15 2017, 03:27:45) [MSC v.1900 64 bit (AMD64)]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
