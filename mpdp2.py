# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 12:04:02 2019

@author: kevin
"""

import math
ln2 = math.log(2)
def ln2over(t):
    return ln2/t
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap

import pymc3 as pm
print('Running on PyMC3 v{}'.format(pm.__version__))
import theano

show_figures = False
save_figures = False
savefiles = False

# Thanks to http://stackoverflow.com/questions/2891790/pretty-printing-of-numpy-array
import contextlib
@contextlib.contextmanager
def printoptions(*args, **kwargs):
    original = np.get_printoptions()
    np.set_printoptions(*args, **kwargs)
    yield 
    np.set_printoptions(**original)

def correct_bloods(Cptimes, Cpsamples, insert_zero=True, zero_zero=False,
                   beta=0, insert_late_at=None, Cinf=600):
    """ Check blood sample times for sensible timing. Optionally add
        a sample at time zero if none was drawn. Optionally add a sample
        at time insert_late_at, after the last sample. 
        If beta > 0, correct inserted points for elimination.
    Cptimes: numpy array of times when blood was drawn
    Cpsamples: plasma concentrations at those times
    beta: terminal phase (elimination) rate constant = ln(2)/t1/2.
        Default beta = 0 (no elimination).
    insert_zero, insert_late_at: as above. If zero_zero, the 
        point added at t=0 is Cp(0)=0.0.
    Returns: new versions of Cptimes, Cpsamples
    """
    assert (Cptimes.size == Cpsamples.size),         "Sizes of Cptimes ({0}) and Cpsamples ({1}) arrays differ".format(Cptimes.size, Cpsamples.size)
    assert (np.all(Cptimes[1:] > Cptimes[:-1])),         "Cptimes not in strictly ascending order at index {0}".format(np.argmax(Cptimes[1:]<=Cptimes[:-1]))
    latest_neg = -1  # i.e. there is no t<0 so we can't correct for elimination
    no_t_0 = True
    for i in range(Cptimes.size):
        if Cptimes[i]<0: 
            if i>latest_neg:
                latest_neg = i
            if i==Cptimes.size-1 and insert_zero:
                # this is the last entry and t<0, so we have to append to the end
                Cptimes = np.append(Cptimes,0)
                Cpsamples = np.append(Cpsamples,Cpsamples[-1]*math.exp(beta*Cptimes[i]))
        elif Cptimes[i] == 0:
            no_t_0 = False
        else:  # Cptimes[i]>0
            if insert_zero:
                if i==1+latest_neg and latest_neg>-1:
                    # this is the first non-negative time after a negative time, and we want a Cp(0)
                    # make it smaller than the last level, based on decay
                    Cpsamples = np.insert(Cpsamples,i,Cpsamples[i-1]*math.exp(beta*Cptimes[i-1]))
                    Cptimes = np.insert(Cptimes,i,0)
    if insert_zero and Cptimes[0]>0:
        # we have no nonpositive times but still want a Cp(0)
        if zero_zero:
            Cpsamples = np.insert(Cpsamples,0,0.0)
        else:
            # make it larger than the first real level, based on backwards decay
            Cpsamples = np.insert(Cpsamples,0,Cpsamples[0]*math.exp(beta*Cptimes[0]))
        Cptimes = Cptimes = np.insert(Cptimes,0,0)

    if insert_late_at is not None:
        assert (insert_late_at >= Cptimes[-1]),             "{0} is not after {1}".format(insert_late_at, Cptimes[-1])
        if insert_late_at == Cptimes[-1]:
            pass  # don't duplicate last point (especially in case we already ran correct_bloods)
        else:
            Cpsamples = np.append(Cpsamples,
                Cpsamples[-1]+(Cpsamples[-1]-Cinf)*math.exp(-beta*(insert_late_at - Cptimes[-1])))
            Cptimes = np.append(Cptimes, insert_late_at)
    return Cptimes, Cpsamples

def Ce(t,k,Cptimes,Cpsamples,Ce0=None,extend=False,debug=False):
    """ Returns a numpy vector Ce(t), where
    t = numpy vector of times at which Ce is to be computed
    k = effect site rate constant (a.k.a. k_e or k_e0),
    based on blood levels Cpsamples at times Cptimes.
    If extend, assume constant Cp(t) after last point. [Not yet implemented.]
    Meant to be called once.
    """
    if k<0 or Cptimes.size==0 or Cpsamples.size==0:
        return np.full_like(t, np.nan, dtype=np.float_)
    if Ce0 == None:
        Ce0 = Cpsamples[0]
    working  = np.full_like(t, np.nan, dtype=np.float_) # will stay NaN at t values out of range
    for i in range(Cptimes.size-1):
        if i == 0:
            Ceti = Ce0  # "Ceti" means Ce(t_i)
            working[t==Cptimes[0]]=Ce0 # set Ce for any value of t == exactly the start time
        else:
            Ceti = temp
        m_i = (Cpsamples[i+1]-Cpsamples[i])/(Cptimes[i+1]-Cptimes[i])
        xx = Cpsamples[i] - m_i/k  # for convenience below
        # find elements with t in (t_i, t_{i+1}],
        indexi = np.logical_and(t>Cptimes[i], t<=Cptimes[i+1])
        # set those elements based on Ce(t_i)
        #    print(type(xx), type(m_i), type(k), type(Ceti), type(t[indexi]), 
        #          xx, m_i, k, Ceti, t[indexi])
        # <class 'theano.tensor.var.TensorVariable'> <class 'numpy.float64'> <class 'pymc3.model.TransformedRV'> <class 'numpy.float64'> <class 'numpy.ndarray'> 
        # Elemwise{sub,no_inplace}.0 -0.4223515572498139 ke 54.913771137852876 []
        working[indexi] = xx + m_i*(t[indexi]-Cptimes[i]) + (Ceti-xx)*np.exp(-k*(t[indexi]-Cptimes[i]))
        # compute Ce(t_{i+1}) ("temp") and increment i:
        temp = xx + m_i*(Cptimes[i+1]-Cptimes[i]) + (Ceti-xx)*math.exp(-k*(Cptimes[i+1]-Cptimes[i]))
    return working

def effect(ce, ec50, n):
    """E(Ce(t)), drug effect at time t, based on [LD] in effect compartment, using a sigmoid PD model.
    ce is a numpy array of effect-site [LD] concentrations.
    ec50 is the EC50 (concentration at which effect is 50%*Emax)
    n is the Hill constant.
    Units: Ce(t) and EC50 ng/ml, n dimensionless.
    Assumes that E(0) = 0 and Emax = 1 = 100%.
    """
    return ce**n/(ce**n + ec50**n)

# PK-PD parameters for Park. Dz. patients' tapping speed response to levodopa after
# a 12-hour washout (see Contin et al 2001, Table 4)
stages = ['best','I','II','III','IV','worst'] 
thalfeqs = [277,133,78,28,20,5]
keqs = [ln2over(x) for x in thalfeqs]
ec50s = [100,200,290,600,940,1200]
hillns = [1,2,5,7,18,49]

cmap = get_cmap('viridis')
colors = list(map(cmap,range(0,1+255,int(255/(len(stages)-1)))))
#colors.reverse()
lines = ['dashed','solid','dashed','solid','dashed','solid']

with open('Cp_estimation_20150304.csv', 'rb') as datafile:
    data  = np.genfromtxt(datafile, delimiter=",", usecols=range(0,7), names=True)  # imports as floats
    # the file has 7 blank rows at the end ...
    data = data[:-7]
data_times=data['new_minutes']
data_concs=data['LD']

if show_figures:
    # get_ipython().run_line_magic('matplotlib', 'inline')
    plt.scatter(data_times,data_concs,color='black')
    plt.axis((0,120,0,3500))
    plt.xlabel('time after start of infusion (min.)')  # ,fontsize=16
    plt.ylabel('Levodopa plasma concentration (ng/ml)')
    plt.show()
    plt.close()

glob_interval_end = np.asarray([2.5, 6.5, 12.5, 17.5, 27.5, 35, 45, 70, 90, 120])
# create a list of arrays with points selected from data, with one array for each interval\n",
num_globs = glob_interval_end.size
globtime  = []
globconc  = []
globsize  = np.zeros([num_globs],int)
globmeant = np.zeros([num_globs])
globmeanc = np.zeros([num_globs])
globsdt = np.zeros([num_globs])
globsdc = np.zeros([num_globs])
globchi = np.zeros([num_globs])
globclo = np.zeros([num_globs])

start = -.001
for i in range(num_globs):
    end = glob_interval_end[i]
    items = np.logical_and(data_times > start,data_times <=end)
    globtime.append(data_times[np.nonzero(items)])  # this array is now globtime[i]
    globconc.append(data_concs[np.nonzero(items)])
    globsize[i]  = globtime[i].size
    globmeant[i] = np.mean(globtime[i])
    globmeanc[i] = np.mean(globconc[i])
    globsdt[i] = np.std(globtime[i])
    globsdc[i] = np.std(globconc[i])
    # The true peak Cp has to be at or after the end of the loading dose, 10 min., so one more tweak. 
    # Let's extrapolate to a peak at 10 minutes. The first 3 points are nearly collinear (R=0.998), and 
    # the best fit line through them includes (10,2532.92). But, sigh, 2238 was already the 
    # best choice peak. Oh well, arbitrary but reasonable choice here.
    if i==2:
        globsize[i] = 14
        globmeant[i] = 10 # was (9, 2237.9)
        globmeanc[i] = 2532.92 # was (9, 2237.9)
        globsdt[i] = 1 # was 1.66
        globsdc[i] = 794.3
    start = end

# That's the medium $C_p(t)$. Now for a low and high $C_p(t)$. 
for i in range(num_globs):
    globclo[i] = np.percentile(globconc[i],10)
    globchi[i] = np.percentile(globconc[i],90)

if show_figures:
    fig, ax1 = plt.subplots()
    ax1.scatter(data_times,data_concs,color='black',label='data')
    ax1.axis((0,120,0,3500))
    plt.xlabel('time after start of infusion (min.)') 
    plt.ylabel('Levodopa plasma concentration (ng/ml)')
    # plot the globbed points
    ax1.plot(globmeant,globchi,'blue',
             markersize=8,marker=('D'),markeredgewidth=0,linewidth=2.0,
             label='high') 
    ax1.plot(globmeant,globmeanc,'coral',
             markersize=8,marker=('D'),markeredgewidth=0,linewidth=2.0,
             label='mean') 
    ax1.plot(globmeant,globclo,'red',
             markersize=8,marker=('D'),markeredgewidth=0,linewidth=2.0,
             label='low') 
    ax1.legend(loc='upper right')
    plt.show()
    if save_figures:
        fig.savefig('Cp-hi-mean-lo.png', dpi=600, transparent=True)

nicelow = globmeanc*.7

with printoptions(precision=2,formatter={'float_kind':'{:.1f}'.format}):
    print('mean times:\n', globmeant)
with printoptions(precision=2,formatter={'float_kind':'{:.0f}'.format}):
    print('high:\n', globchi)
    print('mean:\n', globmeanc)
    print('low:\n', nicelow)

##### Effect curves #####

times = np.append(np.arange(0,30),np.arange(30,121,5))
Cptimes, Cpsamples = correct_bloods(
    globmeant, globmeanc, insert_zero=True, beta=ln2over(90), insert_late_at=times[-1])

def graph_effects(graphconc, graphtime=globmeant, Cinf=None, subtitle=None, savefig=False):
    """
    graphconc is meant to be globchi, globmeanc, or globclo
    graphtime is meant to be globmeant
    """
    if Cinf == None:
        Cinf=graphconc[-1]
    Cptimes, Cpsamples = correct_bloods(
        graphtime, graphconc, insert_zero=True, zero_zero=True,
        beta=ln2over(90), insert_late_at=times[-1], Cinf=Cinf)
    if Cptimes[0]>0.0:
        Cptimes = np.insert(Cptimes,0,0.0)
        Cpsamples = np.insert(Cpsamples,0,0.0)
    
    fig, ax1 = plt.subplots()
    plt.title('Predicted effect: '+subtitle+' Cp(t)')
    ax1.set_xlabel('Time from start of loading dose (min.)')
    ax1.set_ylabel('[LD], ng/ml')
    ax1.set_ylim(0,3500)
    ax1.plot(graphtime, graphconc,'ko-',label='Cp(t)')
    #ax1.plot(Cptimes,   Cpsamples,'kx')
    ax1.legend(loc=(1.15,0.9))

    ax2 = ax1.twinx()  # instantiate a second "axes" that shares the same x-axis
    ax2.set_ylim(0,1.01)
    ax2.set_ylabel('Fractional effect')
    for i in reversed(range(len(stages))):
        ces = Ce(times, keqs[i], Cptimes, Cpsamples)
        effects = effect(ces,ec50s[i],hillns[i])
        peakwhere = np.abs(times-20.0)<0.5
        ax2.plot(times, effects, color=colors[i], linestyle=lines[i],
                 linewidth=2.0, label='effect, H&Y {}'.format(stages[i]))
    ax2.legend(loc=(1.15,0.4))
    ax2.yaxis.label.set_color(colors[3])
    ax2.tick_params(axis='y',colors=colors[3])
    ax2.spines['right'].set_color(colors[3])
    plt.show()
    temp = subtitle+'.png'
    if savefig:
        fig.savefig(temp, dpi=600, transparent=True, bbox_inches="tight")
    plt.close()

if show_figures:
    graph_effects(globchi, subtitle='high', savefig=save_figures)
    graph_effects(globmeanc, subtitle='mean', savefig=save_figures)
    graph_effects(nicelow, subtitle='low', savefig=save_figures)

##### trying a higher infusion rate, especially for more affected participants:
#graph_effects(nicelow*3/2, subtitle='low_3-2_dose', savefig=save_figures)
#graph_effects(globchi*3/2, subtitle='high_3-2_dose', savefig=save_figures)
#graph_effects(nicelow*5/4, subtitle='low_5-4_dose', savefig=save_figures)
#graph_effects(globchi*5/4, subtitle='high_5-4_dose', savefig=save_figures)

# ## Resample effect curves to fit plausible ASL timepoints
# Approximate the mean value within the (relatively short) CBF acquisition time by the midpoint value.
# Times for ASL frame start, end & midpoint, in minutes, for a ~9'40\" pCASL series
# (See Google sheet MPDP_ASL_timing for more details)

frame1start = 7/60. # first frame starts about 7 sec. after hitting start
TR = 17/60.  # 17sec., in min. (one GRASE frame--effective TR for an ASL frame is twice that)
numframes = 18  # meaning ASL frames, so twice as many GRASE frames (but really there 
    # are 1 fewer ASL frames since the first 2 GRASE frames don't produce an ASL image)
# "range(1," in the next line because the first 2 GRASE frames don't produce an ASL image
aslstarts = np.fromiter((frame1start + 2*i*TR for i in range(1, numframes)), np.float) 
aslendpts = aslstarts + 2*TR
aslmidpts = (aslstarts + aslendpts)/2
# Note: aslendpts[-1] = 10.32 min. So let's say it's 11'00\" between series, start to start.
# On average that's probably about what it will be.
# So for frame i, counting from 0, frames will start at 11*i + aslstarts.
seriesduration = 11 # minutes
# Times for ASL frame starts, in minutes, from the beginning to the end of the scan:
duration = 125 # minutes
numruns = duration // seriesduration
asl_start_all = np.concatenate([seriesduration*run + aslstarts for run in range(numruns)])
baselinestart = -12.0  # minutes before drug infusion begins
# add one baseline, pre-drug scan starting baselinestart min. before drug infusion begins
asl_start_all = np.insert(asl_start_all, 0, baselinestart + aslstarts)
asl_endpt_all = asl_start_all + 2*TR
asl_midpt_all = (asl_start_all + asl_endpt_all)/2
print('number of frames (time points) in asl_midpt_all: ', len(asl_midpt_all))
print(asl_midpt_all[:4], '...', asl_midpt_all[-4:])

# Using 50 ml/hg/min for resting rCBF, and 10 ml/hg/min rCBF response to LD in midbrain
meansize   = 50  # ml/hg/min
effectsize = 10  # ml/hg/min

# for each Cp(t), add a value of Cp(t) based on (un-)decay at the start of 
#   the baseline scan and at time = 0
# These are now cphi, cpmean and cplow:
original_cps = [globchi, globmeanc, nicelow]
cps = [[],[],[]]
beta=ln2over(90)
for i in range(len(original_cps)):
    # add a time point at zero and at 120
    # def correct_bloods(Cptimes, Cpsamples, insert_zero=True, beta=0, insert_late_at=None, Cinf=600):
    cpt, cp =  correct_bloods(globmeant, original_cps[i], beta=beta,
                             insert_late_at=120, Cinf=original_cps[i][-1])
    # add a blood level at that time point based on (un-)decay prior to zero
    cps[i] = np.insert(cp, 0, cp[0]*math.exp(-beta*baselinestart))
# add a time point at baselinestart:
cpt = np.insert(cpt, 0, baselinestart)

times = np.append(np.arange(-12,30),np.arange(30,121,5))
replicates = 100

# Save as 4-byte float for compatibility with "4dfp" image file format.
midpteffects = np.zeros((len(stages),len(cps),replicates,len(asl_midpt_all)),
                        dtype=np.float32)
for i in range(len(stages)):  # for each H&Y stage:
    for j in range(len(cps)):  # for each Cp curve j (i.e., for each voxel in first frame):
        ces = Ce(asl_midpt_all, keqs[i], cpt, cps[j]) 
        # NOTE: The corresponding time points are asl_midpt_all.
        for k in range(replicates):
            midpteffects[i,j,k,:] = meansize + \
                effectsize * effect(ces,ec50s[i],hillns[i]) # .astype(np.float32)
    if savefiles:
        with open('effect_'+stages[i]+'.4dfp.img', 'wb') as effectfile:
            # swap axes because .4dfp.img format is concat(frame0, frame1, ...)"
            np.swapaxes(midpteffects[i],0,-1).tofile(effectfile)  

if show_figures:
    for i in range(len(stages)):
        plt.plot(asl_midpt_all, midpteffects[i,1,0,:], color=colors[i], 
                 linestyle=lines[i],label='effect, H&Y {}'.format(stages[i]))
    plt.legend(loc='lower right')
    plt.show()

# ## Add noise to effect data
# 24% was mean CoV over 3 subjects for movement-smoothed CBF in a cortical region
# from pCASL data (see baseline_signal_CBF_interpolated.xlsx for details)
mySD = 0.24 * effectsize
SDs     = [mySD]  # originally had considered repeating for various SDs
# approximately a fixed ration of 10^(-1/3), which would be equidistant on a log scale
SDnames = ['24pct']

# The shape of the array is irrelevant--it could be flattened and work the same--but 
# writing it this way may help clarify the intent, i.e. to be added to the effect arrays 
noise = np.zeros((len(SDs), len(cps), replicates, len(asl_midpt_all)),dtype=np.float32) 
# 4-byte data (np.float32), for use with lab .4dfp format
for m in range(len(SDs)):
    noise[m] = np.random.normal(0.0, SDs[m], size=(len(cps), replicates, len(asl_midpt_all)) )

if savefiles:
    for m in range(len(SDs)):
        with open('noise_'+SDnames[m]+'.4dfp.img', 'wb') as f:
            noise[m].tofile(f)

##### Trying PyMC3 #####
# Fit to 
#   Cp(t) = {(globmeant, globmeanc)}, and
#   effect data generated from parameters in Contin et al. for H&Y 3 

stage = 3
# stages[stage]
# Out[22]: 'III'
testce = Ce(asl_midpt_all, keqs[stage], Cptimes, Cpsamples)
testeffect = effect(testce, ec50s[stage], hillns[stage])

def Ce_tensor(t,k,Cptimes,Cpsamples,Ce0=None,extend=False,debug=False):
    """ Returns a numpy vector Ce(t), where
    t = numpy vector of times at which Ce is to be computed
    k = effect site rate constant (a.k.a. k_e or k_e0),
    based on blood levels Cpsamples at times Cptimes.
    If extend, assume constant Cp(t) after last point. [Not yet implemented.]
    Meant to be called once.
    """
    # Theano can't deal with boolean operations on ke
    #    if k<0 or Cptimes.size==0 or Cpsamples.size==0:
    #       return np.full_like(t, np.nan, dtype=np.float_)
    if Ce0 == None:
        Ce0 = Cpsamples[0]
    working  = np.full_like(t, np.nan, dtype=np.float_) # will stay NaN at t values out of range
    for i in range(Cptimes.size-1):
        if i == 0:
            Ceti = Ce0  # "Ceti" means Ce(t_i)
            working[t==Cptimes[0]]=Ce0 # set Ce for any value of t == exactly the start time
        else:
            Ceti = temp
        m_i = (Cpsamples[i+1]-Cpsamples[i])/(Cptimes[i+1]-Cptimes[i])
        xx = Cpsamples[i] - m_i/k  # for convenience below
        # find elements with t in (t_i, t_{i+1}],
        indexi = np.logical_and(t>Cptimes[i], t<=Cptimes[i+1])
        # set those elements based on Ce(t_i)
        #    print(type(xx), type(m_i), type(k), type(Ceti), type(t[indexi]), 
        #          xx, m_i, k, Ceti, t[indexi])
        # <class 'theano.tensor.var.TensorVariable'> <class 'numpy.float64'> <class 'pymc3.model.TransformedRV'> <class 'numpy.float64'> <class 'numpy.ndarray'> 
        # Elemwise{sub,no_inplace}.0 -0.4223515572498139 ke 54.913771137852876 []
        working[indexi] = xx + m_i*(t[indexi]-Cptimes[i]) + (Ceti-xx)*theano.tensor.exp(-k*(t[indexi]-Cptimes[i]))
        # compute Ce(t_{i+1}) ("temp") and increment i:
        temp = xx + m_i*(Cptimes[i+1]-Cptimes[i]) + (Ceti-xx)*math.exp(-k*(Cptimes[i+1]-Cptimes[i]))
    return working


# NOTE: these would probably be pm.Normal variables when run
e0 = 50; emax = 10

with pm.Model() as ke_model:
    # Priors for unknown model parameters
    ke = pm.Uniform('ke', lower=0.0015, upper=0.15, testval=.01)
    n  = pm.Uniform('n',  lower=1,      upper=49,   testval=6.)
    ec50=pm.Uniform('ec50',lower=67,    upper=1800)
    # 24% was mean CoV over 3 subjects for movement-smoothed CBF in a cortical region
    # from pCASL data (see baseline_signal_CBF_interpolated.xlsx for details)
    error = pm.HalfNormal('error',sd=0.24*emax)
    # Expected value of outcome
    cbf = e0 + emax*effect(Ce_tensor(asl_midpt_all,ke,Cptimes,Cpsamples))
    # Likelihood (sampling distribution) of observations
    asl = pm.Normal('asl', mu=cbf, sd=error, observed=testeffect)

### ARRGH
    #   File "C:/Users/kevin/Documents/SourceTree/hysteresis/mpdp2.py", line 110, in Ce
    # working[indexi] = xx + m_i*(t[indexi]-Cptimes[i]) + (Ceti-xx)*np.exp(-k*(t[indexi]-Cptimes[i]))
# ValueError: setting an array element with a sequence.

