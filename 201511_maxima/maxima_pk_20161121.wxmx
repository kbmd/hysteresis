PK     d°uIñBH         mimetypetext/x-wxmathmlPK     d°uI£$®  ®  
   format.txt
This file contains a wxMaxima session.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
other files inside them can can be extracted using any .zip file viewer.
The reason why part of a .wxmx file still might seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: This way if only one line of the text changes this won't
(with a high probability) change the value of half of the bytes of the
.zip archive making the .wxmx file more version-control-friendly.
wxMaxima can be downloaded from https://github.com/andrejv/wxmaxima.
PK     d°uIZV  V     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created by wxMaxima 16.04.2   -->
<!--http://wxmaxima.sourceforge.net-->

<wxMaximaDocument version="1.4" zoom="100" activecell="30">

<cell type="title" sectioning_level="1">
<editor type="title" sectioning_level="1">
<line>Closed form solution to single-dose PK.</line>
</editor>

</cell>

<cell type="section" sectioning_level="2">
<editor type="section" sectioning_level="2">
<line>KJB, 10 Nov 2015</line>
</editor>

</cell>

<cell type="text">
<editor type="text">
<line>See www.boomer.org/c/p4/c19/c1902.html and .../c1903.html.</line>
<line>See also http://pharmacy.ufl.edu/files/2013/01/two-compartment-model.pdf re: Vd(t).</line>
<line>Piecewise fn. definitions in Maxima: www.ma.utexas.edu/pipermail/maxima/2006/003398.html</line>
<line>The trick will be to figure out reasonable prior probability distributions for the</line>
<line>  rate constants, if we apply the Bayes image toolkit.</line>
</editor>

</cell>

<cell type="section" sectioning_level="2">
<editor type="section" sectioning_level="2">
<line>Model specification</line>
</editor>

</cell>

<cell type="text">
<editor type="text">
<line>Central compartment:</line>
<line>  Mass of drug x(t), volume p (approx. plasma volume). Cp(t)=x(t)/p.</line>
<line>Peripheral / tissue compartment:</line>
<line>  Mass of drug y(t), volume v (VODss - p), C2(t)=y(t)/v.</line>
<line>Infusion rate (n) given in ng/min. (not mg/min!)</line>
<line></line>
<line>Equations below  hysteresis_variable_Cp_2.ipynb</line>
<line>===============  ==============================</line>
<line>    j               k0</line>
<line>    k               k1</line>
<line>    m               k2 </line>
<line>    n               RL</line>
<line>    x(s)            X1(t)</line>
<line>    y(s)            X2(t)</line>
<line>    c               C0</line>
<line></line>
<line>Differential equations:</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff_eq1:  &apos;diff(x(s),s) = -j*x(s) - k*x(s) + m*y(s) + n;</line>
<line>diff_eq2:  &apos;diff(y(s),s) =           k*x(s) - m*y(s);</line>
<line>ode_system1: [diff_eq1, diff_eq2];</line>
<line>atvalue (x(s), s=0, c);</line>
<line>atvalue (y(s), s=0, c);</line>
<line>atvalue (&apos;diff(x(s),s), s=0, n+(m-j-k)*c);</line>
<line>atvalue (&apos;diff(y(s),s), s=0, (k-m)*c);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(diff_eq1)</lbl><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>s</v></r></f><h>*</h><fn><fnm>x</fnm><p><v>s</v></p></fn></d><v>=</v><v>m</v><h>*</h><fn><fnm>y</fnm><p><v>s</v></p></fn><v>â</v><v>k</v><h>*</h><fn><fnm>x</fnm><p><v>s</v></p></fn><v>â</v><v>j</v><h>*</h><fn><fnm>x</fnm><p><v>s</v></p></fn><v>+</v><v>n</v><lbl userdefined="yes">(diff_eq2)</lbl><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>s</v></r></f><h>*</h><fn><fnm>y</fnm><p><v>s</v></p></fn></d><v>=</v><v>k</v><h>*</h><fn><fnm>x</fnm><p><v>s</v></p></fn><v>â</v><v>m</v><h>*</h><fn><fnm>y</fnm><p><v>s</v></p></fn><lbl userdefined="yes">(ode_system1)</lbl><t>[</t><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>s</v></r></f><h>*</h><fn><fnm>x</fnm><p><v>s</v></p></fn></d><v>=</v><v>m</v><h>*</h><fn><fnm>y</fnm><p><v>s</v></p></fn><v>â</v><v>k</v><h>*</h><fn><fnm>x</fnm><p><v>s</v></p></fn><v>â</v><v>j</v><h>*</h><fn><fnm>x</fnm><p><v>s</v></p></fn><v>+</v><v>n</v><t>,</t><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>s</v></r></f><h>*</h><fn><fnm>y</fnm><p><v>s</v></p></fn></d><v>=</v><v>k</v><h>*</h><fn><fnm>x</fnm><p><v>s</v></p></fn><v>â</v><v>m</v><h>*</h><fn><fnm>y</fnm><p><v>s</v></p></fn><t>]</t><lbl>(%o4) </lbl><v>c</v><lbl>(%o5) </lbl><v>c</v><lbl>(%o6) </lbl><v>n</v><v>+</v><v>c</v><h>*</h><p><v>m</v><v>â</v><v>k</v><v>â</v><v>j</v></p><lbl>(%o7) </lbl><v>c</v><h>*</h><p><v>k</v><v>â</v><v>m</v></p>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>sol_inf_pos:  desolve(ode_system1, [x(s), y(s)]);</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k</v><h>*</h><v>m</v><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e><st> positive, negative or zero?</st><editor type="input">
<line>positive;</line>
</editor>
<lbl userdefined="yes">(sol_inf_pos)</lbl><t>[</t><fn><fnm>x</fnm><p><v>s</v></p></fn><v>=</v><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><p><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></p></r><r><v>j</v></r></f><v>â</v><f><r><n>2</n><h>*</h><p><p><v>m</v><v>+</v><v>k</v></p><h>*</h><v>n</v><v>â</v><n>2</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p></r><r><v>j</v></r></f></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q></r></f><v>â</v><f><r><p><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><v>j</v></r></f></p><v>+</v><f><r><v>n</v></r><r><v>j</v></r></f><t>,</t><fn><fnm>y</fnm><p><v>s</v></p></fn><v>=</v><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><p><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p></r><r><v>j</v><h>*</h><v>m</v></r></f><v>â</v><f><r><n>2</n><h>*</h><p><p><v>k</v><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><v>j</v><h>*</h><v>k</v></p><h>*</h><v>n</v><v>+</v><p><v>â</v><n>2</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><v>c</v><h>*</h><e><r><v>j</v></r><r><n>2</n></r></e></p><h>*</h><v>m</v></p></r><r><v>j</v><h>*</h><v>m</v></r></f></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q></r></f><v>â</v><f><r><p><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><v>j</v><h>*</h><v>m</v></r></f></p><v>+</v><f><r><v>k</v><h>*</h><v>n</v></r><r><v>j</v><h>*</h><v>m</v></r></f><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>sol_inf_neg:  desolve(ode_system1, [x(s), y(s)]);</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k</v><h>*</h><v>m</v><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e><st> positive, negative or zero?</st><editor type="input">
<line>negative;</line>
</editor>
<lbl userdefined="yes">(sol_inf_neg)</lbl><t>[</t><fn><fnm>x</fnm><p><v>s</v></p></fn><v>=</v><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><p><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></p></r><r><v>j</v></r></f><v>â</v><f><r><n>2</n><h>*</h><p><p><v>m</v><v>+</v><v>k</v></p><h>*</h><v>n</v><v>â</v><n>2</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p></r><r><v>j</v></r></f></p><h>*</h><fn><fnm>sin</fnm><p><f><r><q><v>â</v><e><r><v>m</v></r><r><n>2</n></r></e><v>â</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>â</v><e><r><v>k</v></r><r><n>2</n></r></e><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><v>â</v><e><r><v>m</v></r><r><n>2</n></r></e><v>â</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>â</v><e><r><v>k</v></r><r><n>2</n></r></e><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><e><r><v>j</v></r><r><n>2</n></r></e></q></r></f><v>â</v><f><r><p><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></p><h>*</h><fn><fnm>cos</fnm><p><f><r><q><v>â</v><e><r><v>m</v></r><r><n>2</n></r></e><v>â</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>â</v><e><r><v>k</v></r><r><n>2</n></r></e><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><v>j</v></r></f></p><v>+</v><f><r><v>n</v></r><r><v>j</v></r></f><t>,</t><fn><fnm>y</fnm><p><v>s</v></p></fn><v>=</v><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><p><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p></r><r><v>j</v><h>*</h><v>m</v></r></f><v>â</v><f><r><n>2</n><h>*</h><p><p><v>k</v><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><v>j</v><h>*</h><v>k</v></p><h>*</h><v>n</v><v>+</v><p><v>â</v><n>2</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><v>c</v><h>*</h><e><r><v>j</v></r><r><n>2</n></r></e></p><h>*</h><v>m</v></p></r><r><v>j</v><h>*</h><v>m</v></r></f></p><h>*</h><fn><fnm>sin</fnm><p><f><r><q><v>â</v><e><r><v>m</v></r><r><n>2</n></r></e><v>â</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>â</v><e><r><v>k</v></r><r><n>2</n></r></e><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><v>â</v><e><r><v>m</v></r><r><n>2</n></r></e><v>â</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>â</v><e><r><v>k</v></r><r><n>2</n></r></e><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><e><r><v>j</v></r><r><n>2</n></r></e></q></r></f><v>â</v><f><r><p><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p><h>*</h><fn><fnm>cos</fnm><p><f><r><q><v>â</v><e><r><v>m</v></r><r><n>2</n></r></e><v>â</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>â</v><e><r><v>k</v></r><r><n>2</n></r></e><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><v>j</v><h>*</h><v>m</v></r></f></p><v>+</v><f><r><v>k</v><h>*</h><v>n</v></r><r><v>j</v><h>*</h><v>m</v></r></f><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>sol_inf_zero:  desolve(ode_system1, [x(s), y(s)]);</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k</v><h>*</h><v>m</v><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e><st> positive, negative or zero?</st><editor type="input">
<line>zero;</line>
</editor>
<lbl userdefined="yes">(sol_inf_zero)</lbl><t>[</t><fn><fnm>x</fnm><p><v>s</v></p></fn><v>=</v><p><f><r><p><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><p><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></p></r><r><v>j</v></r></f><v>â</v><f><r><n>2</n><h>*</h><p><p><v>m</v><v>+</v><v>k</v></p><h>*</h><v>n</v><v>â</v><n>2</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p></r><r><v>j</v></r></f></p><h>*</h><v>s</v></r><r><n>2</n></r></f><v>â</v><f><r><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></r><r><v>j</v></r></f></p><h>*</h><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><v>+</v><f><r><v>n</v></r><r><v>j</v></r></f><t>,</t><fn><fnm>y</fnm><p><v>s</v></p></fn><v>=</v><p><f><r><p><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><p><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p></r><r><v>j</v><h>*</h><v>m</v></r></f><v>â</v><f><r><n>2</n><h>*</h><p><p><v>k</v><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><v>j</v><h>*</h><v>k</v></p><h>*</h><v>n</v><v>+</v><p><v>â</v><n>2</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><v>c</v><h>*</h><e><r><v>j</v></r><r><n>2</n></r></e></p><h>*</h><v>m</v></p></r><r><v>j</v><h>*</h><v>m</v></r></f></p><h>*</h><v>s</v></r><r><n>2</n></r></f><v>â</v><f><r><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></r><r><v>j</v><h>*</h><v>m</v></r></f></p><h>*</h><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><v>+</v><f><r><v>k</v><h>*</h><v>n</v></r><r><v>j</v><h>*</h><v>m</v></r></f><t>]</t>
</mth></output>
</cell>

<cell type="subsection" sectioning_level="3">
<editor type="subsection" sectioning_level="3">
<line>Check out that quantity that&apos;s under a radical and in </line>
<line>the denominator.</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>decision1 : m^2+2*k*m-2*j*m+k^2+2*j*k+j^2;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(decision1)</lbl><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k</v><h>*</h><v>m</v><v>â</v><n>2</n><h>*</h><v>j</v><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>decision2 : (m+k)^2 + j^2 +2*j*(k-m);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(decision2)</lbl><e><r><p><v>m</v><v>+</v><v>k</v></p></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><p><v>k</v><v>â</v><v>m</v></p><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp( decision1 - decision2 );</line>
</editor>
</input>
<output>
<mth><lbl>(%o13) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>decision3 : (m+k)^2 + 2*j(m+k) - 4*j*m + j^2;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(decision3)</lbl><n>2</n><h>*</h><fn><fnm>j</fnm><p><v>m</v><v>+</v><v>k</v></p></fn><v>+</v><e><r><p><v>m</v><v>+</v><v>k</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>decision4 : (j+k+m)^2 - 4*j*m;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(decision4)</lbl><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(decision4-decision1);</line>
</editor>
</input>
<output>
<mth><lbl>(%o22) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>rad : (j+k+m)^2 - 4*j*m;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(rad)</lbl><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(sol_inf_pos);</line>
</editor>
</input>
<output>
<mth><lbl>(%o28) </lbl><t>[</t><fn><fnm>x</fnm><p><v>s</v></p></fn><v>=</v><v>â</v><f><r><e><r><s>%e</s></r><r><v>â</v><f><r><v>m</v><h>*</h><v>s</v></r><r><n>2</n></r></f><v>â</v><f><r><v>k</v><h>*</h><v>s</v></r><r><n>2</n></r></f><v>â</v><f><r><v>j</v><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><p><p><v>m</v><v>+</v><v>k</v><v>â</v><v>j</v></p><h>*</h><v>n</v><v>â</v><n>3</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v><v>+</v><v>c</v><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><v>c</v><h>*</h><e><r><v>j</v></r><r><n>2</n></r></e></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>+</v><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><p><p><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>â</v><v>n</v><h>*</h><e><r><s>%e</s></r><r><f><r><v>m</v><h>*</h><v>s</v></r><r><n>2</n></r></f><v>+</v><f><r><v>k</v><h>*</h><v>s</v></r><r><n>2</n></r></f><v>+</v><f><r><v>j</v><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e></p></p></r><r><v>j</v><h>*</h><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q></r></f><t>,</t><fn><fnm>y</fnm><p><v>s</v></p></fn><v>=</v><v>â</v><f><r><e><r><s>%e</s></r><r><v>â</v><f><r><v>m</v><h>*</h><v>s</v></r><r><n>2</n></r></f><v>â</v><f><r><v>k</v><h>*</h><v>s</v></r><r><n>2</n></r></f><v>â</v><f><r><v>j</v><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><p><p><v>k</v><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><v>j</v><h>*</h><v>k</v></p><h>*</h><v>n</v><v>+</v><v>c</v><h>*</h><v>j</v><h>*</h><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><v>â</v><n>3</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><v>c</v><h>*</h><e><r><v>j</v></r><r><n>2</n></r></e></p><h>*</h><v>m</v></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>+</v><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><p><p><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>â</v><v>k</v><h>*</h><v>n</v><h>*</h><e><r><s>%e</s></r><r><f><r><v>m</v><h>*</h><v>s</v></r><r><n>2</n></r></f><v>+</v><f><r><v>k</v><h>*</h><v>s</v></r><r><n>2</n></r></f><v>+</v><f><r><v>j</v><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e></p></p></r><r><v>j</v><h>*</h><v>m</v><h>*</h><q><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><n>2</n><h>*</h><v>k</v><v>â</v><n>2</n><h>*</h><v>j</v></p><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><e><r><v>j</v></r><r><n>2</n></r></e></q></r></f><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>sol_pos: [x(s)=-(%e^(-s*(j+k+m)/2)*(((m+k-j)*n-3*c*j*m+c*j*k+c*j^2)*</line>
<line>            sinh((sqrt(rad)*s)/2)+sqrt(rad)*((n-c*j)*cosh((sqrt(rad)*s)/2)</line>
<line>                -n*%e^((j+k+m)*s/2))))/(j*sqrt(rad)),</line>
<line>    y(s)=-(%e^(-s*(j+k+m)/2)*(((k*m+k^2+j*k)*n+c*j*m^2+(-3*c*j*k-c*j^2)*m)*</line>
<line>sinh((sqrt(rad)*s)/2)+sqrt(rad)*((k*n-c*j*m)*cosh((sqrt(rad)*s)/2)</line>
<line>                -k*n*%e^((j+k+m)*s/2))))/(j*m*sqrt(rad))];</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(sol_pos)</lbl><t>[</t><fn><fnm>x</fnm><p><v>s</v></p></fn><v>=</v><v>â</v><f><r><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><p><p><v>m</v><v>+</v><v>k</v><v>â</v><v>j</v></p><h>*</h><v>n</v><v>â</v><n>3</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v><v>+</v><v>c</v><h>*</h><v>j</v><h>*</h><v>k</v><v>+</v><v>c</v><h>*</h><e><r><v>j</v></r><r><n>2</n></r></e></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>+</v><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><p><p><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>â</v><v>n</v><h>*</h><e><r><s>%e</s></r><r><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e></p></p></r><r><v>j</v><h>*</h><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q></r></f><t>,</t><fn><fnm>y</fnm><p><v>s</v></p></fn><v>=</v><v>â</v><f><r><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><p><p><v>k</v><h>*</h><v>m</v><v>+</v><e><r><v>k</v></r><r><n>2</n></r></e><v>+</v><v>j</v><h>*</h><v>k</v></p><h>*</h><v>n</v><v>+</v><v>c</v><h>*</h><v>j</v><h>*</h><e><r><v>m</v></r><r><n>2</n></r></e><v>+</v><p><v>â</v><n>3</n><h>*</h><v>c</v><h>*</h><v>j</v><h>*</h><v>k</v><v>â</v><v>c</v><h>*</h><e><r><v>j</v></r><r><n>2</n></r></e></p><h>*</h><v>m</v></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>+</v><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><p><p><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>â</v><v>k</v><h>*</h><v>n</v><h>*</h><e><r><s>%e</s></r><r><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e></p></p></r><r><v>j</v><h>*</h><v>m</v><h>*</h><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q></r></f><t>]</t>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>Use Q for rad so it doesn&apos;t substitute for me ...</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>test1: [x(s)=-(%e^(-s*(j+k+m)/2)*(((m+k-j)*n + c*j*(j+k-3*m))*</line>
<line>            sinh((sqrt(Q)*s)/2)+sqrt(Q)*((n-c*j)*cosh((sqrt(Q)*s)/2)</line>
<line>                -n*%e^((j+k+m)*s/2))))/(j*sqrt(Q)),</line>
<line>    y(s)=-(%e^(-s*(j+k+m)/2)*((k*n*(m+k+j) + c*j*m*(m-3*k-j))*sinh((sqrt(Q)*s)/2)</line>
<line>                + sqrt(Q)*((k*n-c*j*m)*cosh((sqrt(Q)*s)/2)</line>
<line>                -k*n*%e^((j+k+m)*s/2))))/(j*m*sqrt(Q))];</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(test1)</lbl><t>[</t><fn><fnm>x</fnm><p><v>s</v></p></fn><v>=</v><v>â</v><f><r><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><p><p><v>m</v><v>+</v><v>k</v><v>â</v><v>j</v></p><h>*</h><v>n</v><v>+</v><v>c</v><h>*</h><v>j</v><h>*</h><p><v>â</v><n>3</n><h>*</h><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>+</v><q><v>Q</v></q><h>*</h><p><p><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>â</v><v>n</v><h>*</h><e><r><s>%e</s></r><r><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e></p></p></r><r><q><v>Q</v></q><h>*</h><v>j</v></r></f><t>,</t><fn><fnm>y</fnm><p><v>s</v></p></fn><v>=</v><v>â</v><f><r><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><p><v>k</v><h>*</h><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>n</v><v>+</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v><h>*</h><p><v>m</v><v>â</v><n>3</n><h>*</h><v>k</v><v>â</v><v>j</v></p></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>+</v><q><v>Q</v></q><h>*</h><p><p><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn><v>â</v><v>k</v><h>*</h><v>n</v><h>*</h><e><r><s>%e</s></r><r><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e></p></p></r><r><q><v>Q</v></q><h>*</h><v>j</v><h>*</h><v>m</v></r></f><t>]</t>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>Simplify that by human, and check: </line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>test2: [x(s)=-(%e^(-s*(j+k+m)/2)*(((m+k-j)*n + c*j*(j+k-3*m))*sinh((sqrt(Q)*s)/2)/sqrt(Q)</line>
<line>            +((n-c*j)*cosh((sqrt(Q)*s)/2)))/j-n/j),</line>
<line>    y(s)=-(%e^(-s*(j+k+m)/2)*((k*n*(m+k+j) + c*j*m*(m-3*k-j))*sinh((sqrt(Q)*s)/2)/sqrt(Q)</line>
<line>                + ((k*n-c*j*m)*cosh((sqrt(Q)*s)/2)))-k*n)/(j*m)];</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(test2)</lbl><t>[</t><fn><fnm>x</fnm><p><v>s</v></p></fn><v>=</v><f><r><v>n</v></r><r><v>j</v></r></f><v>â</v><f><r><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><p><v>m</v><v>+</v><v>k</v><v>â</v><v>j</v></p><h>*</h><v>n</v><v>+</v><v>c</v><h>*</h><v>j</v><h>*</h><p><v>â</v><n>3</n><h>*</h><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><v>Q</v></q></r></f><v>+</v><p><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></p></r><r><v>j</v></r></f><t>,</t><fn><fnm>y</fnm><p><v>s</v></p></fn><v>=</v><f><r><v>k</v><h>*</h><v>n</v><v>â</v><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><v>k</v><h>*</h><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>n</v><v>+</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v><h>*</h><p><v>m</v><v>â</v><n>3</n><h>*</h><v>k</v><v>â</v><v>j</v></p></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><v>Q</v></q></r></f><v>+</v><p><v>k</v><h>*</h><v>n</v><v>â</v><v>c</v><h>*</h><v>j</v><h>*</h><v>m</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></p></r><r><v>j</v><h>*</h><v>m</v></r></f><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(test2 - test1);</line>
</editor>
</input>
<output>
<mth><lbl>(%o56) </lbl><t>[</t><n>0</n><v>=</v><n>0</n><t>,</t><n>0</n><v>=</v><n>0</n><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>test3: [x(s)=-(%e^(-s*(j+k+m)/2)*(((m+k-j)*n/j + c*(j+k-3*m))*sinh((sqrt(Q)*s)/2)/sqrt(Q)</line>
<line>            +((n/j-c)*cosh((sqrt(Q)*s)/2)))-n/j),</line>
<line>    y(s)=-(%e^(-s*(j+k+m)/2)*((k*n*(m+k+j)/(j*m) + c*(m-3*k-j))*sinh((sqrt(Q)*s)/2)/sqrt(Q)</line>
<line>                + ((k*n/(j*m)-c)*cosh((sqrt(Q)*s)/2)))-k*n/(j*m))];</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(test3)</lbl><t>[</t><fn><fnm>x</fnm><p><v>s</v></p></fn><v>=</v><f><r><v>n</v></r><r><v>j</v></r></f><v>â</v><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><f><r><p><v>m</v><v>+</v><v>k</v><v>â</v><v>j</v></p><h>*</h><v>n</v></r><r><v>j</v></r></f><v>+</v><v>c</v><h>*</h><p><v>â</v><n>3</n><h>*</h><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><v>Q</v></q></r></f><v>+</v><p><f><r><v>n</v></r><r><v>j</v></r></f><v>â</v><v>c</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></p><t>,</t><fn><fnm>y</fnm><p><v>s</v></p></fn><v>=</v><f><r><v>k</v><h>*</h><v>n</v></r><r><v>j</v><h>*</h><v>m</v></r></f><v>â</v><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><f><r><v>k</v><h>*</h><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>n</v></r><r><v>j</v><h>*</h><v>m</v></r></f><v>+</v><v>c</v><h>*</h><p><v>m</v><v>â</v><n>3</n><h>*</h><v>k</v><v>â</v><v>j</v></p></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><v>Q</v></q></r></f><v>+</v><p><f><r><v>k</v><h>*</h><v>n</v></r><r><v>j</v><h>*</h><v>m</v></r></f><v>â</v><v>c</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><v>Q</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></p><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(test3-test1);</line>
</editor>
</input>
<output>
<mth><lbl>(%o66) </lbl><t>[</t><n>0</n><v>=</v><n>0</n><t>,</t><n>0</n><v>=</v><n>0</n><t>]</t>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>Same but with rad instead of Q:</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>test4: [x(s)=-(%e^(-s*(j+k+m)/2)*(((m+k-j)*n/j + c*(j+k-3*m))*sinh((sqrt(rad)*s)/2)/sqrt(rad)</line>
<line>            +((n/j-c)*cosh((sqrt(rad)*s)/2)))-n/j),</line>
<line>    y(s)=-(%e^(-s*(j+k+m)/2)*((k*n*(m+k+j)/(j*m) + c*(m-3*k-j))*sinh((sqrt(rad)*s)/2)/sqrt(rad)</line>
<line>                + ((k*n/(j*m)-c)*cosh((sqrt(rad)*s)/2)))-k*n/(j*m))];</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes">(test4)</lbl><t>[</t><fn><fnm>x</fnm><p><v>s</v></p></fn><v>=</v><f><r><v>n</v></r><r><v>j</v></r></f><v>â</v><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><f><r><p><v>m</v><v>+</v><v>k</v><v>â</v><v>j</v></p><h>*</h><v>n</v></r><r><v>j</v></r></f><v>+</v><v>c</v><h>*</h><p><v>â</v><n>3</n><h>*</h><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q></r></f><v>+</v><p><f><r><v>n</v></r><r><v>j</v></r></f><v>â</v><v>c</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></p><t>,</t><fn><fnm>y</fnm><p><v>s</v></p></fn><v>=</v><f><r><v>k</v><h>*</h><v>n</v></r><r><v>j</v><h>*</h><v>m</v></r></f><v>â</v><e><r><s>%e</s></r><r><v>â</v><f><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>s</v></r><r><n>2</n></r></f></r></e><h>*</h><p><f><r><p><f><r><v>k</v><h>*</h><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p><h>*</h><v>n</v></r><r><v>j</v><h>*</h><v>m</v></r></f><v>+</v><v>c</v><h>*</h><p><v>m</v><v>â</v><n>3</n><h>*</h><v>k</v><v>â</v><v>j</v></p></p><h>*</h><fn><fnm>sinh</fnm><p><f><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></r><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q></r></f><v>+</v><p><f><r><v>k</v><h>*</h><v>n</v></r><r><v>j</v><h>*</h><v>m</v></r></f><v>â</v><v>c</v></p><h>*</h><fn><fnm>cosh</fnm><p><f><r><q><e><r><p><v>m</v><v>+</v><v>k</v><v>+</v><v>j</v></p></r><r><n>2</n></r></e><v>â</v><n>4</n><h>*</h><v>j</v><h>*</h><v>m</v></q><h>*</h><v>s</v></r><r><n>2</n></r></f></p></fn></p><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(test4-sol_inf_pos);</line>
</editor>
</input>
<output>
<mth><lbl>(%o68) </lbl><t>[</t><n>0</n><v>=</v><n>0</n><t>,</t><n>0</n><v>=</v><n>0</n><t>]</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
</editor>
</input>
</cell>

</wxMaximaDocument>PK      d°uIñBH                       mimetypePK      d°uI£$®  ®  
             5   format.txtPK      d°uIZV  V                 content.xmlPK      §       