PK     τBKρBH         mimetypetext/x-wxmathmlPK     τBKΰΙnA5  5  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/andrejv/wxmaxima.
It also is part of the windows installer for maxima
(http://maxima.sourceforge.net).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using an text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     τBKnγ"Ψ#  #     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created by wxMaxima 17.05.0   -->
<!--http://wxmaxima.sourceforge.net-->

<wxMaximaDocument version="1.5" zoom="100" activecell="37">

<cell type="title" sectioning_level="1">
<editor type="title" sectioning_level="1">
<line>Closed form solution to single-dose PK.</line>
</editor>

</cell>

<cell type="section" sectioning_level="2">
<editor type="section" sectioning_level="2">
<line>KJB, 22 Sep 2017 and earlier</line>
</editor>

</cell>

<cell type="text">
<editor type="text">
<line>See www.boomer.org/c/p4/c19/c1902.html and .../c1903.html.</line>
<line>See also http://pharmacy.ufl.edu/files/2013/01/two-compartment-model.pdf re: Vd(t).</line>
<line>Piecewise fn. definitions in Maxima: www.ma.utexas.edu/pipermail/maxima/2006/003398.html</line>
<line>The trick will be to figure out reasonable prior probability distributions for the</line>
<line>  rate constants, if we apply the Bayes image toolkit.</line>
</editor>

</cell>

<cell type="section" sectioning_level="2">
<editor type="section" sectioning_level="2">
<line>Model specification</line>
</editor>

</cell>

<cell type="text">
<editor type="text">
<line>Central compartment:</line>
<line>  Mass of drug x1(t), volume v1 (approx. plasma volume). Cp(t) ~ c1(t)=x1(t)/v1.</line>
<line>Peripheral / tissue compartment:</line>
<line>  Mass of drug x2(t), volume v2 (VODss - v1), c2(t)=x2(t)/v2.</line>
<line>Infusion rate RL or RM given in ng/min. (not mg/min!)</line>
<line>x10 = x1(0). x20 = x2(0).</line>
<line>Note:Β at real t=0, x1(t)=C0*v1, x2(t)=C0*v2. </line>
<line>But here I&apos;m using a more general model to make it simpler overall for the piecewise definition (since rate changes).</line>
<line>Differential equations:</line>
</editor>

</cell>

<cell type="subsection" sectioning_level="3">
<editor type="subsection" sectioning_level="3">
<line>General solution:</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff_eq1:  &apos;diff(x1(t),t) = -k0*x1(t) - k1*x1(t) + k2*x2(t) + R;</line>
<line>diff_eq2:  &apos;diff(x2(t),t) =             k1*x1(t) - k2*x2(t);</line>
<line>ode_system1: [diff_eq1, diff_eq2];</line>
<line>atvalue (x1(t), t=0, x10);</line>
<line>atvalue (x2(t), t=0, x20);</line>
<line>atvalue (&apos;diff(x1(t),t), t=0, -(k0+k1)*x10 + k2*x20 + R);</line>
<line>atvalue (&apos;diff(x2(t),t), t=0,       k1*x10 - k2*x20);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="diff_eq1">(%o1) </lbl><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn></d><v>=</v><v>k2</v><h>*</h><fn><r><fnm>x2</fnm></r><r><p><v>t</v></p></r></fn><v>β</v><v>k1</v><h>*</h><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn><v>β</v><v>k0</v><h>*</h><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn><v>+</v><v>R</v><lbl userdefined="yes" userdefinedlabel="diff_eq2">(%o2) </lbl><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><fn><r><fnm>x2</fnm></r><r><p><v>t</v></p></r></fn></d><v>=</v><v>k1</v><h>*</h><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn><v>β</v><v>k2</v><h>*</h><fn><r><fnm>x2</fnm></r><r><p><v>t</v></p></r></fn><lbl userdefined="yes" userdefinedlabel="ode_system1">(%o3) </lbl><t>[</t><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn></d><v>=</v><v>k2</v><h>*</h><fn><r><fnm>x2</fnm></r><r><p><v>t</v></p></r></fn><v>β</v><v>k1</v><h>*</h><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn><v>β</v><v>k0</v><h>*</h><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn><v>+</v><v>R</v><t>,</t><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><fn><r><fnm>x2</fnm></r><r><p><v>t</v></p></r></fn></d><v>=</v><v>k1</v><h>*</h><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn><v>β</v><v>k2</v><h>*</h><fn><r><fnm>x2</fnm></r><r><p><v>t</v></p></r></fn><t>]</t><lbl>(%o4) </lbl><v>x10</v><lbl>(%o5) </lbl><v>x20</v><lbl>(%o6) </lbl><v>k2</v><h>*</h><v>x20</v><v>+</v><r><p><v>β</v><v>k1</v><v>β</v><v>k0</v></p></r><h>*</h><v>x10</v><v>+</v><v>R</v><lbl>(%o7) </lbl><v>k1</v><h>*</h><v>x10</v><v>β</v><v>k2</v><h>*</h><v>x20</v>
</mth></output>
</cell>

<cell type="code" answer1="positive;" hide="true">
<input>
<editor type="input">
<line>sol_pos_ugly:Β desolve(ode_system1, [x1(t), x2(t)]);</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><e><r><v>k2</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k1</v><h>*</h><v>k2</v><v>β</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k2</v><v>+</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e><st> positive, negative or zero?</st><editor type="input">
<line>positive;</line>
</editor>
<lbl userdefined="yes" userdefinedlabel="sol_pos_ugly">(%o26) </lbl><t>[</t><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><fn><r><fnm>sinh</fnm></r><r><p><f><r><q><e><r><v>k2</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>+</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><f><r><n>2</n><h>*</h><r><p><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x10</v><v>β</v><v>R</v><h>*</h><v>k2</v><v>β</v><v>R</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v></r></f><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><r><p><v>k0</v><h>*</h><v>x10</v><v>β</v><v>R</v></p></r></r><r><v>k0</v></r></f></p></r></r><r><q><e><r><v>k2</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>+</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e></q></r></f><v>+</v><f><r><fn><r><fnm>cosh</fnm></r><r><p><f><r><q><e><r><v>k2</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>+</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><v>k0</v><h>*</h><v>x10</v><v>β</v><v>R</v></p></r></r><r><v>k0</v></r></f></p></r><v>+</v><f><r><v>R</v></r><r><v>k0</v></r></f><t>,</t><fn><r><fnm>x2</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><fn><r><fnm>sinh</fnm></r><r><p><f><r><q><e><r><v>k2</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>+</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><f><r><n>2</n><h>*</h><r><p><r><p><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e></p></r><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><v>k0</v><h>*</h><v>k1</v><h>*</h><v>k2</v><h>*</h><v>x10</v><v>β</v><v>R</v><h>*</h><v>k1</v><h>*</h><v>k2</v><v>β</v><v>R</v><h>*</h><e><r><v>k1</v></r><r><n>2</n></r></e><v>β</v><v>R</v><h>*</h><v>k0</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><r><p><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x20</v><v>β</v><v>R</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r></r><r><q><e><r><v>k2</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>+</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e></q></r></f><v>+</v><f><r><fn><r><fnm>cosh</fnm></r><r><p><f><r><q><e><r><v>k2</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>+</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x20</v><v>β</v><v>R</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r><v>+</v><f><r><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f><t>]</t>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>Make that ugly quantity under the radical easier to write:</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>rad:Β (k0+k1+k2)^2 - 4*k0*k2;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="rad">(%o16) </lbl><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e><v>β</v><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(rad - (k2^2+2*k1*k2-2*k0*k2+k1^2+2*k0*k1+k0^2));</line>
</editor>
</input>
<output>
<mth><lbl>(%o17) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>Simplify the nasty solution (on paper) and check here:</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>sol_pos_1:Β %e^(-(k0+k1+k2)*t/2)*( </line>
<line>    sinh(t*sqrt(rad)/2)*((k0-k1-k2)*R/k0 + x10*(k2-k1-k0) + 2*x20*k2)/sqrt(rad) </line>
<line>    + cosh(t*sqrt(rad)/2)*(x10 - R/k0)Β ) +Β R/k0;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="sol_pos_1">(%o24) </lbl><e><r><s>%e</s></r><r><f><r><r><p><v>β</v><v>k2</v><v>β</v><v>k1</v><v>β</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><fn><r><fnm>sinh</fnm></r><r><p><f><r><q><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e><v>β</v><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><n>2</n><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><r><p><v>k2</v><v>β</v><v>k1</v><v>β</v><v>k0</v></p></r><h>*</h><v>x10</v><v>+</v><f><r><v>R</v><h>*</h><r><p><v>β</v><v>k2</v><v>β</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><v>k0</v></r></f></p></r></r><r><q><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e><v>β</v><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v></q></r></f><v>+</v><fn><r><fnm>cosh</fnm></r><r><p><f><r><q><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e><v>β</v><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><v>x10</v><v>β</v><f><r><v>R</v></r><r><v>k0</v></r></f></p></r></p></r><v>+</v><f><r><v>R</v></r><r><v>k0</v></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(sol_pos_1 - rhs(sol_pos_ugly[1]));</line>
</editor>
</input>
<output>
<mth><lbl>(%o25) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="code" hide="true">
<input>
<editor type="input">
<line>sol_pos_2:Β %e^(-(k0+k1+k2)*t/2)*( sinh(t*sqrt(rad)/2)*</line>
<line>    (x20*(k0+k1-k2)+2*k1*x10-R*k1*(k0+k1+k2)/(k0*k2))/sqrt(rad) </line>
<line>     + cosh(t*sqrt(rad)/2)*(x20-R*k1/(k0*k2))Β ) +Β R*k1/(k0*k2);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="sol_pos_2">(%o29) </lbl><e><r><s>%e</s></r><r><f><r><r><p><v>β</v><v>k2</v><v>β</v><v>k1</v><v>β</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><fn><r><fnm>sinh</fnm></r><r><p><f><r><q><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e><v>β</v><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><r><p><v>β</v><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>x20</v><v>+</v><n>2</n><h>*</h><v>k1</v><h>*</h><v>x10</v><v>β</v><f><r><v>R</v><h>*</h><v>k1</v><h>*</h><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r></r><r><q><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e><v>β</v><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v></q></r></f><v>+</v><fn><r><fnm>cosh</fnm></r><r><p><f><r><q><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e><v>β</v><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><v>x20</v><v>β</v><f><r><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r></p></r><v>+</v><f><r><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(sol_pos_2 - rhs(sol_pos_ugly[2]));</line>
</editor>
</input>
<output>
<mth><lbl>(%o30) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>Now for when the quantity under the radical is negative.</line>
</editor>

</cell>

<cell type="code" answer1="negative;" hide="true">
<input>
<editor type="input">
<line>sol_neg_ugly:Β desolve(ode_system1, [x1(t), x2(t)]);</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><e><r><v>k2</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k1</v><h>*</h><v>k2</v><v>β</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k2</v><v>+</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e><st> positive, negative or zero?</st><editor type="input">
<line>negative;</line>
</editor>
<lbl userdefined="yes" userdefinedlabel="sol_neg_ugly">(%o31) </lbl><t>[</t><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><f><r><q><v>β</v><e><r><v>k2</v></r><r><n>2</n></r></e><v>β</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>β</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>β</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>β</v><e><r><v>k0</v></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><f><r><n>2</n><h>*</h><r><p><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x10</v><v>β</v><v>R</v><h>*</h><v>k2</v><v>β</v><v>R</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v></r></f><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><r><p><v>k0</v><h>*</h><v>x10</v><v>β</v><v>R</v></p></r></r><r><v>k0</v></r></f></p></r></r><r><q><v>β</v><e><r><v>k2</v></r><r><n>2</n></r></e><v>β</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>β</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>β</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>β</v><e><r><v>k0</v></r><r><n>2</n></r></e></q></r></f><v>+</v><f><r><fn><r><fnm>cos</fnm></r><r><p><f><r><q><v>β</v><e><r><v>k2</v></r><r><n>2</n></r></e><v>β</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>β</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>β</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>β</v><e><r><v>k0</v></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><v>k0</v><h>*</h><v>x10</v><v>β</v><v>R</v></p></r></r><r><v>k0</v></r></f></p></r><v>+</v><f><r><v>R</v></r><r><v>k0</v></r></f><t>,</t><fn><r><fnm>x2</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><f><r><q><v>β</v><e><r><v>k2</v></r><r><n>2</n></r></e><v>β</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>β</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>β</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>β</v><e><r><v>k0</v></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><f><r><n>2</n><h>*</h><r><p><r><p><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e></p></r><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><v>k0</v><h>*</h><v>k1</v><h>*</h><v>k2</v><h>*</h><v>x10</v><v>β</v><v>R</v><h>*</h><v>k1</v><h>*</h><v>k2</v><v>β</v><v>R</v><h>*</h><e><r><v>k1</v></r><r><n>2</n></r></e><v>β</v><v>R</v><h>*</h><v>k0</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><r><p><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x20</v><v>β</v><v>R</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r></r><r><q><v>β</v><e><r><v>k2</v></r><r><n>2</n></r></e><v>β</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>β</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>β</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>β</v><e><r><v>k0</v></r><r><n>2</n></r></e></q></r></f><v>+</v><f><r><fn><r><fnm>cos</fnm></r><r><p><f><r><q><v>β</v><e><r><v>k2</v></r><r><n>2</n></r></e><v>β</v><r><p><n>2</n><h>*</h><v>k1</v><v>β</v><n>2</n><h>*</h><v>k0</v></p></r><h>*</h><v>k2</v><v>β</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>β</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>β</v><e><r><v>k0</v></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x20</v><v>β</v><v>R</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r><v>+</v><f><r><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f><t>]</t>
</mth></output>
</cell>

<cell type="code" hide="true">
<input>
<editor type="input">
<line>sol_neg_1:Β %e^(-t*(k0+k1+k2)/2)*(</line>
<line>      sin(t*sqrt(-rad)/2)*( 2*k2*x20+x10*(k2-k1-k0)+R*(k0-k1-k2)/k0 )/sqrt(-rad) </line>
<line>    + cos(t*sqrt(-rad)/2)*(x10-R/k0)) +Β R/k0;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="sol_neg_1">(%o34) </lbl><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><f><r><q><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v><v>β</v><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><n>2</n><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><r><p><v>k2</v><v>β</v><v>k1</v><v>β</v><v>k0</v></p></r><h>*</h><v>x10</v><v>+</v><f><r><v>R</v><h>*</h><r><p><v>β</v><v>k2</v><v>β</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><v>k0</v></r></f></p></r></r><r><q><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v><v>β</v><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e></q></r></f><v>+</v><fn><r><fnm>cos</fnm></r><r><p><f><r><q><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v><v>β</v><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><v>x10</v><v>β</v><f><r><v>R</v></r><r><v>k0</v></r></f></p></r></p></r><v>+</v><f><r><v>R</v></r><r><v>k0</v></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp( sol_neg_1 - rhs(sol_neg_ugly[1]) );</line>
</editor>
</input>
<output>
<mth><lbl>(%o37) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="code" hide="true">
<input>
<editor type="input">
<line>sol_neg_2:Β %e^(-t*(k0+k1+k2)/2)*(</line>
<line>    sin(t*sqrt(-rad)/2)*( 2*x10*k1 +Β x20*(k0+k1-k2) - R*k1*(k0+k1+k2)/(k0*k2) )/sqrt(-rad) </line>
<line>  +Β cos(t*sqrt(-rad)/2)*(x20-R*k1/(k0*k2)) ) +Β R*k1/(k0*k2);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="sol_neg_2">(%o38) </lbl><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><f><r><q><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v><v>β</v><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><r><p><v>β</v><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>x20</v><v>+</v><n>2</n><h>*</h><v>k1</v><h>*</h><v>x10</v><v>β</v><f><r><v>R</v><h>*</h><v>k1</v><h>*</h><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r></r><r><q><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v><v>β</v><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e></q></r></f><v>+</v><fn><r><fnm>cos</fnm></r><r><p><f><r><q><n>4</n><h>*</h><v>k0</v><h>*</h><v>k2</v><v>β</v><e><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><n>2</n></r></e></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><v>x20</v><v>β</v><f><r><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r></p></r><v>+</v><f><r><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(sol_neg_2 - rhs(sol_neg_ugly[2]));</line>
</editor>
</input>
<output>
<mth><lbl>(%o39) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="text">
<editor type="text">
<line>One last time, when rad = 0</line>
</editor>

</cell>

<cell type="code" answer1="zero;" hide="true">
<input>
<editor type="input">
<line>sol_zero_ugly:Β desolve(ode_system1, [x1(t), x2(t)]);</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><e><r><v>k2</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k1</v><h>*</h><v>k2</v><v>β</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k2</v><v>+</v><e><r><v>k1</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e><st> positive, negative or zero?</st><editor type="input">
<line>zero;</line>
</editor>
<lbl userdefined="yes" userdefinedlabel="sol_zero_ugly">(%o40) </lbl><t>[</t><fn><r><fnm>x1</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><v>t</v><h>*</h><r><p><f><r><n>2</n><h>*</h><r><p><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x10</v><v>β</v><v>R</v><h>*</h><v>k2</v><v>β</v><v>R</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v></r></f><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><r><p><v>k0</v><h>*</h><v>x10</v><v>β</v><v>R</v></p></r></r><r><v>k0</v></r></f></p></r></r><r><n>2</n></r></f><v>+</v><f><r><v>k0</v><h>*</h><v>x10</v><v>β</v><v>R</v></r><r><v>k0</v></r></f></p></r><v>+</v><f><r><v>R</v></r><r><v>k0</v></r></f><t>,</t><fn><r><fnm>x2</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><v>t</v><h>*</h><r><p><f><r><n>2</n><h>*</h><r><p><r><p><v>k0</v><h>*</h><v>k1</v><v>+</v><e><r><v>k0</v></r><r><n>2</n></r></e></p></r><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><v>k0</v><h>*</h><v>k1</v><h>*</h><v>k2</v><h>*</h><v>x10</v><v>β</v><v>R</v><h>*</h><v>k1</v><h>*</h><v>k2</v><v>β</v><v>R</v><h>*</h><e><r><v>k1</v></r><r><n>2</n></r></e><v>β</v><v>R</v><h>*</h><v>k0</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><r><p><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x20</v><v>β</v><v>R</v><h>*</h><v>k1</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r></r><r><n>2</n></r></f><v>+</v><f><r><v>k0</v><h>*</h><v>k2</v><h>*</h><v>x20</v><v>β</v><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r><v>+</v><f><r><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f><t>]</t>
</mth></output>
</cell>

<cell type="code" hide="true">
<input>
<editor type="input">
<line>sol_zero_1:Β %e^(-t*(k0+k1+k2)/2)*(</line>
<line>    t*(2*k2*x20+x10*(k2-k1-k0)+R*(k0-k1-k2)/k0)/2 + x10-R/k0 ) + R/k0;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="sol_zero_1">(%o43) </lbl><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><v>t</v><h>*</h><r><p><n>2</n><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><r><p><v>k2</v><v>β</v><v>k1</v><v>β</v><v>k0</v></p></r><h>*</h><v>x10</v><v>+</v><f><r><v>R</v><h>*</h><r><p><v>β</v><v>k2</v><v>β</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><v>k0</v></r></f></p></r></r><r><n>2</n></r></f><v>+</v><v>x10</v><v>β</v><f><r><v>R</v></r><r><v>k0</v></r></f></p></r><v>+</v><f><r><v>R</v></r><r><v>k0</v></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(sol_zero_1 - rhs(sol_zero_ugly[1]));</line>
</editor>
</input>
<output>
<mth><lbl>(%o44) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>sol_zero_2:Β %e^(-t*(k0+k1+k2)/2)*(</line>
<line>    t*(2*k1*x10 +Β (k0+k1-k2)*x20 - R*k1*(k0+k1+k2)/(k0*k2))/2 </line>
<line>    +Β x20-R*k1/(k0*k2) ) +Β R*k1/(k0*k2);</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="sol_zero_2">(%o49) </lbl><e><r><s>%e</s></r><r><v>β</v><f><r><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><v>t</v><h>*</h><r><p><r><p><v>β</v><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r><h>*</h><v>x20</v><v>+</v><n>2</n><h>*</h><v>k1</v><h>*</h><v>x10</v><v>β</v><f><r><v>R</v><h>*</h><v>k1</v><h>*</h><r><p><v>k2</v><v>+</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r></r><r><n>2</n></r></f><v>+</v><v>x20</v><v>β</v><f><r><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f></p></r><v>+</v><f><r><v>R</v><h>*</h><v>k1</v></r><r><v>k0</v><h>*</h><v>k2</v></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(sol_zero_2 - rhs(sol_zero_ugly[2]));</line>
</editor>
</input>
<output>
<mth><lbl>(%o50) </lbl><n>0</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
</editor>
</input>
</cell>

<cell type="section" sectioning_level="2">
<editor type="section" sectioning_level="2">
<line>Try to add the effect site</line>
</editor>

</cell>

<cell type="subsection" sectioning_level="3">
<editor type="subsection" sectioning_level="3">
<line>Equations</line>
</editor>

</cell>

<cell type="text">
<editor type="text">
<line>Ce&apos;(t) = k(Cp(t) - Ce(t))</line>
<line>Ce(0) = ce0</line>
<line></line>
<line>Cp(t) = sol_pos_1/v1  (or sol_neg_1/v1, or sol_zero_1/v1)</line>
</editor>

</cell>

<cell type="text">
<editor type="text">
<line>diff_eq5:  &apos;diff(ce(t),t) = k*(sol_pos_1/v1 - ce(t))</line>
</editor>

</cell>

<cell type="code">
<input>
<editor type="input">
<line>sol_pos_1:Β %e^(-(k0+k1+k2)*t/2)*( </line>
<line>    sinh(t*sqrt(rad)/2)*((k0-k1-k2)*R/k0 + x10*(k2-k1-k0) + 2*x20*k2)/sqrt(rad) </line>
<line>    + cosh(t*sqrt(rad)/2)*(x10 - R/k0)Β ) +Β R/k0;</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="sol_pos_1">(%o6) </lbl><e><r><s>%e</s></r><r><f><r><r><p><v>β</v><v>k2</v><v>β</v><v>k1</v><v>β</v><v>k0</v></p></r><h>*</h><v>t</v></r><r><n>2</n></r></f></r></e><h>*</h><r><p><f><r><fn><r><fnm>sinh</fnm></r><r><p><f><r><q><v>rad</v></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><n>2</n><h>*</h><v>k2</v><h>*</h><v>x20</v><v>+</v><r><p><v>k2</v><v>β</v><v>k1</v><v>β</v><v>k0</v></p></r><h>*</h><v>x10</v><v>+</v><f><r><v>R</v><h>*</h><r><p><v>β</v><v>k2</v><v>β</v><v>k1</v><v>+</v><v>k0</v></p></r></r><r><v>k0</v></r></f></p></r></r><r><q><v>rad</v></q></r></f><v>+</v><fn><r><fnm>cosh</fnm></r><r><p><f><r><q><v>rad</v></q><h>*</h><v>t</v></r><r><n>2</n></r></f></p></r></fn><h>*</h><r><p><v>x10</v><v>β</v><f><r><v>R</v></r><r><v>k0</v></r></f></p></r></p></r><v>+</v><f><r><v>R</v></r><r><v>k0</v></r></f>
</mth></output>
</cell>

<cell type="code" answer1="no;" answer2="no;" answer3="no;" answer4="no;" answer5="no;" answer6="no;" answer7="no;" answer8="no;" answer9="n;" answer10="no;" answer11="no;" answer12="false;" answer13="n;" answer14="N;" answer15="no;" answer16=";">
<input>
<editor type="input">
<line>desolve(diff_eq5, ce(t));</line>
</editor>
</input>
<output>
<mth><t breakline="true">desolve: can&apos;t handle this case.</t><t breakline="true" type="error"> -- an error. To debug this try: debugmode(true);</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff_eq6: &apos;diff(ce(t),t)=ke*(  %e^(-a*t)*(b*sinh(c*t) + d*cosh(f*t)) +Β g  - ce(t)  ); </line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="diff_eq6">(%o18) </lbl><d><f diffstyle="yes"><r><s>d</s></r><r><s>d</s><h>*</h><v>t</v></r></f><h>*</h><fn><r><fnm>ce</fnm></r><r><p><v>t</v></p></r></fn></d><v>=</v><v>ke</v><h>*</h><r><p><e><r><s>%e</s></r><r><v>β</v><v>a</v><h>*</h><v>t</v></r></e><h>*</h><r><p><v>d</v><h>*</h><fn><r><fnm>cosh</fnm></r><r><p><v>f</v><h>*</h><v>t</v></p></r></fn><v>+</v><v>b</v><h>*</h><fn><r><fnm>sinh</fnm></r><r><p><v>c</v><h>*</h><v>t</v></p></r></fn></p></r><v>β</v><fn><r><fnm>ce</fnm></r><r><p><v>t</v></p></r></fn><v>+</v><v>g</v></p></r>
</mth></output>
</cell>

<cell type="code" answer1="pos;" answer2="pos;" answer3="pos;" answer4="pos;">
<input>
<editor type="input">
<line>desolve(diff_eq6,ce(t));</line>
</editor>
</input>
<output>
<mth><st breakline="true">Is </st><v>g20400</v><v>+</v><v>c</v><v>+</v><v>a</v><st> positive, negative or zero?</st><editor type="input">
<line>pos;</line>
</editor>
<st breakline="true">Is </st><v>g20400</v><v>β</v><v>c</v><v>+</v><v>a</v><st> positive, negative or zero?</st><editor type="input">
<line>pos;</line>
</editor>
<st breakline="true">Is </st><v>g20400</v><v>+</v><v>f</v><v>+</v><v>a</v><st> positive, negative or zero?</st><editor type="input">
<line>pos;</line>
</editor>
<st breakline="true">Is </st><v>g20400</v><v>β</v><v>f</v><v>+</v><v>a</v><st> positive, negative or zero?</st><editor type="input">
<line>pos;</line>
</editor>
<lbl>(%o17) </lbl><fn><r><fnm>ce</fnm></r><r><p><v>t</v></p></r></fn><v>=</v><v>β</v><f><r><r><p><r><p><v>g</v><v>+</v><v>d</v><v>β</v><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn></p></r><h>*</h><e><r><v>ke</v></r><r><n>4</n></r></e><v>+</v><r><p><v>β</v><n>4</n><h>*</h><v>a</v><h>*</h><v>g</v><v>β</v><n>3</n><h>*</h><v>a</v><h>*</h><v>d</v><v>β</v><v>b</v><h>*</h><v>c</v><v>+</v><n>4</n><h>*</h><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><v>a</v></p></r><h>*</h><e><r><v>ke</v></r><r><n>3</n></r></e><v>+</v><r><p><r><p><v>β</v><e><r><v>f</v></r><r><n>2</n></r></e><v>β</v><e><r><v>c</v></r><r><n>2</n></r></e><v>+</v><n>6</n><h>*</h><e><r><v>a</v></r><r><n>2</n></r></e></p></r><h>*</h><v>g</v><v>+</v><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><e><r><v>f</v></r><r><n>2</n></r></e><v>+</v><r><p><n>3</n><h>*</h><e><r><v>a</v></r><r><n>2</n></r></e><v>β</v><e><r><v>c</v></r><r><n>2</n></r></e></p></r><h>*</h><v>d</v><v>+</v><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><e><r><v>c</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>a</v><h>*</h><v>b</v><h>*</h><v>c</v><v>β</v><n>6</n><h>*</h><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><e><r><v>a</v></r><r><n>2</n></r></e></p></r><h>*</h><e><r><v>ke</v></r><r><n>2</n></r></e><v>+</v><r><p><r><p><n>2</n><h>*</h><v>a</v><h>*</h><e><r><v>f</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>a</v><h>*</h><e><r><v>c</v></r><r><n>2</n></r></e><v>β</v><n>4</n><h>*</h><e><r><v>a</v></r><r><n>3</n></r></e></p></r><h>*</h><v>g</v><v>+</v><r><p><v>b</v><h>*</h><v>c</v><v>β</v><n>2</n><h>*</h><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><v>a</v></p></r><h>*</h><e><r><v>f</v></r><r><n>2</n></r></e><v>+</v><r><p><v>a</v><h>*</h><e><r><v>c</v></r><r><n>2</n></r></e><v>β</v><e><r><v>a</v></r><r><n>3</n></r></e></p></r><h>*</h><v>d</v><v>β</v><n>2</n><h>*</h><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><v>a</v><h>*</h><e><r><v>c</v></r><r><n>2</n></r></e><v>β</v><e><r><v>a</v></r><r><n>2</n></r></e><h>*</h><v>b</v><h>*</h><v>c</v><v>+</v><n>4</n><h>*</h><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><e><r><v>a</v></r><r><n>3</n></r></e></p></r><h>*</h><v>ke</v><v>+</v><r><p><r><p><e><r><v>c</v></r><r><n>2</n></r></e><v>β</v><e><r><v>a</v></r><r><n>2</n></r></e></p></r><h>*</h><e><r><v>f</v></r><r><n>2</n></r></e><v>β</v><e><r><v>a</v></r><r><n>2</n></r></e><h>*</h><e><r><v>c</v></r><r><n>2</n></r></e><v>+</v><e><r><v>a</v></r><r><n>4</n></r></e></p></r><h>*</h><v>g</v><v>+</v><r><p><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><e><r><v>a</v></r><r><n>2</n></r></e><v>β</v><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><e><r><v>c</v></r><r><n>2</n></r></e></p></r><h>*</h><e><r><v>f</v></r><r><n>2</n></r></e><v>+</v><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><e><r><v>a</v></r><r><n>2</n></r></e><h>*</h><e><r><v>c</v></r><r><n>2</n></r></e><v>β</v><fn><r><fnm>ce</fnm></r><r><p><n>0</n></p></r></fn><h>*</h><e><r><v>a</v></r><r><n>4</n></r></e></p></r><h>*</h><e><r><s>%e</s></r><r><v>β</v><v>ke</v><h>*</h><v>t</v></r></e></r><r><e><r><v>ke</v></r><r><n>4</n></r></e><v>β</v><n>4</n><h>*</h><v>a</v><h>*</h><e><r><v>ke</v></r><r><n>3</n></r></e><v>+</v><r><p><v>β</v><e><r><v>f</v></r><r><n>2</n></r></e><v>β</v><e><r><v>c</v></r><r><n>2</n></r></e><v>+</v><n>6</n><h>*</h><e><r><v>a</v></r><r><n>2</n></r></e></p></r><h>*</h><e><r><v>ke</v></r><r><n>2</n></r></e><v>+</v><r><p><n>2</n><h>*</h><v>a</v><h>*</h><e><r><v>f</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>*</h><v>a</v><h>*</h><e><r><v>c</v></r><r><n>2</n></r></e><v>β</v><n>4</n><h>*</h><e><r><v>a</v></r><r><n>3</n></r></e></p></r><h>*</h><v>ke</v><v>+</v><r><p><e><r><v>c</v></r><r><n>2</n></r></e><v>β</v><e><r><v>a</v></r><r><n>2</n></r></e></p></r><h>*</h><e><r><v>f</v></r><r><n>2</n></r></e><v>β</v><e><r><v>a</v></r><r><n>2</n></r></e><h>*</h><e><r><v>c</v></r><r><n>2</n></r></e><v>+</v><e><r><v>a</v></r><r><n>4</n></r></e></r></f><v>+</v><f><r><v>d</v><h>*</h><v>ke</v><h>*</h><e><r><s>%e</s></r><r><v>β</v><r><p><v>f</v><v>+</v><v>a</v></p></r><h>*</h><v>t</v></r></e></r><r><n>2</n><h>*</h><v>ke</v><v>β</v><n>2</n><h>*</h><v>f</v><v>β</v><n>2</n><h>*</h><v>a</v></r></f><v>+</v><f><r><v>d</v><h>*</h><v>ke</v><h>*</h><e><r><s>%e</s></r><r><v>β</v><r><p><v>a</v><v>β</v><v>f</v></p></r><h>*</h><v>t</v></r></e></r><r><n>2</n><h>*</h><v>ke</v><v>+</v><n>2</n><h>*</h><v>f</v><v>β</v><n>2</n><h>*</h><v>a</v></r></f><v>β</v><f><r><v>b</v><h>*</h><v>ke</v><h>*</h><e><r><s>%e</s></r><r><v>β</v><r><p><v>c</v><v>+</v><v>a</v></p></r><h>*</h><v>t</v></r></e></r><r><n>2</n><h>*</h><v>ke</v><v>β</v><n>2</n><h>*</h><v>c</v><v>β</v><n>2</n><h>*</h><v>a</v></r></f><v>+</v><f><r><v>b</v><h>*</h><v>ke</v><h>*</h><e><r><s>%e</s></r><r><v>β</v><r><p><v>a</v><v>β</v><v>c</v></p></r><h>*</h><v>t</v></r></e></r><r><n>2</n><h>*</h><v>ke</v><v>+</v><n>2</n><h>*</h><v>c</v><v>β</v><n>2</n><h>*</h><v>a</v></r></f><v>+</v><v>g</v>
</mth></output>
</cell>

</wxMaximaDocument>PK      τBKρBH                       mimetypePK      τBKΰΙnA5  5  
             5   format.txtPK      τBKnγ"Ψ#  #                 content.xmlPK      §   ή    