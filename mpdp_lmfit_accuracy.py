import argparse
import numpy as np
import pandas as pd

from glob import glob


def gen_accuracy_table(df, noise_cov, cp_t, var):
    grouped = df.groupby('stage')
    pct_05 = grouped[var].quantile(.05)
    median = grouped[var].quantile(.5)
    pct_95 = grouped[var].quantile(.95)

    return pd.DataFrame({
        'noise': noise_cov,
        'cp_t': cp_t,
        'var': var,
        'median': median,
        '5 %tile': pct_05,
        '95 %tile': pct_95
    })


if __name__ == '__main__':
    for noise_cov in [0.05, 0.129]:
        frames = []
        for cp_t in ['mean', 'hi', 'lo']:
            df = pd.read_csv('all_stages_noise-{}_cpt-{}_emcee_results.csv'.format(noise_cov, cp_t))
            df['stage'] = df['stage'].str.lower()
            frames.append(gen_accuracy_table(df, noise_cov, cp_t, 'k'))

        res = pd.concat(frames)
        res.to_csv('ke_noise-{}_accuracy.csv'.format(noise_cov))
