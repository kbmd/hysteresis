import argparse
import csv
import lmfit
import mpdp
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import os
import seaborn

from glob import glob
from matplotlib.backends.backend_pdf import PdfPages
from string import ascii_uppercase

def model(params, t):
    k = params['k']
    ec50 = params['ec50']
    n = params['n']
    e0 = params['e0']
    emax = params['emax']

    return e0 + emax*mpdp.effect(mpdp.Ce(t, k, Cptimes, Cpsamples), ec50, n)

def residual(params, t, data):
    return (model(params,t) - data) #/ ( 1 / count)

redo = True

show_figures = True
save_figures = False
savefiles = False

# target = input parameters
target = {
    'e0':mpdp.meansize,
    'emax':mpdp.effectsize
}

cpt_map = {
    'hi': 'High',
    'lo': 'Low'
}

parser = argparse.ArgumentParser()
parser.add_argument('noise', type=float, choices=[0.05, 0.129])
parser.add_argument('--redo', action='store_true')
parser.add_argument('--outdir', default=os.getcwd())
parser.add_argument('--mc_nwalkers', type=int, default=100)
parser.add_argument('--mc_steps', type=int, default=1000)
parser.add_argument('--mc_workers', type=int, default=1)
parser.add_argument('--slow', action='store_true', help='use slow infusion timing (default = mean)')
args = parser.parse_args()

noiseCoV = args.noise

niter = 100
nrows = 5
ncols = 5

times2 = mpdp.asl_midpt_all
cpt_pairs = []

if not args.slow:
    Cptimes = mpdp.globmeant
    cpt_pairs = [ ('mean', mpdp.globmeanc), ('hi', mpdp.globchi), ('lo', mpdp.globclo) ]
else:
    Cptimes = mpdp.slowt
    cpt_pairs = [ ('Slow', mpdp.slowc) ]

os.chdir(args.outdir)
columns = ['noiseCoV', 'stage', 'redchi', 'k', 'ec50', 'n', 'e0', 'emax', 'lnsigma']

sd = target['e0']*noiseCoV
#
# for cp_t, Cpsamples in cpt_pairs:
#     Cptimes = np.insert(Cptimes, 0, -15.)
#     Cptimes = np.append(Cptimes, 121.)
#     Cpsamples = np.insert(Cpsamples, 0, Cpsamples[0])
#     Cpsamples = np.append(Cpsamples, Cpsamples[-1])
#
#     for i, stage in enumerate(mpdp.stages):
#         outroot = 'hy-{}_noise-{}_cpt-{}_emcee_results'.format(stage, noiseCoV, cp_t)
#
#         if os.path.exists(os.path.join(args.outdir, outroot + '.pdf')) and not args.redo:
#             continue
#
#         target['k'] = mpdp.keqs[i]
#         target['ec50'] = mpdp.ec50s[i]
#         target['n'] = mpdp.hillns[i]
#         clean = target['e0'] + target['emax']*mpdp.effect(
#                 mpdp.Ce(times2, target['k'], Cptimes, Cpsamples),
#                     target['ec50'],target['n'])
#
#         fit_results = [columns]
#         # data = []
#
#         with PdfPages(outroot + '.pdf') as pdf:
#             for page in range(niter // (nrows * ncols)):
#                 fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(11,8))
#                 for row in range(nrows):
#                     for col in range(ncols):
#                         noise = np.random.normal(0.0, sd, size=len(clean))
#                         data = (clean + noise)
#
#                         params = lmfit.Parameters()
#                         params.add('k', min=mpdp.keqs[0], max=mpdp.keqs[-1])
#                         params.add('ec50', min=mpdp.ec50s[0], max=mpdp.ec50s[-1])
#                         params.add('n', min=mpdp.hillns[0], max=mpdp.hillns[-1])
#                         params.add('e0', min=0, max=100)
#                         params.add('emax', value=0, min=-40, max=40) # midbrain response in Chen 2015 was ~70%
#
#                         mi = lmfit.minimize(residual, params, method='ampgo', args=(times2, data))
#                         res = lmfit.minimize(residual, mi.params, method='emcee', args=(times2, data), nwalkers=args.mc_nwalkers, steps=args.mc_steps, workers=args.mc_workers, is_weighted=False)
#
#                         result = [noiseCoV, stage, res.redchi]
#
#                         highest_prob = np.argmax(res.lnprob)
#                         hp_loc = np.unravel_index(highest_prob, res.lnprob.shape)
#                         mle_soln = res.chain[hp_loc]
#                         p = {}
#                         for i, par in enumerate(res.params.keys()):
#                             p[par] = mle_soln[i]
#                             result.append(mle_soln[i])
#
#                         fit_results.append(result)
#
#                         axes[row][col].plot(times2, data, color='black', marker='.', linestyle='')
#                         axes[row][col].plot(times2, clean, color='gray')
#                         axes[row][col].plot(times2, model(p, times2), color='red')
#                         print(row, col)
#
#                 pdf.savefig()
#                 plt.close()
#
#         with open(outroot + '.csv', 'w', newline='') as f:
#             writer = csv.writer(f)
#             writer.writerows(fit_results)
#
#
#     #    make violin plots for variables
#     all_stages_csv = 'all_stages_noise-{}_cpt-{}_emcee_results.csv'.format(noiseCoV, cp_t)
#     if not os.path.exists(all_stages_csv):
#         df = None
#         stage_csvs = glob('hy-*_noise-{}_cpt-{}_emcee_results.csv'.format(noiseCoV, cp_t))
#         for f in stage_csvs:
#             temp_df = pd.read_csv(f, header=0, names=['noiseCoV', 'stage', 'redchi', 'k', 'ec50', 'n', 'e0', 'emax', 'lnsigma'])
#             df = temp_df if df is None else pd.concat([df, temp_df], axis=0)
#         df.to_csv(all_stages_csv, index=False)

for var in ['k', 'ec50', 'n']:
    plt_rows = len(cpt_pairs)
    fig, axes = plt.subplots(nrows=plt_rows, ncols=1, sharex=True, sharey=True, figsize=(6, 3.75*plt_rows))
    # plt.set_cmap('gray')

    for i, cp_t in enumerate([ label for label, _ in cpt_pairs ]):
        ax = axes[i] if plt_rows > 1 else axes
        color = plt.cm.gray(.5)
        df = pd.read_csv(os.path.join(args.outdir, 'all_stages_noise-{}_cpt-{}_emcee_results.csv'.format(noiseCoV, cp_t)))

        datasets = [ (g, data[var].values) for g, data in sorted(df.groupby('stage'), key=lambda x: x[0].lower()) ]
        data = [ np.log10(d[1]) if var == 'k' else d[1] for d in datasets]
        violin = ax.violinplot(data, positions=range(len(datasets)), showextrema=False)
        ax.set_xticks(range(len(datasets)))
        ax.set_xticklabels(['H&Y {}'.format(label) if label.startswith('I') else label for label, _ in datasets])

        for pc in violin['bodies']:
            pc.set_facecolor(color)
        #     pc.set_edgecolor('black')
        #     pc.set_alpha(1)

        pctile5, medians, pctile95 = np.percentile(data, [5, 50, 95], axis=1)

        if var == 'k':
            target = [np.log10(val) for val in mpdp.keqs]
            yticks = np.arange(-3,2)
            ax.set_yticks(yticks)
            ax.set_yticklabels(10.0 ** yticks)
        elif var == 'ec50':
            target = mpdp.ec50s
        else:
            target = mpdp.hillns

        indexes = range(len(datasets))


        ax.vlines(indexes, pctile5, pctile95, linestyle='-', color=color) # add vertical line for 5th-95th percentiles
        for pctile in [ pctile5, medians, pctile95]:
            ax.scatter(indexes, pctile, marker=0, color=color)
            ax.scatter(indexes, pctile, marker=1, color=color)
        ax.scatter(indexes, target[:len(datasets)], color='black', zorder=5) # add the target point

        title = '{} Cp(t)'.format(cpt_map[cp_t] if cp_t in cpt_map else cp_t.title())
        ax.set_title(title)
        ax.set_ylabel('Estimated {}'.format(var if var != 'k' else '$\mathregular{k_e}$'))

        ax.text(-0.1, 1, ascii_uppercase[i], transform=ax.transAxes, size=18, weight='bold')

    plt.xlabel('Disease Severity')
    # plt.show()
    outfile = os.path.join(args.outdir, '{}_noise-{}_cpt-{}_{}iter_pctile.tif'.format(var, noiseCoV, 'slow' if args.slow else 'all', niter))
    plt.savefig(outfile, bbox_inches='tight', dpi=600, type='tiff')