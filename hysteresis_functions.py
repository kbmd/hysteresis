import math
ln2 = math.log(2)
def ln2over(t):
    return ln2/t
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.integrate import ode

# Thanks to http://stackoverflow.com/questions/2891790/pretty-printing-of-numpy-array
import contextlib
@contextlib.contextmanager
def printoptions(*args, **kwargs):
    original = np.get_printoptions()
    np.set_printoptions(*args, **kwargs)
    yield 
    np.set_printoptions(**original)

import csv

glob_interval_end = np.asarray([2.5, 6.5, 10.0, 12.5, 17.5, 22.5, 27.5, 35, 45, 70, 90, 120])
num_globs = glob_interval_end.size
globtime  = []
globconc  = []
globsize  = np.zeros([num_globs],int)
globmeant = np.zeros([num_globs])
globmeanc = np.zeros([num_globs])
globsdt = np.zeros([num_globs])
globsdc = np.zeros([num_globs])

sample_n = 100

def plot_data_and_samples(n=sample_n,savepng=False):
    plt.axis((0,112,0,3500))
    plt.xlabel('time after start of infusion (min.)') 
    plt.ylabel('Levodopa plasma concentration (ng/ml)')
    # plot the samples
    mylabel = 'sample curves'
    for i in range(n):
        plt.plot(t_samples[i],c_samples[i],
                 color='lightgrey',marker=None,linewidth=1.0, 
                 label=mylabel)
        mylabel = '_nolegend_'  # => label is defined only for i==0
    # plot the real data
    plt.scatter(data_times,data_concs,color='black',label='data')
    legend = plt.legend(loc='upper right')
    # ,frameon=False
    # framealpha=0.3,
    legend.get_frame().set_facecolor('none')
    # '#FFCC77'
    if savepng:
        plt.savefig('samples.png', dpi=600, transparent=True)
    plt.show()


# In[12]:

def generic_x1(t,k0,k1,k2,R,x10,x20,ts):
    '''Returns a closed-form solution to the problem in the cell above. 
    The return value and the parameter t are numpy arrays. Other parameters are real numbers.
    '''
    u = t-ts
    rad = (k0+k1+k2)**2 - 4*k0*k2
    if rad > 0 :
        sqrad = math.sqrt(rad)
        return np.exp(-u*(k0+k1+k2)/2)*(                     np.sinh(u*sqrad/2)* ((k0-k1-k2)*R/k0 + x10*(k2-k1-k0) + 2*x20*k2)/sqrad                   + np.cosh(u*sqrad/2)*(x10-R/k0) ) + R/k0 
    elif rad < 0 :
        sqrad = math.sqrt(-rad)
        return np.exp(-u*(k0+k1+k2)/2)*(                     np.sin(u*sqrad/2)*(2*k2*x20+x10*(k2-k1-k0)+R*(k0-k1-k2)/k0)/sqrad                   + np.cos(u*sqrad/2)*(x10-R/k0) ) + R/k0 
    else:  # rad = 0
        return np.exp(-u*(k0+k1+k2)/2)*( u*(2*k2*x20+x10*(k2-k1-k0)+R*(k0-k1-k2)/k0)/2+x10-R/k0 ) + R/k0

def generic_x2(t,k0,k1,k2,R,x10,x20,ts):
    '''Returns a closed-form solution to the problem in the cell above. 
    The return value and the parameter t are numpy arrays. Other parameters are real numbers.
    '''
    u = t-ts
    rad = (k0+k1+k2)**2 - 4*k0*k2
    if rad > 0 :
        sqrad = math.sqrt(rad)
        return  np.exp(-u*(k0+k1+k2)/2)*(                     np.sinh(u*sqrad/2)*(x20*(k0+k1-k2)+2*k1*x10-R*k1*(k0+k1+k2)/(k0*k2))/sqrad                   + np.cosh(u*sqrad/2)*(x20-R*k1/(k0*k2)) ) + R*k1/(k0*k2)
    elif rad < 0 :
        sqrad = math.sqrt(-rad)
        return np.exp(-u*(k0+k1+k2)/2)*(                     np.sin(u*sqrad/2)*(2*x10*k1+x20*(k0+k1-k2)-R*k1*(k0+k1+k2)/(k0*k2))/sqrad                   + np.cos(u*sqrad/2)*(x20-R*k1/(k0*k2)) ) + R*k1/(k0*k2) 
    else:  # rad = 0
        return np.exp(-u*(k0+k1+k2)/2)*(                     u*(2*k1*x10 +(k0+k1-k2)*x20-R*k1*(k0+k1+k2)/(k0*k2))/2 + x20-R*k1/(k0*k2) )                     + R*k1/(k0*k2)


def c1(t,k0,k1,k2,V1,V2,ts,C0,RL,RM,tL=10.0):
    '''Returns a numpy array with C1(t), based on the generic solution above.
    The time shift ts can cause values for c1(t), t<ts, to be < C0 (or even < 0). Hence the
        last line.
    Note x1(t) is divided by V1, in the penultimate line, to return concentration not mass.
    '''
    def x1_load(t):
        return generic_x1(t,k0,k1,k2,RL,C0*V1,C0*V2,ts)
    def x2_load(t): 
        return generic_x2(t,k0,k1,k2,RL,C0*V1,C0*V2,ts)
    def x1_maint(t):
        return generic_x1(t-tL,k0,k1,k2,RM,x1_load(tL+ts),x2_load(tL+ts),ts)
    temp = np.where(t<=tL+ts,x1_load(t),x1_maint(t))/V1
    return np.where(temp<C0, C0, temp)  


# ### The function that `curve_fit` sees can't have fixed parameters. Create a wrapper function for `c1(t)` that has only parameters to be estimated.

mass_known = 70 # kg
age_known = 60 # yr
C0_known = 55.4  # ng/ml
tL_known = 10  # min.
RL_known = 0.6426 * mass_known * 1e6 / tL_known # ng/min
RM_known = 2.882E-5 * mass_known * (140-age_known) * 1e6  # ng/min

def c1_for_estimation(t,k0,k1,k2,V1,V2,ts):
    return c1(t,k0,k1,k2,V1,V2,ts,C0_known,RL_known,RM_known,tL_known)

# ### Set bounds for the `curve_fit` routine to use in estimating PK parameters:

# Plasma volume: PMID: 1110403
v1_est = 3000*mass_known/70  # units: ml
# VODss estimate from http://www.frontiersin.org/files/Articles/168565/fphar-06-00307-HTML/image_m/fphar-06-00307-t002.jpg
# is 1.18 L/kg. Hence:
v2_est = 1.18e3*mass_known - v1_est  # units: ml
# Elimination half-life is about 1.5 hr (http://www.frontiersin.org/files/Articles/168565/fphar-06-00307-HTML/image_m/fphar-06-00307-t002.jpg)
# From the same source, t1/2alpha is about 0.14 hr.
# From equilibrium, k2 is approx. k1*V1/V2. (See "parameter reduction", above.)
k0_est = ln2over(1.5*60)   # 0.0077 units: 1/min.
k1_est = ln2over(0.14*60)  # 0.0825  units: 1/min.
k2_est = k1_est*v1_est/v2_est  # 0.0031 units: 1/min.
# Better starting guesses, from hand fit at Cp_estimation_20150305_20170925.xlsx
k0_est = 0.0684  # units: 1/min.
k1_est = 0.042  # units: 1/min.
k2_est = 0.013   # units: 1/min.
# From PET experience (see above):
ts_est = 20/60.  # units: min.

# def c1_for_estimation(t,k0,k1,k2,V1,V2,ts):
myp0 =  (k0_est, k1_est, k2_est, v1_est, v2_est, ts_est)

# See entries above in this cell, and the same spreadsheet, for some of these limits.
# VODss bounds come from the Siddiqi et al table linked above: next-lowest and next-highest values.
mybounds = ((0.0100, 0.0279, 0.0100,  800*v1_est/3000., 0.26e3*mass_known-v1_est,   5/60.), 
            (0.0800, 0.4000, 0.1500, 4600*v1_est/3000., 2.63e3*mass_known-v1_est, 120/60.))


def print_PK_ests(k0,k1,k2,v1,v2,ts, head=None, tail=None):
    if head==None:
        head = '  k0\t  k1\t  k2\t v1\t  v2\t ts'
    if head=='':
        pass
    else:
        print(head)
    print('{0:.4f}\t{1:.3f}\t{2:6.4f}\t{3:4.0f}\t{4:6.0f}\t{5:5.3f}'          .format(k0,k1,k2,v1,v2,ts))
    if tail==None:
        # See above link to boomer.org for the following:
        S = k0+k1+k2
        P = k0*k2
        beta  = (S-math.sqrt(S*S-4*P))/2
        alpha = (S+math.sqrt(S*S-4*P))/2
        print('\tthalf approx. = {0:.0f} min.'.format(ln2over(beta)))
        print('\tthalfalpha approx. = {0:.1f} min.'.format(ln2over(alpha)))
    elif tail=='':
        pass
    else:
        print(tail)

def plot_one_sample_with_data_and_fit(n,k0,k1,k2,v1,v2,ts,savepng=False,with_data=True):
    plt.axis((0,112,0,3600))
    plt.xlabel('time after start of infusion (min.)')
    plt.ylabel('Levodopa plasma concentration (ng/ml)')
    plt.plot(t_samples[n],c_samples[n],
        color='black',marker=None,linewidth=2.0, 
        label='data sample #'+str(n))
    tees = np.linspace(0,120,121)
    plt.plot(tees,c1_for_estimation(tees,k0,k1,k2,v1,v2,ts),'r', label='C1, fit to #'+str(n))
    if with_data:
        # plot the real data
        plt.scatter(data_times,data_concs,color='black',facecolors='none',label='data')
    legend = plt.legend(loc='upper right')
    legend.get_frame().set_facecolor('none')
    if savepng:
        plt.savefig('1samplefit.png', dpi=600, transparent=True)
    plt.show()


iterations = sample_n

def Ceprime(t, ce, ke, k0,k1,k2,v1,v2,ts):
    """Derivative with respect to t of Ce(t), or [LD] in the effect compartment.
    t is time as a scalar
    ke is the rate constant for the effect compartment, a.k.a. ke0.  
    Units: Ceprime(t) ng/ml/min, t min, ke 1/min, ce0 ng/ml.
    Requires c1_for_estimation(t,k0,k1,k2,V1,V2,ts) as defined above.
    """
    return ke*(c1_for_estimation(t, k0,k1,k2,v1,v2,ts)-ce)

r = ode(Ceprime).set_integrator('dopri5', atol=1, rtol=0.001, max_step=10.0, verbosity=1)
# 'dopri5' = Runge-Kutta method of oreder (4)5, atol = absolute tolerance, rtol = relative tolerance
# r.set_initial_value(ce0).set_f_params(ke, *cp_params)
# r.t = 0.0
# r.integrate(200.0)

def Ce_scalar(time, ke, k0,k1,k2,V1,V2,ts, ce0=55.4):
    """Ce(t), or levodopa concentration in the effect compartment.
    keyword argument: ce0 is the concentration in the effect compartment at time t=0, Ce(0).
    t is time in minutes (scalar)
    ke is the rate constant for the effect compartment, a.k.a. ke0.  
    Units: Ce(t) ng/ml, t min, ke 1/min, ce0 ng/ml.
    ce0 = Ce(0)=the baseline Cp in the "practical off" state (was 55.4ng/ml for 10 LD-treated 
        PD patients in Black et al. 2003)
    Ceprime uses c1_for_estimation(t,k0,k1,k2,V1,V2,ts) as defined above.
    """
    assert ke > 0, 'ke must be positive, but was given as {0}.'.format(ke)
    if float(ke)==0.0:  # If k is zero, Ce(t)==Ce(0) for all t>=0.
        return ce0
    # otherwise, ...
    r = ode(Ceprime).set_integrator('dopri5', atol=1, rtol=0.001, max_step=10.0, verbosity=1)
    # def Ceprime(t, ce, ke, k0,k1,k2,v1,v2,ts):   
    # (Ceprime is 'f' and ce is 'y' in the scipy.integrate.ode documentation)
    r.set_initial_value(ce0).set_f_params(ke, k0,k1,k2,V1,V2,ts)
    i = 0
    r.t = 0.0
    return r.integrate(time)

# time = np.linspace(0,300,301)
def Ce(time, ke, k0,k1,k2,V1,V2,ts, ce0=55.4):
    """Ce(t), or levodopa concentration in the effect compartment.
    keyword argument: ce0 is the concentration in the effect compartment at time t=0, Ce(0).
    t is a numpy array of time points
    ke is the rate constant for the effect compartment, a.k.a. ke0.  
    Units: Ce(t) ng/ml, t min, ke 1/min, ce0 ng/ml.
    ce0 = Ce(0)=the baseline Cp in the "practical off" state (was 55.4ng/ml for 10 LD-treated 
        PD patients in Black et al. 2003)
    Ceprime uses c1_for_estimation(t,k0,k1,k2,V1,V2,ts) as defined above.
    """
    assert ke > 0, 'ke must be positive, but was given as {0}.'.format(ke)
    temp = np.full(time.shape,ce0,dtype=float)
    if float(ke)==0.0:  # If k is zero, Ce(t)==Ce(0) for all t>=0.
        return temp
    # otherwise, ...
    r = ode(Ceprime).set_integrator('dopri5', rtol=0.01, verbosity=1)
    # def Ceprime(t, ce, ke, k0,k1,k2,v1,v2,ts):   
    # (Ceprime is 'f' and ce is 'y' in the scipy.integrate.ode documentation)
    r.set_initial_value(ce0).set_f_params(ke, k0,k1,k2,V1,V2,ts)
    i = 0
    r.t = 0.0
    while r.successful() and r.t < time[-1]:
        temp[i+1] = r.integrate(time[i+1])
        i += 1
    return temp


def effect(t, k0, k1, k2, v1, v2, ts, ke, ec50, n, ce0=55.4):
    """E(Ce(t)), drug effect at time t, based on [LD] in effect compartment, using a sigmoid PD model.
    t is a numpy array of time points.
    ke is the rate constant ke, a.k.a. ke0.
    n is the Hill constant.
    Units: t min, Ce(t) ng/ml, k 1/min.
    Assumes that E(0) = 0 and Emax = 1.
    Assumes the Cp(t) modeled above based on a 2-compartment model and Fig. 3 from Black et al 2003. 
    Ce(0) = the baseline Cp in the "practical off" state, 55.4 for 10 LD-treated PD patients in ibid.
    """
    x = Ce(t, ke, k0,k1,k2,v1,v2,ts, ce0)
    return x**n/(x**n + ec50**n)


# PK-PD parameters for Park. Dz. patients' tapping speed
# Contin et al 2001, Table 4
# 12-hour washout

stages = ['best','I','II','III','IV','worst'] 
thalfeqs = [277,133,78,28,20,5]
keqs = [ln2over(x) for x in thalfeqs]
ec50s = [100,200,290,600,940,1200]
hillns = [1,2,5,7,18,49]
# next line for info only
durations = [0.5,2.0,5.0,9.0,12.0,24.0] # duration of PD in years
# from effect_compartment_modeling_20141223.xlsx , 
# y = 0.0028x - 0.0021, R² = 0.8967 for all data from Contin et al 2001 and Harder & Bass 1998,
# when plotting disease duration in years against Keq in 1/min.
# When forcing y-intercept to be 0, the line is y = 0.0026x, R² = 0.8908.
#durations = [0.5, 2.0, 5.0, 10.0, 15.0, 25.0]
#keqs = [0.0028*x for x in durations]
#thalfeqs = [ln2over(x) for x in keqs]


# Times for ASL frame start, end & midpoint, in minutes, for a ~9'40" ASL series
# (See Google sheet MPDP_ASL_timing)
frame1start = 7/60. # first frame starts about 7 sec. after hitting start
TR = 17/60.  # 17sec., in min. (one GRASE frame--effective TR for an ASL frame is twice that)
numframes = 18  # meaning ASL frames, so twice as many GRASE frames
aslstarts = np.fromiter((frame1start + 2*i*TR for i in range(numframes)), np.float)
aslendpts = aslstarts + 2*TR
aslmidpts = (aslstarts + aslendpts)/2
# Note: aslendpts[-1] = 10.32 min. So let's say it's 11'00" between series, start to start.
# On average that's probably about what it will be.
# So for frame i, counting from 0, frames will start at 11*i + aslstarts.
seriesduration = 11 # minutes
# Times for ASL frame starts, in minutes, from the beginning to the end of the scan:
duration = 120 # minutes
numruns = duration // seriesduration
asl_start_all = np.concatenate([seriesduration*run + aslstarts for run in range(numruns)])
asl_endpt_all = np.concatenate([seriesduration*run + aslendpts for run in range(numruns)])
asl_midpt_all = np.concatenate([seriesduration*run + aslmidpts for run in range(numruns)])


SDs     = [0.01, 0.02, 0.05, 0.1, 0.2, 0.5,  1,  2]
# approximately a fixed ration of 10^(-1/3), which would be equidistant on a log scale
SDnames = ['p01','p02','p05','p1','p2','p5','1','2']
noisevoxels = sample_n * len(asl_midpt_all)
noise = np.zeros((len(SDs),noisevoxels),dtype=np.float32) 
# 4-byte data (np.float32) for use with lab .4dfp format

numvoxels = sample_n
#midpteffects = np.zeros((len(stages),numvoxels,len(asl_midpt_all)), dtype=np.float32)
#for i in range(len(stages)):  # for each stage:
#    for j in range(numvoxels):  # for each Cp curve j (i.e., for each voxel in first frame):
        # def effect(t, k0, k1, k2, v1, v2, ts, ke, ec50, n, ce0=55.4):
#        starteffects = effect(asl_start_all, *answers[j], keqs[i], ec50s[i], hillns[i], ce0=c_samples[j,0])
#        endpteffects = effect(asl_endpt_all, *answers[j], keqs[i], ec50s[i], hillns[i], ce0=c_samples[j,0])
        # effect curve (i,j) at point t = asl_midpt_all[k], measured by ASL, is 
        # the mean value over the interval [a,b] (rather than the instantaneous
        # value). See above re trapezoidal rule. 
        # Save as 4-byte float for compatibility with "4dfp" image file format.
        # NOTE: The corresponding time points are asl_midpt_all.
#        midpteffects[i,j] = ((starteffects + endpteffects)/2).astype(np.float32)

