# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 12:04:02 2019

@author: kevin
"""

import mpdp
from lmfit import minimize, Parameters, fit_report

show_figures = True
save_figures = False
savefiles = False

# thsi should use globmeanc and globmeant instead
# add a data point past the end of the asl_midpt_all times
Cptimes = np.append(mpdp.slowt,121.)
Cpsamples = np.append(mpdp.slowc,mpdp.slowc[-1])
# add a data point before the beginning of the asl_midpt_all times
Cptimes = np.insert(Cptimes, 0, -15.)
Cpsamples = np.insert(Cpsamples, 0, Cpsamples[0])
#times2 = np.arange(0,121)
times2 = mpdp.asl_midpt_all

i, j, k = 2, 0, 0  # stage, Cp curve, replicate (from mpdp, just after defining midpteffects)
noiseCoV = 0.129 # from first 11 subjects in MPDP study, pre-LD GM rCBF
noiseCoV = 0.05
# target = input parameters
target = {'k':mpdp.keqs[i], 'ec50':mpdp.ec50s[i], 'n':mpdp.hillns[i], 
          'e0':mpdp.meansize, 'emax':mpdp.effectsize}
clean = target['e0'] + target['emax']*mpdp.effect(
        mpdp.Ce(times2, target['k'], Cptimes, Cpsamples),
            target['ec50'],target['n'])
sd = target['e0']*noiseCoV 
noise = np.random.normal(0.0, sd, size=len(clean))
data = clean + noise

def model(params, t):
    k = params['k']
    ec50 = params['ec50']
    n = params['n']
    e0 = params['e0']
    emax = params['emax']
    
    return e0 + emax*mpdp.effect(mpdp.Ce(t, k, Cptimes, Cpsamples), ec50, n)

def residual(params, t, data):
    return (model(params,t) - data)

params = Parameters()
params.add('k', min=mpdp.keqs[0], max=mpdp.keqs[-1])
params.add('ec50', min=mpdp.ec50s[0], max=mpdp.ec50s[-1])
params.add('n', min=mpdp.hillns[0], max=mpdp.hillns[-1])
params.add('e0', min=0, max=100)
params.add('emax', value=0, min=-40, max=40) # midbrain response in Chen 2015 was ~70%

out = minimize(residual, params, method='ampgo', args=(times2, data))

print(fit_report(out))
print('input: k={0:.5f}, ec50={1}, n={2}, e0={3:.1f}, emax={4:.1f}'.format(
        target['k'],target['ec50'],target['n'],target['e0'],target['emax']))
print('noise CoV = {0:.3f}.  Relative error of k is {1:.1%}'.format(
        sd/target['e0'], out.params['k'].value/target['k']-1))
plt.plot(times2, data, color='black', marker='.', linestyle='')
plt.plot(times2, clean, color='gray')
plt.plot(times2, model(out.params, times2), color='red')