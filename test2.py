# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 13:23:40 2019

@author: kevin
"""

# PK-PD parameters for Park. Dz. patients' tapping speed response to levodopa after
# a 12-hour washout (see Contin et al 2001, Table 4)
pkpd = [ 
        { 'stage': 'best', 'thalfeq': 277, 'ec50': 100, 'hilln': 1},
        { 'stage': 'I', 'thalfeq': 133, 'ec50': 200, 'hilln': 2},
        { 'stage': 'II', 'thalfeq': 78, 'ec50': 290, 'hilln': 5},
        { 'stage': 'III', 'thalfeq': 28, 'ec50': 600, 'hilln': 7},
        { 'stage': 'IV', 'thalfeq': 20, 'ec50': 940, 'hilln': 18},
        { 'stage': 'worst', 'thalfeq': 5, 'ec50': 1200, 'hilln': 49},
        ]
for x in pkpd:
    x['keq'] = ln2over(x['thalfeq'])


#stages = ['best','I','II','III','IV','worst'] 
#thalfeqs = [277,133,78,28,20,5]
#keqs = [ln2over(x) for x in thalfeqs]
#ec50s = [100,200,290,600,940,1200]
#hillns = [1,2,5,7,18,49]
