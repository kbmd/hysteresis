import math
ln2 = math.log(2)
def ln2over(t):
    return ln2/t
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.integrate import ode

# Thanks to http://stackoverflow.com/questions/2891790/pretty-printing-of-numpy-array
import contextlib
@contextlib.contextmanager
def printoptions(*args, **kwargs):
    original = np.get_printoptions()
    np.set_printoptions(*args, **kwargs)
    yield 
    np.set_printoptions(**original)

import csv
with open('Cp_estimation_20150304.csv', 'rb') as datafile:
    data  = np.genfromtxt(datafile, delimiter=",", usecols=range(0,7), names=True)  # imports as floats
    # the file has 7 blank rows at the end ...
    data = data[:-7]
data_times=data['new_minutes']
data_concs=data['LD']

%matplotlib inline
plt.scatter(data_times,data_concs,color='black')
plt.axis((0,120,0,3500))
plt.xlabel('time after start of infusion (min.)')  # ,fontsize=16
plt.ylabel('Levodopa plasma concentration (ng/ml)')
plt.show()

glob_interval_end = np.asarray([2.5, 6.5, 12.5, 17.5, 27.5, 35, 45, 70, 90, 120])
# create a list of arrays with points selected from data, with one array for each interval\n",
num_globs = glob_interval_end.size
globtime  = []
globconc  = []
globsize  = np.zeros([num_globs],int)
globmeant = np.zeros([num_globs])
globmeanc = np.zeros([num_globs])
globsdt = np.zeros([num_globs])
globsdc = np.zeros([num_globs])

DEBUG = True
start = -.001
for i in range(num_globs):
    end = glob_interval_end[i]
    items = np.logical_and(data_times > start,data_times <=end)
    globtime.append(data_times[np.nonzero(items)])  # this array is now globtime[i]
    globconc.append(data_concs[np.nonzero(items)])
    globsize[i]  = globtime[i].size
    globmeant[i] = np.mean(globtime[i])
    globmeanc[i] = np.mean(globconc[i])
    globsdt[i] = np.std(globtime[i])
    globsdc[i] = np.std(globconc[i])
    if i==2:
        globsize[i] = 14
        globmeant[i] = 9.0
        globmeanc[i] = 2237.93
        globsdt[i] = 1.66
        globsdc[i] = 794.3
        if DEBUG:
            print('interval i={0}, start time = {1}, end time = {2}, globsize[{0}]={3}'.\
                  format(i,start,end,globsize[i]))
            # print(items)
            print(globtime[i])
            print(globconc[i])
            print('  actual mean time in this interval: {0:.2f} +- {1:.2f}; mean conc in this interval: {2:.2f} +- {3:.2f}'.\
                  format(np.mean(globtime[i]),np.std(globtime[i]), np.mean(globconc[i]), np.std(globconc[i])))
            print('reported mean time in this interval: {0:.2f} +- {1:.2f}; mean conc in this interval: {2:.2f} +- {3:.2f}'.\
                  format(globmeant[i], globsdt[i], globmeanc[i], globsdc[i]))
    start = end


# Re-plot the real observed data with the Newer, Improved-er "globbed" data:
plt.scatter(data_times,data_concs,color='black',label='data')
plt.axis((0,120,0,3500))
plt.xlabel('time after start of infusion (min.)') 
plt.ylabel('Levodopa plasma concentration (ng/ml)')
# plot the globbed points
plt.plot(globmeant,globmeanc,'coral',
         markersize=8,marker=('D'),markeredgewidth=0,linewidth=2.0,
         label='globbed') 
    # marker=(8,2,0),marker='D',marker='o',markeredgecolor='lime',
    # http://stackoverflow.com/questions/22408237/named-colors-in-matplotlib

plt.legend(loc='upper right')
printstringend = (num_globs-1)*'{:7.1f},'+'{:7.1f}'
print('time:  '+printstringend.format(*globmeant))
print('[LD]:  '+printstringend.format(*globmeanc))
print('S.D.:  '+printstringend.format(*globsdc))
printstringend = (num_globs-1)*'{:7d},'+'{:7d}'
print('N   :  '+printstringend.format(*globsize))
plt.show()


plt.axis((10,120,500,3500))
plt.xlabel('time after start of infusion (min.)') 
plt.ylabel('Levodopa plasma concentration (ng/ml)')
# plot the globbed points
plt.semilogy(globmeant[2:],globmeanc[2:],'coral',
         markersize=8,marker=('D'),markeredgewidth=0,linewidth=2.0,
         label='globbed') 
    # marker=(8,2,0),marker='D',marker='o',markeredgecolor='lime',
    # http://stackoverflow.com/questions/22408237/named-colors-in-matplotlib
plt.legend(loc='upper right')
plt.show()


def twocompinf(t, A, a, B, b, cinf):
    return cinf + (A-cinf)*np.exp(-a*(t-10))+(B-cinf)*np.exp(-b*(t-10))

popt, pcov = curve_fit(twocompinf,globmeant[2:],globmeanc[2:])
popt

# make sure a >= b, for convenience
if popt[1]<popt[3]:
    temp = popt
    popt = np.asarray((temp[2],temp[3],temp[0],temp[1],temp[4]))
    temp = pcov
    pcov = np.asarray((temp[2],temp[3],temp[0],temp[1],temp[4]))
popt


plt.axis((10,120,500,3500))
plt.xlabel('time after start of infusion (min.)') 
plt.ylabel('Levodopa plasma concentration (ng/ml)')
# plot the globbed points
plt.semilogy(globmeant[2:],globmeanc[2:],'coral',
         markersize=8,marker=('D'),markeredgewidth=0,linewidth=2.0,
         label='globbed') 
    # marker=(8,2,0),marker='D',marker='o',markeredgecolor='lime',
    # http://stackoverflow.com/questions/22408237/named-colors-in-matplotlib
plt.legend(loc='upper right')
plt.semilogy(globmeant[2:], twocompinf(globmeant[2:], *popt), 'k',
            label='2-compartment fit')
plt.show()
print(('distribution half-life = {0:.1f} min.\n'+
      'elimination half-life = {1:.1f} min.\n'+
      'C(infinity) = {2:.0f}').format(
          ln2over(popt[1]),ln2over(popt[3]),popt[4]))


sse = np.sum((globmeanc[2:]-twocompinf(globmeant[2:], *popt))**2)
print('Summed squared error = {0:.1f}.'.format(sse))
# From [GraphPad notes](https://www.graphpad.com/guides/prism/7/curve-fitting/reg_howtheftestworks.htm), 
# $$ F = \frac{(SS1-SS2)/(DF1-DF2)}{SS2/DF2} $$
ftest = (9951.3-8880.8)/((8-4)-(8-5)) / (8880.8/(8-4))
print('F test = {0:.2f}'.format(ftest))
print('DFnum = {0}, DFdenom = {1}'.format(1,(8-4)))

