#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  2 11:08:27 2019

@author: kevin
"""

import math
import numpy as np
from matplotlib import pyplot as plt
from lmfit import minimize, Parameters, fit_report

n = 100
mu = 0 
sigma = [x/n for x in range(n)] # mean and standard deviation of noise
#sigma = .5
a, f, h = 2., 3., 5. # amplitude, frequency, phase shift of test data

x = np.linspace(0, 2*math.pi, n)
noise = np.random.normal(mu, sigma, n)
data = a*np.sin(f*x-h) + noise

def model(params, x):
    amp = params['amp']
    freq = params['freq']
    phase = params['phase']

    return amp * np.sin(freq*x - phase)

def residual(params, x, data):
    return (model(params,x) - data)

params = Parameters()
params.add('amp', min=0, max=10)
params.add('freq', min=0, max=10)
params.add('phase', min=0, max=2*math.pi)

out = minimize(residual, params, method='ampgo', args=(x, data))

#print(out.success)
#print(out.redchi) # chisq / (n - Nparams)
#print(out.nfev) # number of function evaluations
#print(out.params)
#print(out.params('amp'))
print(fit_report(out))

plt.plot(x, data)
plt.plot(x, model(out.params, x))
