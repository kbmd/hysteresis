# # version 0.5
# # 04 Oct 2017
# coding: utf-8
# See hysteresis_variable_Cp4 and hysteresis7 iPython notebook files for more details and explanations.
import math
ln2 = math.log(2)
def ln2over(t):
    return ln2/t
import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import ode
import matplotlib.pyplot as plt
import matplotlib.ticker

# ### Definitions: plotting parameters
figure_size = (3,4)  # figsize=(w,h) in inches
dots_per_inch = 600  # dpi for figures
figureext = 'png'     # 'tiff'

# ### Definitions: time axis
time_step = 1.0   	# 0.25
start_time = 0.0
duration = 90.0     # 90.0, 480.0
times = np.arange(start_time, duration+time_step, time_step)

# ### Definitions: infusion details
tload = 10.0

# ### Definitions: PKPD parameters   
# From Contin et al 2001, Table 4
# See table in effect_compartment_modeling_20141025.xlsx
HY_name = ( 'best', 'HY1', 'HY2', 'HY3', 'HY4', 'worst' )
ke0s = ( 0.002502336, 0.005211633, 0.008886502, 0.024755256, 0.034657359, 0.138629436)
ec50s = ( 100, 200, 290, 600, 940, 1200 )
nHills = ( 1, 2, 5, 7, 18, 49 )

def percentify(frac):
    return '{0:.0%}'.format(frac)

def generic_x1(t,k0,k1,k2,R,x10,x20,ts):
    '''Returns a closed-form solution to the problem in the cell above. 
    The return value and the parameter t are numpy arrays. Other parameters are real numbers.
    '''
    u = t-ts
    rad = (k0+k1+k2)**2 - 4*k0*k2
    if rad > 0 :
        sqrad = math.sqrt(rad)
        return np.exp(-u*(k0+k1+k2)/2)*( \
                    np.sinh(u*sqrad/2)* ((k0-k1-k2)*R/k0 + x10*(k2-k1-k0) + 2*x20*k2)/sqrad \
                  + np.cosh(u*sqrad/2)*(x10-R/k0) ) + R/k0 
    elif rad < 0 :
        sqrad = math.sqrt(-rad)
        return np.exp(-u*(k0+k1+k2)/2)*( \
                    np.sin(u*sqrad/2)*(2*k2*x20+x10*(k2-k1-k0)+R*(k0-k1-k2)/k0)/sqrad \
                  + np.cos(u*sqrad/2)*(x10-R/k0) ) + R/k0 
    else:  # rad = 0
        return np.exp(-u*(k0+k1+k2)/2)*( u*(2*k2*x20+x10*(k2-k1-k0)+R*(k0-k1-k2)/k0)/2+x10-R/k0 ) + R/k0

def generic_x2(t,k0,k1,k2,R,x10,x20,ts):
    '''Returns a closed-form solution to the problem in the cell above. 
    The return value and the parameter t are numpy arrays. Other parameters are real numbers.
    '''
    u = t-ts
    rad = (k0+k1+k2)**2 - 4*k0*k2
    if rad > 0 :
        sqrad = math.sqrt(rad)
        return  np.exp(-u*(k0+k1+k2)/2)*( \
                    np.sinh(u*sqrad/2)*(x20*(k0+k1-k2)+2*k1*x10-R*k1*(k0+k1+k2)/(k0*k2))/sqrad \
                  + np.cosh(u*sqrad/2)*(x20-R*k1/(k0*k2)) ) + R*k1/(k0*k2)
    elif rad < 0 :
        sqrad = math.sqrt(-rad)
        return np.exp(-u*(k0+k1+k2)/2)*( \
                    np.sin(u*sqrad/2)*(2*x10*k1+x20*(k0+k1-k2)-R*k1*(k0+k1+k2)/(k0*k2))/sqrad \
                  + np.cos(u*sqrad/2)*(x20-R*k1/(k0*k2)) ) + R*k1/(k0*k2) 
    else:  # rad = 0
        return np.exp(-u*(k0+k1+k2)/2)*( \
                    u*(2*k1*x10 +(k0+k1-k2)*x20-R*k1*(k0+k1+k2)/(k0*k2))/2 + x20-R*k1/(k0*k2) ) \
                    + R*k1/(k0*k2)

def c1(t,k0,k1,k2,V1,V2,ts,C0,RL,RM,tL=10.0):
    '''Returns a numpy array with C1(t), based on the generic solution above.
    The time shift ts can cause values for c1(t), t<ts, to be < C0 (or even < 0). Hence the
        last line.
    Note x1(t) is divided by V1, in the penultimate line, to return concentration not mass.
    '''
    def x1_load(t):
        return generic_x1(t,k0,k1,k2,RL,C0*V1,C0*V2,ts)
    def x2_load(t): 
        return generic_x2(t,k0,k1,k2,RL,C0*V1,C0*V2,ts)
    def x1_maint(t):
        return generic_x1(t-tL,k0,k1,k2,RM,x1_load(tL),x2_load(tL),ts)
    temp = np.where(t<=tL,x1_load(t),x1_maint(t))/V1
    return np.where(temp<C0, C0, temp)  

def c1_for_estimation(t,k0,k1,k2,V1,V2,ts):
    return c1(t,k0,k1,k2,V1,V2,ts,C0_known,RL_known,RM_known,tL_known)

##################################################################################
##  Got here. 
##################################################################################

def Ce(time, ke):
    """Ce(t), (approximately) exact solution for concentration of LD in the effect compartment.
    
    t is a numpy array of time points
    k is the rate constant ke, a.k.a. ke0.  Units: t min, Ce(t) ng/ml, k 1/min.
    Assumes the Cp(t) modeled in hysteresis_5.ipynb based on Fig. 3 from Black et al 2003. 
    Ce(0) is assumed to be the baseline Cp, Cp(0).
    """
    a0 = 55.4 # baseline Cp in the "practical off" state for 10 LD-treated PD pts, Black et al 2003
    ce0 = a0  # Ce(0) is assumed to be the baseline Cp(0)
    # The next couple of lines could be dealt with more gracefully, I suppose.
    # assert ke != alpha, "If k==alpha, Ce(t) may need a modified form."
    # assert ke != beta,  "If k==beta,  Ce(t) may need a modified form."
    if float(ke)==0.0:  # If k is zero, Ce(t)==Ce(0) for all t>=0.
        return np.full(time.shape,ce0,dtype=float)
    # See hysteresis_6.ipynb and higher for floats below.
    ce1constant = ce0 - 286.69070707/ke - 11.73414142/ke**2  # for convenience in ce1(t) below
    return np.where(time<0.0,np.full(time.shape,ce0),\
        np.where(time<=tload, (ce0-ce1constant)*np.exp(-ke*time) \
            -5.86707071*time**2 + (286.69070707 + 11.73414142/ke)*time + ce1constant , \
        577.144039 + np.exp(-ke*(time-10))*(1758.4559607-169.349292870/ke-11.73414142/ke**2) \
            + (ce0-ce1constant)*np.exp(-ke*time)) )

def effect(time, ke, ec50, n=1):
    """E(Ce(t)), drug effect at time t, based on [drug] in effect compartment, using a sigmoid PD model.

    _Ignores scaling by Emax and shifting by e0._
    time is a numpy array of time points
    ke is the rate constant ke, a.k.a. ke0.  Units: t min, Ce(t) ng/ml, ke 1/min.
    n is the Hill constant.
    Assumes that E(0) = 0 and Emax = 1 = 100%.
    """
    return Ce(time, ke)**n/(Ce(time, ke)**n+ec50**n)

#for maint in ( 'maint', 'no' ):
for maint in ( 'maint', ):
#    for HY_num in ( 0, 1, 2, 3, 4, 5 ):
    for HY_num in ( 5, ):
    
        figure_name = HY_name[HY_num] + '_' + maint + '_' + str(int(round(duration))) + 'min_2plot2a'
        ke0 = ke0s[HY_num]
        t_half_effect = np.log(2)/ke0
        nHill = nHills[HY_num]
        ec50 = ec50s[HY_num]

        # effect(time, ke, ec50, n=1) is defined above

        # ### Plot Cp(t), Ce(t) and EC50, and effect(t), vs. time.
        figure = plt.figure(figsize=figure_size) 
        figure.clear

        ax1 = figure.add_subplot(2,1,1)
        ax1.plot(times,Cp(times),'black',linewidth=2, label='plasma')
        ax1.plot(times,Ce(times,ke0),'black',linestyle='--',linewidth=2, label='effect site')
        ax1.axhline(ec50,color='gray',linewidth=1, label='$EC_{50}$')
        ax1.axis((start_time,duration,0,2400))
        ax1.axes.xaxis.set_ticklabels([])
        c_ylabel = ax1.set_ylabel('concentration (ng/ml)', family='serif')  # , fontsize=12
        ax1.legend(framealpha=0, loc='best',fontsize=9)  # ['plasma','effect site','EC50'], 

        tempeffect = effect(times,ke0,ec50,n=nHill)  # (just to avoid recalculating it 2 or 3 times)

        ax2 = figure.add_subplot(2,1,2)
        ax2.plot(times,tempeffect, 'red',linewidth=2)
        ax2.axis((start_time,duration,0,1))
        e_xlabel = ax2.set_xlabel('time after start of infusion (min)', family='serif') # , fontsize=12
        e_ylabel = ax2.set_ylabel('effect (% of $E_{max}$)', family='serif'\
            , verticalalignment='center') # , fontsize=12
        ax2.legend(['effect'], framealpha=0, loc='best', fontsize='small')

        locs,labels = plt.yticks()
        plt.yticks(locs, map(percentify, locs))

        #titlestring = 'Predicted $C_p(t)$, $C_e(t)$ and $E(C_e(t))$\n$t_{\\frac{1}{2}e} =$'+' {:.0f} min.'.format(t_half_effect) # , verticalalignment='center'
        titlestring = '$t_{\\frac{1}{2}e} =$'+' {:.0f} min.'.format(t_half_effect)
        figure.suptitle(titlestring, family='serif', size='large', verticalalignment='center') # 

        figure.savefig(figure_name+'.'+figureext, dpi=dots_per_inch, bbox_inches='tight') 
        print
        print(figure_name+'.'+figureext + ' uses the following parameters:')
        print('ke0 = {:0.6f}/min, t_half_effect = {:.2f} min, ec50 = {:.1f} ng/ml, nHill = {:.2f}'\
                .format( ke0, t_half_effect, ec50, nHill ) )
        print
        print('tload = {}; Ce(tload) = {:.1f}; ec50 = {}; E(Ce(tload)) = {:.3f}'.format(\
            times[tload/time_step], Ce(times, ke0)[tload/time_step], ec50, tempeffect[tload/time_step]))
        
        np.savetxt(figure_name+'.txt', \
            np.hstack((times.reshape((times.size,1)),tempeffect.reshape((tempeffect.size,1)))), \
            delimiter=",")
