import numpy as np
import matplotlib.ticker
import matplotlib.pyplot as plt

# ### Definitions: plotting parameters
figure_size = (3,4)  # figsize=(w,h) in inches
dots_per_inch = 600  # dpi for figures
figureext = 'png'     # 'tiff'

# ### Definitions: time axis
time_step = 0.25
start_time = 0.0
duration = 90.0     # 90.0, 480.0
times = np.arange(start_time, duration+time_step, time_step)

# ### Definitions: infusion details
tload = 10.0

# ### Plot Cp(t), Ce(t) and EC50, and effect(t), vs. time.
figure = plt.figure(figsize=figure_size) 
figure.clear

	ax1 = figure.add_subplot(2,1,1)
	ax1.plot(times,Cp(times),'black',linewidth=2, label='plasma')
	ax1.plot(times,Ce(times,ke0),'black',linestyle='--',linewidth=2, label='effect site')
	ax1.axhline(ec50,color='gray',linewidth=1, label='$EC_{50}$')
	ax1.axis((start_time,duration,0,2400))
	ax1.axes.xaxis.set_ticklabels([])
	c_ylabel = ax1.set_ylabel('concentration (ng/ml)', family='serif')  # , fontsize=12
	ax1.legend(framealpha=0, loc='best',fontsize=9)  # ['plasma','effect site','EC50'], 

	tempeffect = effect(times,ke0,ec50,n=nHill)  # (just to avoid recalculating it 2 or 3 times)

	ax2 = figure.add_subplot(2,1,2)
	ax2.plot(times,tempeffect, 'red',linewidth=2)
	ax2.axis((start_time,duration,0,1))
	e_xlabel = ax2.set_xlabel('time after start of infusion (min)', family='serif') # , fontsize=12
	e_ylabel = ax2.set_ylabel('effect (% of $E_{max}$)', family='serif'\
		, verticalalignment='center') # , fontsize=12
	ax2.legend(['effect'], framealpha=0, loc='best', fontsize='small')

	locs,labels = plt.yticks()
	plt.yticks(locs, map(percentify, locs))

	titlestring = 'Predicted $C_p(t)$, $C_e(t)$ and $E(C_e(t))$\n$t_{\\frac{1}{2}e} =$'\
		+' {:.0f} min.'.format(t_half_effect)
	figure.suptitle(titlestring, family='serif', size='large', verticalalignment='center') # 

	figure.savefig(figure_name+'.'+figureext, dpi=dots_per_inch, \
			bbox_extra_artists=(c_ylabel, e_xlabel, e_ylabel,), bbox_inches='tight')
	print
	print(figure_name+'.'+figureext + ' uses the following parameters:')
	print('ke0 = {:0.6f}/min, t_half_effect = {:.2f} min, ec50 = {:.1f} ng/ml, nHill = {:.2f}'\
			.format( ke0, t_half_effect, ec50, nHill ) )
	
	np.savetxt(figure_name+'.txt', \
		np.hstack((times.reshape((times.size,1)),tempeffect.reshape((tempeffect.size,1)))), \
		delimiter=",")
