import argparse
import lmfit
import numpy as np
import matplotlib.pyplot as plt
import mpdp
import pandas as pd
import os
import seaborn

from string import ascii_uppercase

OUTFILE_TEMPLATE = '{}_cpt-{}_{}iter_pctile.tif'

cpt_map = {
    'hi': 'High',
    'lo': 'Low'
}

def horiz_plot(cpt_pairs, noiseCoV, var, outdir, outfile, axes, savefig=False, col2=False):
    for i, cp_t in enumerate([ label for label, _ in cpt_pairs ]):
        ax = axes[i] if plt_rows > 1 else axes
        print(ax)
        color = plt.cm.gray(.5)
        df = pd.read_csv(os.path.join(outdir, 'all_stages_noise-{}_cpt-{}_emcee_results.csv'.format(noiseCoV, cp_t)))


        datasets = [ (g, data[var].values) for g, data in sorted(df.groupby('stage'), key=lambda x: x[0].lower()) ]
        positions = range(len(datasets), 0, -1) # reverse the positions so best is on top and worst on bottom for y-xis

        data = [ np.log10(d[1]) if var == 'k' else d[1] for d in datasets]
        violin = ax.violinplot(data, positions=positions, showextrema=False, vert=False)
        ax.set_yticks(positions)
        ax.set_yticklabels(['H&Y {}'.format(label) if label.startswith('I') else label for label, _ in datasets])

        for pc in violin['bodies']:
            pc.set_facecolor(color)

        pctile5, medians, pctile95 = np.percentile(data, [5, 50, 95], axis=1)

        if var == 'k':
            target = [np.log10(val) for val in mpdp.keqs]
            xticks = np.arange(-3,1)
            ax.set_xticks(xticks)
            ax.set_xticklabels(10.0 ** xticks)
        elif var == 'ec50':
            target = mpdp.ec50s
        else:
            target = mpdp.hillns


        ax.hlines(positions, pctile5, pctile95, linestyle='-', color=color) # add horizontal line for 5th-95th percentiles
        for pctile in [ pctile5, medians, pctile95]:
            ax.scatter(pctile, positions, marker='|', color=color)
        ax.scatter(target[:len(datasets)], positions, color='black', zorder=5) # add the target point

        title = '{} Cp(t)'.format(cpt_map[cp_t] if cp_t in cpt_map else cp_t.title())
        ax.set_title(title)
        ax.set_xlabel('Estimated {}'.format(var if var != 'k' else '$\mathregular{k_e}$'))

        letter_idx = i if not col2 else i*2+1
        ax.text(-0.15, 1.05, ascii_uppercase[letter_idx], transform=ax.transAxes, size=18, weight='bold')

    plt.ylabel('Disease Severity')
    plt.suptitle('Noise CoV = {}%'.format(noiseCoV*100))


outdir = r'C:\Users\acevedoh\Box\Black_Lab\projects\PD\hysteresis_phMRI\analysis\lmfit\emcee'
color = plt.cm.gray(.5)
noise_levels = [0.129, 0.05]
pad = 5

for pairlst in [[ 'mean', 'hi', 'lo' ], ['slow'] ]:
    plt_rows = len(pairlst)

    for var in ['k', 'ec50', 'n']:
        outfile = os.path.join(outdir, OUTFILE_TEMPLATE.format(var, pairlst[0] if len(pairlst) == 1 else 'all', 100))
        fig, axes = plt.subplots(nrows=plt_rows, ncols=2, sharex=True, sharey=True, figsize=(8, 5*plt_rows), squeeze=False)# gridspec_kw={'wspace': 0, 'hspace': 0})

        for i, cp_t in enumerate(pairlst):
            for j, noiseCoV in enumerate(noise_levels):
                ax = axes[i][j]
                df = pd.read_csv(os.path.join(outdir, 'all_stages_noise-{}_cpt-{}_emcee_results.csv'.format(noiseCoV, cp_t)))

                datasets = [ (g, data[var].values) for g, data in sorted(df.groupby('stage'), key=lambda x: x[0].lower()) ]
                data = [ np.log10(d[1]) if var == 'k' else d[1] for d in datasets]
                positions = range(len(datasets))
                violin = ax.violinplot(data, positions=positions, showextrema=False)
                ax.set_xticks(positions)
                ax.set_xticklabels(['H&Y {}'.format(label) if label.startswith('I') else label for label, _ in datasets], rotation=45)

                for pc in violin['bodies']:
                    pc.set_facecolor(color)

                pctile5, medians, pctile95 = np.percentile(data, [5, 50, 95], axis=1)

                if var == 'k':
                    target = [np.log10(val) for val in mpdp.keqs]
                    yticks = np.arange(-3,1)
                    ax.set_yticks(yticks)
                    ax.set_yticklabels(10.0 ** yticks)
                elif var == 'ec50':
                    target = mpdp.ec50s
                else:
                    target = mpdp.hillns

                positions = range(len(datasets))

                ax.vlines(positions, pctile5, pctile95, linestyle='-', color=color) # add vertical line for 5th-95th percentiles
                for pctile in [ pctile5, medians, pctile95]:
                    ax.scatter(positions, pctile, marker=0, color=color)
                    ax.scatter(positions, pctile, marker=1, color=color)
                ax.scatter(positions, target[:len(datasets)], color='black', zorder=5) # add the target point

                # title = '{} Cp(t)'.format(cpt_map[cp_t] if cp_t in cpt_map else cp_t.title())
                # ax.set_title(title)
                # ax.set_ylabel('Estimated {}'.format(var if var != 'k' else '$\mathregular{k_e}$'))

                letter_idx = j * plt_rows + i
                letter_x, letter_y = -0.1, 1
                ax.text(letter_x, letter_y, ascii_uppercase[letter_idx], transform=ax.transAxes, size=18, weight='bold')

            # ax.set_xlabel('Disease Severity')
            # plt.suptitle('Noise CoV = {}%'.format(noiseCoV*100))

        for ax, noiseCoV in zip(axes[0], noise_levels):
            col_label = 'Noise CoV = {}%'.format(noiseCoV*100)
            # ax.set_title(col_label)
            ax.annotate(col_label, xy=(0.5, 1), xytext=(0, 20),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')

        for ax, noiseCoV in zip(axes[-1], noise_levels):
            ax.set_xlabel('Disease Severity')

        for ax, cp_t in zip(axes[:, 0], pairlst):
            ax.set_ylabel('Estimated {}'.format(var if var != 'k' else '$\mathregular{k_e}$'))
            if len(pairlst) == 1:
                continue

            row_label = '{} Cp(t)'.format(cpt_map[cp_t] if cp_t in cpt_map else cp_t.title())
            # ax.set_ylabel(row_label, size='large')
            ax.annotate(row_label, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                        xycoords=ax.yaxis.label, textcoords='offset points',
                        size='large', ha='right', va='center')


        # plt.tight_layout()
        # plt.subplots_adjust(top=0.85)
        # plt.suptitle('Estimated {}'.format(var if var != 'k' else '$\mathregular{k_e}$'))

        plt.show()
        break
        # plt.savefig(outfile, bbox_inches='tight', dpi=600, type='tiff')
