# README #

Bucket mapping project 2017-2020

---

### What is this repository for? ###

* This repository holds code for the [QuanDyn�](http://quandyn.com/) method for 
dopamine buffering capacity imaging (see References below).
* Version 1

### How do I get set up? ###

* This is our raw, un-buffed code. Usual disclaimers.

### Contribution guidelines ###

Feel free to fork this and improve it, or if you want send us a pull request.

### Who do I talk to? ###

* Owner: [Kevin J. Black, M.D.](mailto:kevin@wustl.edu?Subject=hysteresis%20mapping%20repo) 
* Jonathan Koller is also a contributor

### References ###

* The files corresponding to the Version 1 tag are archived on 
[Zenodo, doi: 10.5281/zenodo.3766492](https://doi.org/10.5281/zenodo.3766492)
* The background and motivation are explained in this article: 

	Black KJ, Acevedo HK and Koller JM (2020) Dopamine buffering capacity
	imaging: A pharmacodynamic fMRI method for staging Parkinson
	disease. Front. Neurol. 11:370. 
	doi: [10.3389/fneur.2020.00370](https://doi.org/10.3389/fneur.2020.00370)

