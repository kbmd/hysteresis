#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 17:04:07 2019

@author: kevin
"""

import math
ln2 = math.log(2)
def ln2over(t):
    return ln2/t
import numpy as np
import matplotlib.pyplot as plt

decay = ln2over(15)
bolust = np.asarray([0.0,4.5,10.0,15.0,21.6,29.7,39.9,57.1,82.0,100.8])
bolusc = np.append(np.asarray([0,1227,2533],dtype=np.float), 
                   2533*np.exp(-decay*(bolust[3:]-10.0)) )
#plt.plot(bolust, bolusc)

delay = 60 # minutes from start of first to start of second i.v. bolus
times2 = np.arange(0,121)
tinterp = np.interp(times2, bolust, bolusc)  # that's just bolusc, resampled
tinterp_delay = np.insert(tinterp, 0, np.zeros(delay))
tinterp_delay = tinterp_delay[:-delay]
#plt.plot(times2, tinterp)
#plt.plot(times2, tinterp_delay)

doublec = tinterp + tinterp_delay
#plt.plot(times2, doublec)

show_figures = False
if show_figures:
    graph_effects(doublec, graphtime=times2, subtitle='double', 
                  savefig=save_figures)
    for i in [1, 2]:
        ces = Ce(times2, keqs[i], times2, doublec)
        effects = effect(ces,ec50s[i],hillns[i])
        plt.plot(times2, effects)

plt.close

decay1 = ln2over(0.14*60) # from Siddiqi et al IVLD review
decay2 = ln2over(1.5*60)  # from Siddiqi et al IVLD review
Afrac = 0.8
lilbolust = np.asarray([0.0,5.0,10.0,15.0,21.6,29.7,39.9,57.1,82.0,100.8])
lilbolusc = np.append(np.asarray([0, 1400],dtype=np.float),
                      1400*(Afrac*np.exp(-decay1*(bolust[2:]-5.0)) +
                      (1-Afrac)*np.exp(-decay2*(bolust[2:]-5.0))) )
delay = 20
d0 =  np.interp(times2, lilbolust, lilbolusc)
d1 = np.insert(d0, 0, np.zeros(delay))
d1 = d1[:d1.size-delay]
d2 = np.insert(d1, 0, np.zeros(delay))
d2 = d2[:d2.size-delay]
d3 = np.insert(d2, 0, np.zeros(delay))
d3 = d3[:d3.size-delay]
d4 = np.insert(d3, 0, np.zeros(delay))
d4 = d4[:d4.size-delay]
total = d0 + d1 + d3
#plt.plot(times2, total)

show_figures = True
if show_figures:
    graph_effects(total, graphtime=times2, subtitle='quad', 
                  savefig=save_figures)

slowt = np.asarray([0.0,9.0,20.0,30.0,43.2,59.4,79.8,114.2])
slowc = np.append(np.asarray([0,1227,2533],dtype=np.float), 
                   2533*(Afrac*np.exp(-decay1*(slowt[3:]-20.0)) +
                         (1-Afrac)*np.exp(-decay2*(slowt[3:]-20.0))))
if show_figures:
    graph_effects(slowc, graphtime=slowt, subtitle='slow', 
                  savefig=save_figures)

slow2t = np.asarray([0.0,18.0,40.0,60.0,86.4,118.8])
slow2c = 2*slowc[:-2]
if show_figures:
    graph_effects(slow2c, graphtime=slow2t, subtitle='slow double', 
                  savefig=save_figures)

