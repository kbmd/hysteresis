
# coding: utf-8

# In[8]:

with open('Cp_estimation_20150304.csv', 'rb') as datafile:
    data  = np.genfromtxt(datafile, delimiter=",", usecols=range(0,7), names=True)  # imports as floats
    # it imports 7 blank rows at the end ...
    data = data[:-7]
data_times=data['new_minutes']
data_concs=data['LD']

get_ipython().magic('matplotlib inline')
plt.scatter(data_times,data_concs,color='black')
plt.axis((0,120,0,3500))
plt.xlabel('time after start of infusion (min.)')  # ,fontsize=16
plt.ylabel('Levodopa plasma concentration (ng/ml)')
plt.show()


# In[6]:

print(data_times[:9])


# In[7]:

print(data_concs[:9])


# In[19]:

print(data[:2])


# # OK. Here is the test.

# In[50]:

newdata = np.vstack((data_times, data_concs))
print(newdata.shape)
print(newdata[:,0])


# In[44]:

#import csv

#with open('test.csv', 'wb') as testfile:
#    writer = csv.writer(testfile, dialect='excel')
#    writer.writerow(["time,[LD]"])
#    for row in range(newdata.shape[1]):
#        writer.writerow((newdata[:,row]))


# In[51]:

with open('test.csv', 'wb') as testfile:
    np.savetxt(testfile,newdata.T,delimiter=',',header='time,[LD]')


# In[ ]:



